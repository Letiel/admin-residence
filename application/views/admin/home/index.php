<!DOCTYPE html>
<html>
<head>
	<!-- Page title -->
	<title>Residence Online</title>
	<?php $this->load->view("admin/inc/head_basico"); ?>
	<!-- CALENDÁRIO -->
	<link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
	<link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
</head>
<body>
	<?php $this->load->view("admin/inc/menu_lateral") ?>

	<!-- Main Wrapper -->
	<div id="wrapper">
		<div class="normalheader ">
			<div class="hpanel">
				<div class="panel-body">
					<h2 class="font-light m-b-xs">
						Bem vind<?= ($usuario->sexo == "M" ? "o" : "a") ?>, <?= $usuario->nome ?>
					</h2>
					<small>Este é seu mural.</small>
				</div>
			</div>
		</div>
		<div class="content animate-panel">

			<div class="row">
				
				<div class="col-lg-12">
					<div class="hpanel">
						<div class="panel-heading">
							<h3>Comunique</h3>
						</div>
						<?php if ($comunique!= null): ?>
							<div class="panel-body list">
								<div class="row">
									<div class="col-lg-12">
										<div class="pull-right">
											<a href="/comunique" class="btn btn-xs btn-default">Ver Todas</a>
										</div>
									</div>
								</div>
								<div class="table-responsive col-lg-12">
									<table class="table table-hover table-mailbox">
										<tbody>
											<?php foreach ($comunique as $sol): ?>
												<tr class="<?= (strtotime($sol->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))) ? 'unread' : '' ; ?>">
													<td class="">
														<i class="fa fa-envelope-o "></i>
													</td>
													<td style="width: 30%;">
														<a href="/comunique/ver/<?= $sol->id ?>">
															<?php if ($sol->remetente == $this->session->userdata('id')): ?>
																Criado por: Você
															<?php else: ?>
																Criado por: <?= $sol->nome_remetente ?>
															<?php endif ?>
														</a>                                                        
													</td>
													<td><a href="/comunique/ver/<?= $sol->id ?>"><?= $sol->titulo ?></a></td>
													<td class="text-right mail-date">
														<?php if ($sol->resolvida == 1): ?>
															<span class="label label-success pull-left">Resolvida</span>
														<?php endif ?>
														<?= date('d/m/Y H:i', strtotime($sol->data_alteracao)) ?>
													</td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						<?php else: ?>
							<div class="panel-body">
								<h2 class="text-info text-center">Nenhuma comunicação</h2>
							</div>
						<?php endif ?>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="hpanel">
						<div class="panel-heading">
							<h3>Reservas</h3>
						</div>
						<div class="panel-body">
							<div class="pull-right">
								<a href="/reservas"  class="btn btn-xs btn-default">Ver Todas</a>
							</div>
							<div id="calendario"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container_modal"></div>
		<?php $this->load->view("admin/inc/footer"); ?>

	</div>

	<?php $this->load->view("admin/inc/scripts_gerais") ?>

	<script src="/vendor/moment/min/moment.min.js"></script>
	<script src="/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
	<script src="/vendor/fullcalendar/dist/lang/pt-br.js"></script>
	<script type="text/javascript">

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$('#calendario').fullCalendar({
			header: {
				left: '',
				center: 'title',
				right: ''
			},
			editable: false,
			droppable: false,
			events: [
			<?php foreach ($reservas as $reserva): ?>
			{
				title: "<?= $reserva->nome_area ?>",
				start: new Date(<?= date("Y", strtotime($reserva->hora_inicio)) ?>, <?= date("m", strtotime($reserva->hora_inicio)) ?> -1, <?= date("d", strtotime($reserva->hora_inicio)) ?>, <?= date("H", strtotime($reserva->hora_inicio)) ?>, <?= date("i", strtotime($reserva->hora_inicio)) ?>),
				end: new Date(<?= date("Y", strtotime($reserva->hora_fim)) ?>, <?= date("m", strtotime($reserva->hora_fim)) ?> -1, <?= date("d", strtotime($reserva->hora_fim)) ?>, <?= date("H", strtotime($reserva->hora_fim)) ?>, <?= date("i", strtotime($reserva->hora_fim)) ?>),
				allDay: false,
				id: <?= $reserva->id ?>,
				<?= $reserva->autorizado ? "backgroundColor: '#62cb31',
				borderColor: '#62cb31'" : "" ?>
			},
		<?php endforeach ?>
		],
		eventClick: function(calEvent) {
			item = $(this);
			anterior = item.html();
			item.html("<i class='fa fa-cog fa-spin'></i>");
			$.post("/reservas/ver", {
				id: calEvent.id
			}, function(result){
				$(".container_modal").html(result);
				item.html(anterior);
			});
		}
	});
</script>
<?php if ($this->session->flashdata("validade")): ?>
	<div class="mensagem_validade">
	<div class="sweet-overlay" tabindex="-1" style="opacity: 1.08; display: block;"></div>
	<div class="sweet-alert showSweetAlert visible" tabindex="-1" data-has-cancel-button="false" data-allow-ouside-click="false" data-has-done-function="false" style="display: block; margin-top: -128px; z-index: 1999 !important;">
		<div class="icon error" style="display: none;">
			<span class="x-mark">
				<span class="line left"></span>
				<span class="line right"></span>
			</span>
		</div>
		<div class="icon warning" style="display: none;">
			<span class="body"></span> <span class="dot"></span>
		</div>
		<div class="icon info" style="display: none;"></div>
		<div class="icon success" style="display: none;">
			<span class="line tip"></span>
			<span class="line long"></span>
			<div class="placeholder"></div>
			<div class="fix"></div>
		</div>
		<div class="icon custom" style="display: none;"></div>
		<h2>ATENÇÃO! Sua licença do <br><b>RESIDENCE ONLINE</b><br> expira em breve</h2>
		<p style="display: block;">Atenção! Sua licença irá expirar em <?php echo $this->session->userdata("dias"); ?> dia(s). Entre em contato com o administrador do sistema para renova-la.</p>
		<button class="cancel" tabindex="2" style="display: none; box-shadow: none;">Cancel</button>
		<button class="confirm-validade" tabindex="1" style="background-color: rgb(174, 222, 244); box-shadow: rgba(174, 222, 244, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset;">OK</button>
	</div>
</div>
<script type="text/javascript">
	$(".confirm-validade").click(function(){
		$(".mensagem_validade").remove();
	});
</script>
<?php endif ?>
</body>
</html>