<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        Cadastrar área
                    </div>
                    <div class="panel-body">
                        <form action="/cadastro/area" method="post">
                            <div class="col-md-5 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Identificação:</label>
                                <input class="form-control" id="identificacao" placeholder="Identificação" />
                            </div>
                            <div class="col-md-5 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Localização:</label>
                                <input class="form-control" id="localizacao" placeholder="Localização" />
                            </div>
                            <div class="col-md-5 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Colaborador responsável:</label>
                                <select class="form-control">
                                    <option value="">Selecione...</option>
                                    <option value="">Portaria</option>
                                </select>
                            </div>
                            <div class="col-md-5 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Descrição:</label>
                                <textarea class="form-control" placeholder="Descrição"></textarea>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12">
                                <button class="btn btn-primary btn-lg pull-right">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
    <div class="ip-modal-dialog">
        <div class="ip-modal-content">
            <div class="ip-modal-header">
                <a class="ip-close" title="Close">&times;</a>
                <h4 class="ip-modal-title">Enviar imagem</h4>
            </div>
            <div class="ip-modal-body">
                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                <button type="button" class="btn btn-info ip-edit">Editar</button>
                <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                <div class="alert ip-alert"></div>
                <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                <div class="ip-preview"></div>
                <div class="ip-rotate">
                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                </div>
                <div class="ip-progress">
                    <div class="text">Enviando</div>
                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                </div>
            </div>
            <div class="ip-modal-footer">
                <div class="ip-actions">
                    <button type="button" class="btn btn-success ip-save">Salvar</button>
                    <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                </div>
                <button type="button" class="btn btn-default ip-close">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<!-- JavaScript CROPP -->
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<script>
    $(function() {
        var time = function(){return'?'+new Date().getTime()};

        // Avatar setup
        $('#avatarModal').imgPicker({
            url: 'server/upload_avatar.php',
            aspectRatio: 1,
            deleteComplete: function() {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                        [(image.width-image.height)/2, 0, image.height, image.height] :
                        [0, (image.height-image.width)/2, image.width, image.width];

                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', image.versions.avatar.url +time() );
                this.modal('hide');
            }
        });

        // Demo only
        $('.navbar-toggle').on('click',function(){$('.navbar-nav').toggleClass('navbar-collapse')});
        $(window).resize(function(e){if($(document).width()>=430)$('.navbar-nav').removeClass('navbar-collapse')});
    });
</script>
</body>
</html>