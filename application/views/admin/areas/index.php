<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- DATEPICKER -->
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <?php $this->load->view("admin/areas/modals/cadastrar_area") ?>
                    <h2 class="font-light m-b-xs">
                        Áreas Comuns
                    </h2>
                    <a class="btn btn-info btn-outline btn-lg btn-block visible-xs" data-toggle="modal" data-target="#cadastro_area"><i class="fa fa-plus"></i> Cadastrar área</a>
                    <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_area") ?></h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/areas" method="get">
                                <div class="input-group">
                                    <input name="k" class="form-control" type="text" placeholder="Pesquisar áreas...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php if ($areas != null): ?>
                            <?php foreach ($areas as $area): ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10 forum-heading m-b-xs">
                                            <a><h4> <?= $area->nome ?></h4></a>
                                            <div class="desc"><b>Localização: </b><?= $area->nome_localizacao ?></div>
                                            <div class="desc"><b>Responsável: </b><?= $area->nome_usuario ?></div>
                                        </div>
                                        <div class="col-md-1 forum-info m-b-xs">
                                            <button value="<?= $area->id ?>" class="btn btn-primary btn-block btn-outline editar"><span class="fa fa-pencil"></span></button>
                                        </div>
                                        <div class="col-md-1 forum-info m-b-xs">
                                            <button value="<?= $area->id ?>" class="btn btn-danger btn-block btn-outline deletar"><span class="fa fa-trash"></span></button>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                            <?php if ($paginacao): ?>
                                <div class="panel-body">
                                    <center><?= $paginacao ?></center>
                                </div>
                            <?php endif ?>
                        <?php else: ?>
                            <div class="panel-body">
                                <div class="row">
                                    <h3 class="text-center text-info">Nenhuma Área</h3>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
        <div class="container_modal"></div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>

    <script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>
    <!-- SELECT2 -->
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#tbl_moradores').footable({paginate: false});

            $(".editar").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/areas/editar", {
                    id: $(this).val()
                }, function(result){
                    item.html(anterior);
                    $(".container_modal").html(result);
                });
            });

            $(".deletar").click(function(){
                if(confirm("Tem certeza?")){
                    item = $(this);
                    anterior = item.html();
                    item.html("<i class='fa fa-cog fa-spin'></i>");
                    $.post("/areas/deletar", {
                        id: $(this).val()
                    }, function(result){
                        item.html(anterior);
                        location.reload();                  
                    });
                }
            });

            $("#localizacao").select2({
                theme: "bootstrap",
                minimumInputLength: 2,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/areas/select2_listaLocalizacoes",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

            $("#usuario_responsavel").select2({
                theme: "bootstrap",
                minimumInputLength: 2,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/veiculos/select2_listaUsuarios",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    type: ""
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome+" "+item.residencia+" "+item.localizacao,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
        });
    </script>
</body>
</html>