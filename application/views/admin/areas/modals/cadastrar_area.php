<a class="btn btn-info btn-outline btn-lg pull-right hidden-xs" data-toggle="modal" data-target="#cadastro_area"><i class="fa fa-plus"></i> Cadastrar área</a>
<div class="modal fade" id="cadastro_area" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar nova área</h4>
                <h5 class="text-danger" id="modal_cadastro_area_erro"></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nome:</label>
                    <input placeholder="Identificação" required class="form-control" type="text" id="nome" />
                </div>
                <div class="form-group">
                    <label>Localização:</label>
                    <select class='form-control' style="width: 100%;" id="localizacao" placeholder="Localização"><option value="">Selecione...</option></select>
                </div>
            	<div class="form-group">
            		<label>Responsável:</label>
            		<select class='form-control' style="width: 100%;" id="usuario_responsavel" placeholder="Responsável"><option value="">Selecione...</option></select>
            	</div>
                <div class="form-group">
                    <label>Descrição:</label>
                    <textarea placeholder="Descrição" required class="form-control" id="descricao"></textarea>
                </div>
                <hr>
                <h4 class="text-center text-info">Horários de funcionamento</h4>
                <div class="form-group row">
                    <label>Domingo</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="domingo" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_domingo" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" id="final_domingo" type="text" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Segunda-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="segunda" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_segunda" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_segunda" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Terça-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="terca" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_terca" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_terca" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Quarta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="quarta" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_quarta" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_quarta" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Quinta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="quinta" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_quinta" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_quinta" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Sexta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="sexta" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_sexta" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_sexta" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Sábado</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="sabado" type="checkbox" class="marcar"> </span> 
                            <input type="text" id="inicial_sabado" disabled class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" type="text" id="final_sabado" disabled class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_area">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
        $(".marcar").change(function(){
            horario_inicial = $(this).parent().parent().children(".horario_inicial");
            horario_final = $(this).parent().parent().parent().parent().children().children(".horario_final");
            horario_inicial.attr("disabled", !this.checked);
            horario_final.attr("disabled", !this.checked);
            horario_inicial.attr("required", this.checked);
            horario_final.attr("required", this.checked);
        });

		$("#cadastrar_area").click(function(){
            if(document.getElementById("domingo").checked){
                if($("#inicial_domingo").val() == "__:__" || $("#inicial_domingo").val() == "" || $("#inicial_domingo").val() == null){
                    $("#inicial_domingo").focus();
                    return false;
                }

                if($("#final_domingo").val() == "__:__" || $("#final_domingo").val() == "" || $("#final_domingo").val() == null){
                    $("#final_domingo").focus();
                    return false;
                }

                if($("#final_domingo").val() <= $("#inicial_domingo").val()){
                    alert("Horários de domingo inválidos.");
                    $("#final_domingo").focus();
                    return false;
                }
            }
            if(document.getElementById("segunda").checked){
                if($("#inicial_segunda").val() == "__:__" || $("#inicial_segunda").val() == "" || $("#inicial_segunda").val() == null){
                    $("#inicial_segunda").focus();
                    return false;
                }

                if($("#final_segunda").val() == "__:__" || $("#final_segunda").val() == "" || $("#final_segunda").val() == null){
                    $("#final_segunda").focus();
                    return false;
                }

                if($("#final_segunda").val() <= $("#inicial_segunda").val()){
                    alert("Horários de segunda-feira inválidos.");
                    $("#final_segunda").focus();
                    return false;
                }
            }
            if(document.getElementById("terca").checked){
                if($("#inicial_terca").val() == "__:__" || $("#inicial_terca").val() == "" || $("#inicial_terca").val() == null){
                    $("#inicial_terca").focus();
                    return false;
                }

                if($("#final_terca").val() == "__:__" || $("#final_terca").val() == "" || $("#final_terca").val() == null){
                    $("#final_terca").focus();
                    return false;
                }

                if($("#final_terca").val() <= $("#inicial_terca").val()){
                    alert("Horários de terça-feira inválidos.");
                    $("#final_terca").focus();
                    return false;
                }
            }
            if(document.getElementById("quarta").checked){
                if($("#inicial_quarta").val() == "__:__" || $("#inicial_quarta").val() == "" || $("#inicial_quarta").val() == null){
                    $("#inicial_quarta").focus();
                    return false;
                }

                if($("#final_quarta").val() == "__:__" || $("#final_quarta").val() == "" || $("#final_quarta").val() == null){
                    $("#final_quarta").focus();
                    return false;
                }

                if($("#final_quarta").val() <= $("#inicial_quarta").val()){
                    alert("Horários de quarta-feira inválidos.");
                    $("#final_quarta").focus();
                    return false;
                }
            }
            if(document.getElementById("quinta").checked){
                if($("#inicial_quinta").val() == "__:__" || $("#inicial_quinta").val() == "" || $("#inicial_quinta").val() == null){
                    $("#inicial_quinta").focus();
                    return false;
                }

                if($("#final_quinta").val() == "__:__" || $("#final_quinta").val() == "" || $("#final_quinta").val() == null){
                    $("#final_quinta").focus();
                    return false;
                }

                if($("#final_quinta").val() <= $("#inicial_quinta").val()){
                    alert("Horários de quinta-feira inválidos.");
                    $("#final_quinta").focus();
                    return false;
                }
            }
            if(document.getElementById("sexta").checked){
                if($("#inicial_sexta").val() == "__:__" || $("#inicial_sexta").val() == "" || $("#inicial_sexta").val() == null){
                    $("#inicial_sexta").focus();
                    return false;
                }

                if($("#final_sexta").val() == "__:__" || $("#final_sexta").val() == "" || $("#final_sexta").val() == null){
                    $("#final_sexta").focus();
                    return false;
                }

                if($("#final_sexta").val() <= $("#inicial_sexta").val()){
                    alert("Horários de sexta-feira inválidos.");
                    $("#final_sexta").focus();
                    return false;
                }
            }
            if(document.getElementById("sabado").checked){
                if($("#inicial_sabado").val() == "__:__" || $("#inicial_sabado").val() == "" || $("#inicial_sabado").val() == null){
                    $("#inicial_sabado").focus();
                    return false;
                }

                if($("#final_sabado").val() == "__:__" || $("#final_sabado").val() == "" || $("#final_sabado").val() == null){
                    $("#final_sabado").focus();
                    return false;
                }

                if($("#final_sabado").val() <= $("#inicial_sabado").val()){
                    alert("Horários de sábado inválidos.");
                    $("#final_sabado").focus();
                    return false;
                }
            }
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/ajax_cadastro_area", {
                nome: $("#nome").val(),
                localizacao:$("#localizacao").val(),
                usuario_responsavel:$("#usuario_responsavel").val(),
                descricao:$("#descricao").val(),
                h_inicial_domingo: $("#inicial_domingo").val(),
                h_final_domingo: $("#final_domingo").val(),
                h_inicial_segunda: $("#inicial_segunda").val(),
                h_final_segunda: $("#final_segunda").val(),
                h_inicial_terca: $("#inicial_terca").val(),
                h_final_terca: $("#final_terca").val(),
                h_inicial_quarta: $("#inicial_quarta").val(),
                h_final_quarta: $("#final_quarta").val(),
                h_inicial_quinta: $("#inicial_quinta").val(),
                h_final_quinta: $("#final_quinta").val(),
                h_inicial_sexta: $("#inicial_sexta").val(),
                h_final_sexta: $("#final_sexta").val(),
                h_inicial_sabado: $("#inicial_sabado").val(),
                h_final_sabado: $("#final_sabado").val(),
                domingo: document.getElementById("domingo").checked,
                segunda: document.getElementById("segunda").checked,
                terca: document.getElementById("terca").checked,
                quarta: document.getElementById("quarta").checked,
                quinta: document.getElementById("quinta").checked,
                sexta: document.getElementById("sexta").checked,
                sabado: document.getElementById("sabado").checked
            }, function(result){
                item.html(anterior);
				if(result == ""){
                    $("#domingo").attr("checked", false);
                    $("#segunda").attr("checked", false);
                    $("#terca").attr("checked", false);
                    $("#quarta").attr("checked", false);
                    $("#quinta").attr("checked", false);
                    $("#sexta").attr("checked", false);
                    $("#sabado").attr("checked", false);
                    $("#inicial_domingo").val("");
                    $("#final_domingo").val("");
                    $("#inicial_segunda").val("");
                    $("#final_segunda").val("");
                    $("#inicial_terca").val("");
                    $("#final_terca").val("");
                    $("#inicial_quarta").val("");
                    $("#final_quarta").val("");
                    $("#inicial_quinta").val("");
                    $("#final_quinta").val("");
                    $("#inicial_sexta").val("");
                    $("#final_sexta").val("");
                    $("#inicial_sabado").val("");
                    $("#final_sabado").val("");
                    $("#inicial_domingo").attr("disabled", true);
                    $("#final_domingo").attr("disabled", true);
                    $("#inicial_segunda").attr("disabled", true);
                    $("#final_segunda").attr("disabled", true);
                    $("#inicial_terca").attr("disabled", true);
                    $("#final_terca").attr("disabled", true);
                    $("#inicial_quarta").attr("disabled", true);
                    $("#final_quarta").attr("disabled", true);
                    $("#inicial_quinta").attr("disabled", true);
                    $("#final_quinta").attr("disabled", true);
                    $("#inicial_sexta").attr("disabled", true);
                    $("#final_sexta").attr("disabled", true);
                    $("#inicial_sabado").attr("disabled", true);
                    $("#final_sabado").attr("disabled", true);
                    $("#nome").val("");
                    $("#localizacao").select2("val", "");
                    $("#usuario_responsavel").select2("val", "");
					$("#descricao").val("");
					$('#cadastro_area').modal('hide');
					$("#modal_cadastro_area_erro").html("");
                    location.reload();
				}else
		        	$("#modal_cadastro_area_erro").html(result);
		    });
		});

        $.datetimepicker.setLocale('pt-BR');
        $(".horario_inicial").datetimepicker({
            format:'H:i',
            datepicker:false,
            mask:true,
        });

        $(".horario_final").datetimepicker({
            format:'H:i',
            datepicker:false,
            mask:true,
        });
	});
</script>