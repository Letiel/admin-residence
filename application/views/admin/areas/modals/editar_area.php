<div class="modal fade" id="edicao_area" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Alterar área</h4>
                <h5 class="text-danger" id="modal_edicao_area_erro"></h5>
            </div>
            <div class="modal-body">
                <input type="hidden" id="editar_id" value="<?= $area->id ?>" />
                <div class="form-group">
                    <label>Nome:</label>
                    <input placeholder="Identificação" value="<?= $area->nome ?>" required class="form-control" type="text" id="editar_nome" />
                </div>
                <div class="form-group">
                    <label>Localização:</label>
                    <select class='form-control' style="width: 100%;" id="editar_localizacao" placeholder="Localização"><option selected value="<?= $area->localizacao ?>"><?= $area->nome_localizacao ?></option></select>
                </div>
            	<div class="form-group">
            		<label>Responsável:</label>
            		<select class='form-control' style="width: 100%;" id="editar_usuario_responsavel" placeholder="Responsável"><option value="<?= $area->usuario_responsavel ?>"><?= $area->nome_usuario." ".$area->localizacao_usuario." ".$area->residencia_usuario ?></option></select>
            	</div>
                <div class="form-group">
                    <label>Descrição:</label>
                    <textarea placeholder="Descrição" required class="form-control" id="editar_descricao"><?= $area->descricao ?></textarea>
                </div>
                <hr>
                <?php
                    $domingo = false;
                    $segunda = false;
                    $terca = false;
                    $quarta = false;
                    $quinta = false;
                    $sexta = false;
                    $sabado = false;
                    $this->db->start_cache();
                    $this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
                    $this->db->where("horarios_areas.area", $area->id);
                    $horarios = $this->db->get("horarios_areas");
                    $this->db->stop_cache();
                    $this->db->flush_cache();
                    $horarios = $horarios->result();
                    foreach($horarios as $horario){
                        switch($horario->dia_repetivel){
                            case 'DOMINGO':
                                $domingo = true;
                                $inicial_domingo = $horario->horario_inicio;
                                $final_domingo = $horario->horario_final;
                                break;
                            case 'SEGUNDA':
                                $segunda = true;
                                $inicial_segunda = $horario->horario_inicio;
                                $final_segunda = $horario->horario_final;
                                break;
                            case 'TERCA':
                                $terca = true;
                                $inicial_terca = $horario->horario_inicio;
                                $final_terca = $horario->horario_final;
                                break;
                            case 'QUARTA':
                                $quarta = true;
                                $inicial_quarta = $horario->horario_inicio;
                                $final_quarta = $horario->horario_final;
                                break;
                            case 'QUINTA':
                                $quinta = true;
                                $inicial_quinta = $horario->horario_inicio;
                                $final_quinta = $horario->horario_final;
                                break;
                            case 'SEXTA':
                                $sexta = true;
                                $inicial_sexta = $horario->horario_inicio;
                                $final_sexta = $horario->horario_final;
                                break;
                            case 'SABADO':
                                $sabado = true;
                                $inicial_sabado = $horario->horario_inicio;
                                $final_sabado = $horario->horario_final;
                                break;
                            default:
                                break;
                        }
                    }
                ?>
                <h4 class="text-center text-info">Horários de funcionamento</h4>
                <div class="form-group row">
                    <label>Domingo</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="domingo" <?= ($domingo ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($domingo ? $inicial_domingo : '') ?>" type="text" id="inicial_domingo" <?= (!$domingo ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($domingo ? $final_domingo : '') ?>" id="final_domingo" type="text" <?= (!$domingo ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Segunda-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="segunda" <?= ($segunda ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($segunda ? $inicial_segunda : '') ?>" type="text" id="inicial_segunda" <?= (!$segunda ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($segunda ? $final_segunda : '') ?>" id="final_segunda" type="text" <?= (!$segunda ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Terça-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="terca" <?= ($terca ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($terca ? $inicial_terca : '') ?>" type="text" id="inicial_terca" <?= (!$terca ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($terca ? $final_terca : '') ?>" id="final_terca" type="text" <?= (!$terca ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Quarta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="quarta" <?= ($quarta ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($quarta ? $inicial_quarta : '') ?>" type="text" id="inicial_quarta" <?= (!$quarta ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($quarta ? $final_quarta : '') ?>" id="final_quarta" type="text" <?= (!$quarta ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Quinta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="quinta" <?= ($quinta ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($quinta ? $inicial_quinta : '') ?>" type="text" id="inicial_quinta" <?= (!$quinta ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($quinta ? $final_quinta : '') ?>" id="final_quinta" type="text" <?= (!$quinta ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Sexta-feira</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="sexta" <?= ($sexta ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($sexta ? $inicial_sexta : '') ?>" type="text" id="inicial_sexta" <?= (!$sexta ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($sexta ? $final_sexta : '') ?>" id="final_sexta" type="text" <?= (!$sexta ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Sábado</label>
                    <br />
                    <div class="col-lg-6 pull-left">
                        <div class="input-group m-b">
                            <span class="input-group-addon"> <input id="sabado" <?= ($sabado ? "checked" : "") ?> type="checkbox" class="marcar"> </span> 
                            <input value="<?= ($sabado ? $inicial_sabado : '') ?>" type="text" id="inicial_sabado" <?= (!$sabado ? "disabled" : "") ?> class="form-control horario_inicial">
                        </div>
                        <small>Horário inicial</small>
                    </div>
                    <div class="col-lg-6 pull-left">
                        <input style="margin-bottom: 15px;" value="<?= ($sabado ? $final_sabado : '') ?>" id="final_sabado" type="text" <?= (!$sabado ? "disabled" : "") ?> class="form-control horario_final">
                        <small>Horário final</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="editar-area">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#edicao_area").modal("show");

        $.datetimepicker.setLocale('pt-BR');
        $(".horario_inicial").datetimepicker({
            format:'H:i',
            datepicker:false,
            mask:true,
        });

        $(".horario_final").datetimepicker({
            format:'H:i',
            datepicker:false,
            mask:true,
        });

        $(".marcar").change(function(){
            horario_inicial = $(this).parent().parent().children(".horario_inicial");
            horario_final = $(this).parent().parent().parent().parent().children().children(".horario_final");
            horario_inicial.attr("disabled", !this.checked);
            horario_final.attr("disabled", !this.checked);
            horario_inicial.attr("required", this.checked);
            horario_final.attr("required", this.checked);
        });

        $("#editar-area").click(function(){
            if(document.getElementById("domingo").checked){
                if($("#inicial_domingo").val() == "__:__" || $("#inicial_domingo").val() == "" || $("#inicial_domingo").val() == null){
                    $("#inicial_domingo").focus();
                    return false;
                }

                if($("#final_domingo").val() == "__:__" || $("#final_domingo").val() == "" || $("#final_domingo").val() == null){
                    $("#final_domingo").focus();
                    return false;
                }

                if($("#final_domingo").val() <= $("#inicial_domingo").val()){
                    alert("Horários de domingo inválidos.");
                    $("#final_domingo").focus();
                    return false;
                }
            }
            if(document.getElementById("segunda").checked){
                if($("#inicial_segunda").val() == "__:__" || $("#inicial_segunda").val() == "" || $("#inicial_segunda").val() == null){
                    $("#inicial_segunda").focus();
                    return false;
                }

                if($("#final_segunda").val() == "__:__" || $("#final_segunda").val() == "" || $("#final_segunda").val() == null){
                    $("#final_segunda").focus();
                    return false;
                }

                if($("#final_segunda").val() <= $("#inicial_segunda").val()){
                    alert("Horários de segunda-feira inválidos.");
                    $("#final_segunda").focus();
                    return false;
                }
            }
            if(document.getElementById("terca").checked){
                if($("#inicial_terca").val() == "__:__" || $("#inicial_terca").val() == "" || $("#inicial_terca").val() == null){
                    $("#inicial_terca").focus();
                    return false;
                }

                if($("#final_terca").val() == "__:__" || $("#final_terca").val() == "" || $("#final_terca").val() == null){
                    $("#final_terca").focus();
                    return false;
                }

                if($("#final_terca").val() <= $("#inicial_terca").val()){
                    alert("Horários de terça-feira inválidos.");
                    $("#final_terca").focus();
                    return false;
                }
            }
            if(document.getElementById("quarta").checked){
                if($("#inicial_quarta").val() == "__:__" || $("#inicial_quarta").val() == "" || $("#inicial_quarta").val() == null){
                    $("#inicial_quarta").focus();
                    return false;
                }

                if($("#final_quarta").val() == "__:__" || $("#final_quarta").val() == "" || $("#final_quarta").val() == null){
                    $("#final_quarta").focus();
                    return false;
                }

                if($("#final_quarta").val() <= $("#inicial_quarta").val()){
                    alert("Horários de quarta-feira inválidos.");
                    $("#final_quarta").focus();
                    return false;
                }
            }
            if(document.getElementById("quinta").checked){
                if($("#inicial_quinta").val() == "__:__" || $("#inicial_quinta").val() == "" || $("#inicial_quinta").val() == null){
                    $("#inicial_quinta").focus();
                    return false;
                }

                if($("#final_quinta").val() == "__:__" || $("#final_quinta").val() == "" || $("#final_quinta").val() == null){
                    $("#final_quinta").focus();
                    return false;
                }

                if($("#final_quinta").val() <= $("#inicial_quinta").val()){
                    alert("Horários de quinta-feira inválidos.");
                    $("#final_quinta").focus();
                    return false;
                }
            }
            if(document.getElementById("sexta").checked){
                if($("#inicial_sexta").val() == "__:__" || $("#inicial_sexta").val() == "" || $("#inicial_sexta").val() == null){
                    $("#inicial_sexta").focus();
                    return false;
                }

                if($("#final_sexta").val() == "__:__" || $("#final_sexta").val() == "" || $("#final_sexta").val() == null){
                    $("#final_sexta").focus();
                    return false;
                }

                if($("#final_sexta").val() <= $("#inicial_sexta").val()){
                    alert("Horários de sexta-feira inválidos.");
                    $("#final_sexta").focus();
                    return false;
                }
            }
            if(document.getElementById("sabado").checked){
                if($("#inicial_sabado").val() == "__:__" || $("#inicial_sabado").val() == "" || $("#inicial_sabado").val() == null){
                    $("#inicial_sabado").focus();
                    return false;
                }

                if($("#final_sabado").val() == "__:__" || $("#final_sabado").val() == "" || $("#final_sabado").val() == null){
                    $("#final_sabado").focus();
                    return false;
                }

                if($("#final_sabado").val() <= $("#inicial_sabado").val()){
                    alert("Horários de sábado inválidos.");
                    $("#final_sabado").focus();
                    return false;
                }
            }
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/ajax_edicao_area", {
                h_inicial_domingo: $("#inicial_domingo").val(),
                h_final_domingo: $("#final_domingo").val(),
                h_inicial_segunda: $("#inicial_segunda").val(),
                h_final_segunda: $("#final_segunda").val(),
                h_inicial_terca: $("#inicial_terca").val(),
                h_final_terca: $("#final_terca").val(),
                h_inicial_quarta: $("#inicial_quarta").val(),
                h_final_quarta: $("#final_quarta").val(),
                h_inicial_quinta: $("#inicial_quinta").val(),
                h_final_quinta: $("#final_quinta").val(),
                h_inicial_sexta: $("#inicial_sexta").val(),
                h_final_sexta: $("#final_sexta").val(),
                h_inicial_sabado: $("#inicial_sabado").val(),
                h_final_sabado: $("#final_sabado").val(),
                domingo: document.getElementById("domingo").checked,
                segunda: document.getElementById("segunda").checked,
                terca: document.getElementById("terca").checked,
                quarta: document.getElementById("quarta").checked,
                quinta: document.getElementById("quinta").checked,
                sexta: document.getElementById("sexta").checked,
                sabado: document.getElementById("sabado").checked,
                id: $("#editar_id").val(),
                nome: $("#editar_nome").val(),
                localizacao:$("#editar_localizacao").val(),
                usuario_responsavel:$("#editar_usuario_responsavel").val(),
                descricao:$("#editar_descricao").val()
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $('#editar_area').modal('hide');
                    $("#modal_edicao_area_erro").html("");
                    location.reload();
                }else
                    $("#modal_edicao_area_erro").html(result);
            });
        });

        $("#editar_localizacao").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/areas/select2_listaLocalizacoes",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#editar_usuario_responsavel").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/veiculos/select2_listaUsuarios",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term,
                        type: "COLABORADOR"
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome+" "+item.residencia+" "+item.localizacao,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

    });
</script>