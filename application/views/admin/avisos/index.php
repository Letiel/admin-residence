<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Avisos | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <?php $this->load->view("admin/avisos/modals/cadastrar") ?>
                    <h2 class="font-light m-b-xs">
                        Avisos
                    </h2>
                    <a class="btn btn-info btn-outline btn-lg bnt-block visible-xs" data-toggle="modal" data-target="#cadastro_aviso"><i class="fa fa-plus"></i> Cadastrar Aviso</a>
                    <h3 class="text-info text-center"><?= $this->session->flashdata("mensagem_cadastro_aviso") ?></h3>
                </div>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel forum-box">
                        <?php if ($avisos == null): ?>
                            <div class="panel-body">
                                <h3 class="text-info text-center">Nenhum aviso</h3>
                            </div>
                        <?php else: ?>
                            <?php foreach ($avisos as $aviso): ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10 forum-heading animated-panel zoomIn" style="animation-delay: 0.4s;">
                                            <h4>
                                                <?php if (strtotime($aviso->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                                    <span class="label label-success">Novo</span>
                                                <?php endif ?>
                                                <?= $aviso->titulo ?>
                                                <?php if ($aviso->administradores): ?>
                                                    <small><span class="label label-info">Administradores</span></small>
                                                <?php else: ?>
                                                    <small><span class="label label-info">Todos</span></small>
                                                <?php endif ?>
                                            </h4>
                                                <div class="desc" style="margin-top: 15px;"><?= $aviso->descricao ?></div>
                                                <p><small><i class="fa fa-clock"></i><i class="fa fa-clock-o"></i> Até <?= date("d/m/Y", strtotime($aviso->data_validade)) ?></small></p>
                                            </div>
                                            <div class="col-md-2 forum-info animated-panel zoomIn" style="animation-delay: 0.4s;">
                                                <div class="btn-group" role="group" aria-label="...">
                                                    <button value="<?= $aviso->id ?>" class="btn btn-danger deletar "><i class="fa fa-trash"></i></button>
                                                    <button value="<?= $aviso->id ?>" class="btn btn-primary ver">Ver aviso</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            <?php endif ?>
                            <?php if ($paginacao != null): ?>
                                <div class="panel-body">
                                    <center><?= $paginacao ?></center>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view("admin/inc/footer"); ?>
        <div class="container_modal"></div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>
    <!-- SELECT2 -->
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#tbl_boxes').footable({paginate: false});

            $(".ver").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/avisos/ver", {
                    id: $(this).val()
                }, function(result){
                    $(".container_modal").html(result);
                    item.html(anterior);
                });
            });

        // $(".deletar").click(function(){
        //     if(confirm("Tem certeza?")){
        //         item = $(this);
        //         anterior = item.html();
        //         item.html("<i class='fa fa-cog fa-spin'></i>");
        //         $.post("/avisos/deletar", {
        //             id: $(this).val()
        //         }, function(result){
        //             console.log(result);
        //             item.html(anterior);
        //             location.reload();
        //         });
        //     }
        // });

        $('.deletar').click(function () {
            var value = $(this).val();
            var item = $(this);
            var anterior = item.html();
            swal({
                title: "Tem certeza?",
                text: "Você tem certeza que quer excluir este aviso?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero excluir!",
                cancelButtonText: "Cancelar",
            },
            function () {
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/avisos/deletar", {
                    id: value
                }, function(result){
                    console.log(result);
                    item.html(anterior);
                    location.reload();
                });
            });
        });
    });
</script>
</body>
</html>