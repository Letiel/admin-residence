<div class="modal fade" id="ver_aviso" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title"><?= $aviso->titulo ?> <br /> <small><small class="text-info text-center">Validade: <?= date("d/m/Y", strtotime($aviso->data_validade)) ?></small></small> </h4>
                <?php if ($aviso->administradores): ?>
                    <small><span class="label label-info">Administradores</span></small>
                <?php else: ?>
                    <small><span class="label label-info">Todos</span></small>
                <?php endif ?>
            </div>
            <div class="modal-body">
                <p class="text-center"><?= nl2br($aviso->descricao) ?></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
        $("#ver_aviso").modal("show");
	});
</script>