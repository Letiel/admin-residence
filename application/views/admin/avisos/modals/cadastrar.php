<a class="btn btn-info btn-outline btn-lg pull-right hidden-xs" data-toggle="modal" data-target="#cadastro_aviso"><i class="fa fa-plus"></i> Cadastrar Aviso</a>
<div class="modal fade" id="cadastro_aviso" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar aviso</h4>
                <h5 class="text-danger" id="modal_cadastro_aviso_erro"></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Destino:</label>
                    <select class="form-control" id="destino">
                        <option selected value="0">Todos</option>
                        <option value="1">Administradores</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Título:</label>
                    <input placeholder="Título" required class="form-control" type="text" id="titulo" />
                </div>
                <div class="form-group">
                    <label>Descrição:</label>
                    <textarea placeholder="Descrição" required class="form-control" id="descricao"></textarea>
                </div>
                <div class="form-group">
                    <label>Data de validade:</label>
                    <input type="text" name="data_validade" id="data_validade" class='form-control' />
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_aviso">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){

        $.datetimepicker.setLocale('pt-BR');
        $("#data_validade").datetimepicker({
            format:'d/m/Y',
            timepicker:false
        });

		$("#cadastrar_aviso").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/avisos/ajax_cadastro_aviso", {
                administradores: $("#destino").val(),
                titulo: $("#titulo").val(),
                data_validade: $("#data_validade").val(),
                descricao:$("#descricao").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$('#cadastro_aviso').modal('hide');
					$("#modal_cadastro_aviso_erro").html("");
                    location.reload();
				}else
		        	$("#modal_cadastro_aviso_erro").html(result);
		    });
		});
	});
</script>