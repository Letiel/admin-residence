<div class="modal fade" id="reenviar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Reenviar Login e Senha</h4>
                <h5 class="text-danger" id="modal_reenviar_erro"></h5>
            </div>
            <div class="modal-body">
                <?php if ($usuario->email == null): ?>
                    <div class="form-group">
                        <label>Informe um email para enviar os dados:</label>
                        <input placeholder="Email" required class="form-control" type="text" id="email_reenviar" />
                    </div>
                <?php else: ?>
                    <div class="form-group">
                        <h3 class="text-center">Dados serão enviados para o email <b><?= $usuario->email ?></b></h3>
                        <input placeholder="Email" value="<?= $usuario->email ?>" class="form-control" type="hidden" id="email_reenviar" />
                    </div>
                    
                <?php endif ?>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="reenviarDados">Enviar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#reenviar").modal("show");             

        $("#reenviarDados").click(function () {
            item = $(this);            
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");

            $.post("/moradores/ajax_reenviar_login_senha", {                
                email: $("#email_reenviar").val(),
                id: <?= $usuario->id ?>,
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $('#reenviar').modal('hide');
                    $("#modal_reenviar_erro").html("");
                    swal({
                        title: "Dados enviados!",
                        text: "Novo login e senha enviados com sucesso!",
                        type: "success"
                    });
                    // location.reload();
                }else
                    $("#modal_reenviar_erro").html(result);
            });
        });
    });
</script>