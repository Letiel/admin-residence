<a class="btn btn-info btn-outline input-group-addon" data-toggle="modal" data-target="#cadastro_residencia"><i class="fa fa-plus"></i></a>
<div class="modal fade" id="cadastro_residencia" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar residência</h4>
                <h5 class="text-danger" id="modal_cadastro_residencia_erro"></h5>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<label>Localização:</label>
            		<select required class="form-control" id="localizacao_residencia">
        			    <option value="">Selecione...</option>
                        <?php foreach ($localizacoes as $localizacao): ?>
                            <option value="<?= $localizacao->id ?>"><?= $localizacao->nome ?></option>
                        <?php endforeach ?>
            		</select>
            	</div>

            	<div class="form-group">
            		<label>Nova residência</label>
            		<input required class="form-control" type="text" id="nome_residencia" />
            	</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_residencia">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cadastrar_residencia").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/moradores/ajax_cadastro_residencia", {
                localizacao: $("#localizacao_residencia").val(),
                nome: $("#nome_residencia").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$("#localizacao_residencia").val("");
					$("#nome_residencia").val("");
					$('#cadastro_residencia').modal('hide');
					$("#modal_cadastro_residencia_erro").html("");
					iniciar_residencias();
				}else
		        	$("#modal_cadastro_residencia_erro").html(result);
		    });
		});
	});
</script>