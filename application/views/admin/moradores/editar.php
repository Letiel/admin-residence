<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">

    <!-- X-EDITABLE -->
    <link rel="stylesheet" href="/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <style type="text/css">
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }
        .container-image {
            position: relative;
            width: 80%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
      }

      .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          width: 100%;
          height: 100%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%)
      }

      .container-image:hover .image {
          opacity: 0.3;
      }

      .container-image:hover .middle {
          opacity: 1;
      }

      .text {
          background-color: transparent;
          color: #333;
          border:none;
          width: 100%;
          height: 100%;
          box-shadow: none;
          font-size: 16px;
          padding: 16px 32px;
      }
  </style>

</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <button class="btn btn-info btn-outline btn-lg pull-right hidden-xs reenviar_login_senha" value="<?= $usuario->id ?>"> Reenviar Usuário e Senha</button>
                    <h2 class="font-light m-b-xs">
                        Editar Usuário: <?= $usuario->nome ?>
                    </h2>
                    <button class="btn btn-info btn-outline btn-lg btn-block visible-xs reenviar_login_senha" value="<?= $usuario->id ?>"> Reenviar Usuário e Senha</button>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <h4 class="text-info text-center" style="margin-bottom: 25px;"><strong>Dica! </strong>Clique sobre a informação que deseja alterar.</h4>
                            <form action="/moradores/cadastrar" method="post">
                                <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                                  <div class="container-image">
                                      <img src="<?= ($usuario->foto ? 'https://residence.acessonaweb.com.br/imagens/moradores/'.$usuario->foto : '//gravatar.com/avatar/0?d=mm&s=150') ?>" alt="Usuário" class="image" style="width:100%" id="avatar2" >
                                      <div class="middle">
                                          <button class="text" type="button" data-ip-modal="#avatarModal">
                                            <i class="pe-7s-camera fa-2x"></i><br>
                                            Selecionar Imagem</button>
                                            <input type="hidden" name="foto" id="url_imagem" value="<?= set_value('foto') ?>">
                                        </div>
                                    </div>  

                                </div>

                                <div class="clearfix visible-sm visible-xs"></div>
                                <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                    <label>Nome:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="nome" data-type="text" data-pk="<?= $usuario->id ?>" data-name="nome" data-title="Nome" class="editable editable-click editable-padding"><?= $usuario->nome ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>CPF:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cpf" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cpf" data-title="CPF" class="editable-padding"><?= $usuario->cpf ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>RG:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="rg" data-type="text" data-pk="<?= $usuario->id ?>" data-name="rg" data-title="RG" class="editable editable-click editable-padding"><?= $usuario->rg ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Tipo de usuário:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="usuario_tipo" data-name="usuario_tipo" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Tipo de usuário" class="editable-padding" style="color: gray;"></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Sub-tipo:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="tipo" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Sub-tipo" class="editable-padding col-md-12" style="color: gray;"></a>
                                    <?php //$this->load->view("admin/moradores/modals/cadastro_tipo") ?>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Localização:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="localizacao" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Localização" class="editable-padding col-md-9" style="color: gray;"></a>
                                    <?php $this->load->view("admin/moradores/modals/cadastro_localizacao") ?>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Residência:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="residencia" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Residência" class="editable-padding col-md-9" style="color: gray;"></a>
                                    <?php $this->load->view("admin/moradores/modals/cadastro_residencia") ?>
                                </div>
                                <!-- <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Naturalidade:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="naturalidade" data-type="text" data-pk="<?= $usuario->id ?>" data-name="naturalidade" data-title="Naturalidade" class="editable editable-click editable-padding"><?= $usuario->naturalidade ?></a>
                                </div> -->
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Sexo:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="sexo" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Sexo" class="editable-padding"></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Data de nascimento:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="data_nascimento" data-type="text" data-pk="<?= $usuario->id ?>" data-name="data_nascimento" data-title="Data de nascimento" class="editable-padding"><?= $usuario->data_nascimento ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Email:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="email" data-type="text" data-pk="<?= $usuario->id ?>" data-name="email" data-title="Email" class="editable editable-click editable-padding"><?= $usuario->email ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Fone:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="fone" data-type="text" data-pk="<?= $usuario->id ?>" data-name="fone" data-title="Fone" class="editable-padding"><?= $usuario->fone ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Cel1:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cel1" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cel1" data-title="Cel 1" class="editable-padding"><?= $usuario->cel1 ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Cel2:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cel2" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cel2" data-title="Cel 2" class="editable-padding"><?= $usuario->cel2 ?></a>
                                </div>
                                <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Endereço:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="endereco" data-type="text" data-pk="<?= $usuario->id ?>" data-name="endereco" data-title="Endereço" class="editable editable-click editable-padding"><?= $usuario->endereco ?></a>
                                </div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Complemento:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="complemento" data-type="text" data-pk="<?= $usuario->id ?>" data-name="complemento" data-title="Complemento" class="editable editable-click editable-padding"><?= $usuario->complemento ?></a>
                                </div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Bairro:</label>
                                    <a href="#" id="bairro" data-type="text" data-pk="<?= $usuario->id ?>" data-name="bairro" data-title="Bairro" class="editable editable-click editable-padding"><?= $usuario->bairro ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>Cidade:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cidade" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cidade" data-title="Cidade" class="editable editable-click editable-padding"><?= $usuario->cidade ?></a>
                                </div>
                                <div class="col-md-1 form-group animated-panel zoomIn">
                                    <label>UF:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="uf" data-type="text" data-pk="<?= $usuario->id ?>" data-name="uf" data-title="UF" class="editable-padding"><?= $usuario->uf ?></a>
                                </div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>CEP:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cep" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cep" data-title="CEP" class="editable-padding"><?= $usuario->cep ?></a>
                                </div> -->
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Situação:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="situacao" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Situação" class="editable-padding"></a>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Obs da situação:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="obs_situacao" data-type="textarea" data-pk="<?= $usuario->id ?>" data-name="obs_situacao" data-title="Obs da situação" class="editable editable-click editable-padding"><?= $usuario->obs_situacao ?></a>
                                </div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Obs da administracao:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="obs_administracao" data-type="textarea" data-pk="<?= $usuario->id ?>" data-name="obs_administracao" data-title="Obs da administração" class="editable editable-click editable-padding"><?= $usuario->obs_administracao ?></a>
                                </div>
                                <div class="col-md-4 form-group animated-panel zoomIn">
                                    <label>Obs da portaria:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="obs_portaria" data-type="textarea" data-pk="<?= $usuario->id ?>" data-name="obs_portaria" data-title="Obs da portaria" class="editable editable-click editable-padding"><?= $usuario->obs_portaria ?></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
    </div>
    <!-- Avatar Modal -->
    <div class="ip-modal" id="avatarModal">
        <div class="ip-modal-dialog">
            <div class="ip-modal-content">
                <div class="ip-modal-header">
                    <a class="ip-close" title="Close">&times;</a>
                    <h4 class="ip-modal-title">Enviar imagem</h4>
                </div>
                <div class="ip-modal-body">
                    <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                    <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                    <button type="button" class="btn btn-info ip-edit">Editar</button>
                    <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                    <div class="alert ip-alert"></div>
                    <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                    <div class="ip-preview"></div>
                    <div class="ip-rotate">
                        <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                        <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                    </div>
                    <div class="ip-progress">
                        <div class="text">Enviando</div>
                        <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                    </div>
                </div>
                <div class="ip-modal-footer">
                    <div class="ip-actions">
                        <button type="button" class="btn btn-success ip-save">Salvar</button>
                        <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                        <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                    </div>
                    <button type="button" class="btn btn-default ip-close">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Modal -->
    <div class="container_modal"></div>
    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <!-- JavaScript CROPP -->
    <script src="/assets/js/jquery.Jcrop.min.js"></script>
    <script src="/assets/js/jquery.imgpicker.js"></script>
    <script src="/scripts/jquery.mask.min.js"></script>
    <script src="/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
    	$(".reenviar_login_senha").click(function(){
    		item = $(this);
    		anterior = item.html();
    		item.html("<i class='fa fa-cog fa-spin'></i>");
    		$.post("/moradores/ReenviarLoginSenha", {
    			id: $(this).val()
    		}, function(result){
    			item.html(anterior);
    			$(".container_modal").html(result);
    		});
    	});
        var subTipos;

        function atualizar_residencias(newValue){
            $.post("/moradores/editable_atualizar_residencias", {
                localizacao: newValue
            }, function(result){
                subTipos = result;
                $('#residencia').editable({
                    source:
                    JSON.parse(subTipos)
                    ,
                    ajaxOptions: {
                        dataType: 'json'
                    },
                    emptytext: 'Não informado',
                    success: function(response, newValue) {
                        if(!response) {
                            return "Erro desconhecido";
                        }

                        if(response.success === false) {
                            return response.msg;
                        }
                    }
                });
                $("#residencia").editable("show");
            });
        }

        $(function() {
        // Avatar setup
        $('#avatarModal').imgPicker({
            url: '/server/upload_avatar.php',
            aspectRatio: 1,
            data: {
                uniqid: '<?= ($usuario->foto != null && $usuario->foto != "" ? $usuario->foto : uniqid()) ?>',
                id: <?= $usuario->id ?>
            },
            deleteComplete: function() {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                $.post("/moradores/atualizar_foto", {
                    foto: "",
                    id: <?= $usuario->id ?>
                }, function(result){

                });
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                [(image.width-image.height)/2, 0, image.height, image.height] :
                [0, (image.height-image.width)/2, image.width, image.width];      
                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                $('#avatar2').attr('src', "https://residence.acessonaweb.com.br/imagens/moradores/"+image.name);
                $.post("/moradores/atualizar_foto", {
                    foto: image.name,
                    id: <?= $usuario->id ?>
                }, function(result){
                    location.reload();
                });
                this.modal('hide');
            }
        });


        $("#cpf").mask("000.000.000-00");

        var maskBehavior = function (val) {
         return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
     },
     options = {onKeyPress: function(val, e, field, options) {
         field.mask(maskBehavior.apply({}, arguments), options);
     }
 };

 $('.fone').mask(maskBehavior, options);
        // $("#fone1").mask("(00)00000-000?0");
        // $("#cel1").mask("(00)00000-000?0");
        // $("#cel2").mask("(00)00000-000?0");
        $("#data_nascimento").mask("00/00/0000");
        $("#cep").mask("00000-000");

        $("#usuario_tipo").change(function(){
            atualizar_subTipos();
        });

        $.fn.editable.defaults.mode = 'popup';
        $.fn.editable.defaults.url = '/moradores/editable';

        $('.editable').editable({
            emptytext: 'Não informado',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#cpf').editable({
            tpl:'   <input type="text" id ="editable_cpf" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#editable_cpf").mask("999.999.999-99");
        });

        $('#data_nascimento').editable({
            tpl:'   <input type="text" id ="editable_data_nascimento" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#editable_data_nascimento").mask("99/99/9999");
        });

        $('#fone, #cel1, #cel2').editable({
            tpl:'   <input type="text" id ="editable_fone" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            var maskBehavior = function (val) {
             return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
         },
         options = {onKeyPress: function(val, e, field, options) {
             field.mask(maskBehavior.apply({}, arguments), options);
         }
     };
     $('#editable_fone').mask(maskBehavior, options);
 });

        $('#cep').editable({
            tpl:'   <input type="text" id ="cep" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#cep").mask("99999-999");
        });

        $('#uf').editable({
            emptytext: '??',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#usuario_tipo').editable({
            value: "<?= $usuario->usuario_tipo ?>",
            source: [
            {value: 'USUARIO', text: 'Morador'},
            {value: 'PRESTADOR', text: 'Prestador'},
            {value: 'COLABORADOR', text: 'Colaborador'},
            {value: 'VISITANTE', text: 'Visitante'}
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#tipo').editable("destroy");
                atualizar_subTipos(newValue);
            }
        });

        iniciar_subTipos();

        function atualizar_subTipos(newValue){
            $.post("/moradores/editable_atualizar_subTipos", {
                tipo: newValue
            }, function(result){
                subTipos = result;
                $('#tipo').editable({
                    source:
                    JSON.parse(subTipos)
                    ,
                    ajaxOptions: {
                        dataType: 'json'
                    },
                    emptytext: 'Não informado',
                    success: function(response, newValue) {
                        if(!response) {
                            return "Erro desconhecido";
                        }

                        if(response.success === false) {
                            return response.msg;
                        }
                    }
                });
                $("#tipo").editable("show");
            });
        }

        $('#localizacao').editable({
            value: "<?= $usuario->localizacao ?>",
            source: [
            <?php
            $i = 0;
            foreach ($localizacoes as $localizacao): ?>
            {value: "<?= $localizacao->id ?>", text: "<?= $localizacao->nome ?>"}<?= ($i < sizeof($localizacoes)-1 ? "," : "") ?>
            <?php
            $i++;
            endforeach ?>
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#residencia').editable("destroy");
                atualizar_residencias(newValue);
            }
        });
        iniciar_residencias();

        $('#sexo').editable({
            value: "<?= $usuario->sexo ?>",
            source: [
            {value: 'M', text: 'Masculino'},
            {value: 'F', text: 'Feminino'},
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#situacao').editable({
            value: "<?= $usuario->situacao ?>",
            source: [
            {value: '1', text: 'Liberado'},
            {value: '0', text: 'Bloqueado'},
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });

function iniciar_subTipos(){
    $('#tipo').editable("destroy");
    $.post("/moradores/editable_atualizar_subTipos", {
        tipo: "<?= $usuario->usuario_tipo ?>",
    }, function(result){
        subTipos = result;
        $('#tipo').editable({
            value: "<?= $usuario->tipo ?>",
            source:
            JSON.parse(subTipos)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione um sub-tipo.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });
}

function iniciar_localizacoes(){
    $('#localizacao').editable("destroy");
    $.post("/moradores/editable_atualizar_localizacoes", {}, function(result){
        localizacoes = result;
        $('#localizacao').editable({
            value: "<?= $usuario->localizacao ?>",
            source:
            JSON.parse(localizacoes)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione uma localização.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#residencia').editable("destroy");
                atualizar_residencias(newValue);
            }
        });
    });
}

function iniciar_residencias(){
    $('#residencia').editable("destroy");
    $.post("/moradores/editable_atualizar_residencias", {
        localizacao: $("#localizacao").editable("getValue").localizacao,
    }, function(result){
        localizacoes = result;
        $('#residencia').editable({
            value: "<?= $usuario->residencia ?>",
            source:
            JSON.parse(localizacoes)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione uma residência.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });
}
</script>
</body>
</html>