<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    
    <!-- TABELA -->
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <a class='btn btn-info btn-lg btn-outline pull-right hidden-xs' href="/usuarios/cadastrar"><i class="fa fa-plus"></i> Cadastrar Usuário</a>
                    <h2 class="font-light m-b-xs">
                        Usuários
                    </h2>
                    <a class='btn btn-info btn-lg btn-outline btn-block visible-xs' href="/usuarios/cadastrar"><i class="fa fa-plus"></i> Cadastrar Usuário</a>
                    <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_morador") ?></h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/moradores" method="get">
                                <div class="input-group">
                                    <input name="k" class="form-control" type="text" placeholder="Pesquisar moradores...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-body">
                            
                            
                            <table id="tbl_moradores" class="footable table table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Residência</th>
                                        <th>Tipo</th>
                                        <th data-hide="phone,tablet">Situação</th>
                                        <th data-hide="phone">Edição</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($moradores as $morador): ?>
                                        <tr>
                                            <td><?= $morador->nome ?></td>
                                            <td><?= ($morador->nome_residencia != null && $morador->nome_residencia != "" ? $morador->nome_residencia : "") ?>  <?= ($morador->nome_localizacao != null && $morador->nome_localizacao != "" ? $morador->nome_localizacao : "") ?></td>
                                            <td><?= $morador->tipo ?></td>
                                            <td><?= ($morador->situacao ? "Liberado" : "Bloqueado") ?></td>
                                            <td><a href="/usuarios/editar/<?= $morador->id ?>" class="btn btn-success btn-outline"><span class="fa fa-pencil"></span></a> <button value="<?= $morador->id ?>" class="btn btn-danger btn-outline deletar"><span class="fa fa-trash"></span></button></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <div class="btn-group pull-right">
                                <?= $paginacao ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#tbl_moradores').footable({ paginate:false });
            $(".deletar").click(function(){
                if(confirm("Tem certeza?")){
                    item = $(this);
                    anterior = item.html();
                    item.html("<i class='fa fa-cog fa-spin'></i>");
                    $.post("/moradores/deletar", {
                        id: $(this).val()
                    }, function(result){
                        item.html(anterior);
                        location.reload();                  
                    });
                }
            });
        });
    </script>
</body>
</html>