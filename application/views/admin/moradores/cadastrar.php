<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">

    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/select2-bootstrap.css">
    <style type="text/css">
        .container-image {
            position: relative;
            width: 100%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
      }

      .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          width: 100%;
          height: 100%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%)
      }

      .container-image:hover .image {
          opacity: 0.3;
      }

      .container-image:hover .middle {
          opacity: 1;
      }

      .text {
          background-color: transparent;
          color: #333;
          border:none;
          width: 100%;
          height: 100%;
          box-shadow: none;
          font-size: 16px;
          padding: 16px 32px;
      }
  </style>
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">

                    <h2 class="font-light m-b-xs">
                        Cadastrar Usuário
                    </h2>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/moradores/cadastrar" method="post">
                                <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12" style="padding: 0 30px">
                                  <div class="container-image">
                                    <img src="<?= (set_value('foto') ? set_value('foto') : '//gravatar.com/avatar/0?d=mm&s=150') ?>" alt="Morador" class="image" style="width:100%" id="avatar2" >
                                    <div class="middle">
                                      <button class="text" type="button" data-ip-modal="#avatarModal">
                                        <i class="pe-7s-camera fa-2x"></i><br>
                                        Selecionar Imagem</button>
                                        <input type="hidden" name="foto" id="url_imagem" value="<?= set_value('foto') ?>">
                                    </div>
                                </div>  
                                <button  data-ip-modal="#avatarModal" class="btn btn-primary btn-block"><i class="fa fa-camera "></i> <span>Selecionar Imagem</span></button>
                            </div>

                            <div class="clearfix visible-sm visible-xs"></div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Nome:</label>
                                <input class="form-control" required value="<?= set_value('nome')  ?>" name="nome" placeholder="Nome" />
                                <span class="text-danger"><?= form_error("nome") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>CPF:</label>
                                <input class="form-control" id="cpf" value="<?= set_value('cpf')  ?>" name="cpf" placeholder="" />
                                <span class="text-danger"><?= form_error("cpf") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>RG:</label>
                                <input class="form-control" value="<?= set_value('rg')  ?>" name="rg" placeholder="" />
                                <span class="text-danger"><?= form_error("rg") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Tipo de usuário:</label>
                                <select required name="usuario_tipo" id="usuario_tipo" class="form-control">
                                    <option value="">Selecione...</option>
                                    <option <?= set_select("usuario_tipo", "USUARIO") ?> value="USUARIO">Morador</option>
                                    <option <?= set_select("usuario_tipo", "PRESTADOR") ?> value="PRESTADOR">Prestador</option>
                                    <option <?= set_select("usuario_tipo", "COLABORADOR") ?> value="COLABORADOR">Colaborador</option>
                                    <option <?= set_select("usuario_tipo", "VISITANTE") ?> value="VISITANTE">Visitante</option>
                                </select>
                                <span class="text-danger"><?= form_error("usuario_tipo") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Sub-tipo:</label>
                                <div style="width: 100%;" class="input-group">
                                    <select required id="tipos" name="tipo" class="form-control">
                                        <option value="">Selecione</option>
                                    </select>
                                    <!-- <?php $this->load->view("admin/moradores/modals/cadastro_tipo") ?> -->
                                </div>
                                <span class="text-danger"><?= form_error("tipo") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Localização:</label>
                                <div class="input-group">
                                    <select required id="localizacao" name="localizacao" class="form-control">
                                        <option value="">Selecione</option>
                                        <?php foreach ($localizacoes as $localizacao): ?>
                                            <option <?= set_select("localizacao", $localizacao->id) ?> value="<?= $localizacao->id ?>"><?= $localizacao->nome ?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <?php $this->load->view("admin/moradores/modals/cadastro_localizacao") ?>
                                </div>
                                <span class="text-danger"><?= form_error("localizacao") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Residência:</label>
                                <div class="input-group">
                                    <select required id="residencia" name="residencia" class="form-control">
                                        <option value="">Selecione</option>
                                    </select>
                                    <?php $this->load->view("admin/moradores/modals/cadastro_residencia") ?>
                                </div>
                                <span class="text-danger"><?= form_error("localizacao") ?></span>
                            </div>
                            <!-- <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Naturalidade:</label>
                                <input class="form-control" value="<?= set_value('naturalidade')  ?>" name="naturalidade" placeholder="Naturalidade" />
                                <span class="text-danger"><?= form_error("naturalidade") ?></span>
                            </div> -->
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Sexo:</label>
                                <select required name="sexo" class="form-control">
                                    <option value="">Selecione...</option>
                                    <option <?= set_select("sexo", "M") ?> value="M">Masculino</option>
                                    <option <?= set_select("sexo", "F") ?> value="F">Feminino</option>
                                </select>
                                <span class="text-danger"><?= form_error("sexo") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Data de nascimento:</label>
                                <input value="<?= set_value('data_nascimento')  ?>" class="form-control" name="data_nascimento" id="data_nascimento" placeholder="" />
                                <span class="text-danger"><?= form_error("data_nascimento") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Email:</label>
                                <input type="email" value="<?= set_value('email')  ?>" class="form-control" name="email" placeholder="email@domínio.com" />
                                <span class="text-danger"><?= form_error("email") ?></span>
                            </div>
                            <div class="col-md-2 form-group animated-panel zoomIn">
                                <label>Fone:</label>
                                <input type="text" value="<?= set_value('fone')  ?>" class="form-control fone" name="fone" placeholder="(00)0000-0000" />
                                <span class="text-danger"><?= form_error("fone") ?></span>
                            </div>
                            <div class="col-md-2 form-group animated-panel zoomIn">
                                <label>Cel1:</label>
                                <input type="text" value="<?= set_value('cel1')  ?>" class="form-control fone" name="cel1" placeholder="(00)00000-0000" />
                                <span class="text-danger"><?= form_error("cel1") ?></span>
                            </div>
                            <div class="col-md-2 form-group animated-panel zoomIn">
                                <label>Cel2:</label>
                                <input type="text" value="<?= set_value('cel2')  ?>" class="form-control fone" name="cel2" placeholder="(00)00000-0000" />
                                <span class="text-danger"><?= form_error("cel2") ?></span>
                            </div>
                            <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Endereço:</label>
                                <input type="text" value="<?= set_value('endereco')  ?>" class="form-control" name="endereco" placeholder="Endereço" />
                                <span class="text-danger"><?= form_error("endereco") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Complemento:</label>
                                <input type="text" value="<?= set_value('complemento')  ?>" class="form-control" name="complemento" placeholder="Complemento" />
                                <span class="text-danger"><?= form_error("complemento") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Bairro:</label>
                                <input type="text" value="<?= set_value('bairro')  ?>" class="form-control" name="bairro" placeholder="Bairro" />
                                <span class="text-danger"><?= form_error("bairro") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cidade:</label>
                                <input type="text" value="<?= set_value('cidade')  ?>" class="form-control" name="cidade" placeholder="Cidade" />
                                <span class="text-danger"><?= form_error("cidade") ?></span>
                            </div>
                            <div class="col-md-1 form-group animated-panel zoomIn">
                                <label>UF:</label>
                                <input type="text" value="<?= set_value('uf')  ?>" maxlength="2" class="form-control" name="uf" placeholder="UF" />
                                <span class="text-danger"><?= form_error("uf") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>CEP:</label>
                                <input type="text" value="<?= set_value('cep')  ?>" class="form-control" id="cep" name="cep" placeholder="" />
                                <span class="text-danger"><?= form_error("cep") ?></span>
                            </div> -->
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Situação:</label>
                                <select required class='form-control' name="situacao">
                                    <option <?= set_select("situacao", "1") ?> value="1">Liberado</option>
                                    <option <?= set_select("situacao", "0") ?> value="0">Bloqueado</option>
                                </select>
                                <span class="text-danger"><?= form_error("situacao") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Obs da situação:</label>
                                <textarea name="obs_situacao" class='form-control' placeholder="Obs"><?= set_value('obs_situacao')  ?></textarea>
                                <span class="text-danger"><?= form_error("obs_situacao") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Obs da administracao:</label>
                                <textarea name="obs_administracao" class='form-control' placeholder="Obs"><?= set_value('obs_administracao')  ?></textarea>
                                <span class="text-danger"><?= form_error("obs_administracao") ?></span>
                            </div>
                            <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Obs da portaria:</label>
                                <textarea name="obs_portaria" class='form-control' placeholder="Obs"><?= set_value('obs_portaria')  ?></textarea>
                                <span class="text-danger"><?= form_error("obs_portaria") ?></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12">
                                <button class="btn btn-primary btn-lg pull-right">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
    <div class="ip-modal-dialog">
        <div class="ip-modal-content">
            <div class="ip-modal-header">
                <a class="ip-close" title="Close">&times;</a>
                <h4 class="ip-modal-title">Enviar imagem</h4>
            </div>
            <div class="ip-modal-body">
                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                <button type="button" class="btn btn-info ip-edit">Editar</button>
                <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                <div class="alert ip-alert"></div>
                <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                <div class="ip-preview"></div>
                <div class="ip-rotate">
                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                </div>
                <div class="ip-progress">
                    <div class="text">Enviando</div>
                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                </div>
            </div>
            <div class="ip-modal-footer">
                <div class="ip-actions">
                    <button type="button" class="btn btn-success ip-save">Salvar</button>
                    <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                </div>
                <button type="button" class="btn btn-default ip-close">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<!-- JavaScript CROPP -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<script src="/scripts/jquery.mask.min.js"></script>
<script src="/js/select2.full.min.js"></script>
<script>
    $(function() {
        // Avatar setup
        $('#avatarModal').imgPicker({
            url: '/server/upload_avatar.php',
            aspectRatio: 1,
            data: {uniqid: '<?php echo uniqid() ?>'},
            deleteComplete: function() {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                [(image.width-image.height)/2, 0, image.height, image.height] :
                [0, (image.height-image.width)/2, image.width, image.width];      
                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', "https://residence.acessonaweb.com.br/imagens/moradores/"+image.name );
                $('#url_imagem').attr('value', image.name );
                this.modal('hide');
            }
        });


        $("#cpf").mask("000.000.0000-00");

        var maskBehavior = function (val) {
         return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
     },
     options = {onKeyPress: function(val, e, field, options) {
         field.mask(maskBehavior.apply({}, arguments), options);
     }
 };

 $('.fone').mask(maskBehavior, options);
        // $("#fone1").mask("(00)00000-000?0");
        // $("#cel1").mask("(00)00000-000?0");
        // $("#cel2").mask("(00)00000-000?0");
        $("#data_nascimento").mask("00/00/0000");
        $("#cep").mask("00000-000");

        $("#usuario_tipo").change(function(){
            iniciar_subTipos();
        });

        $("#localizacao").change(function(){
            iniciar_residencias();
        });
    });

    function iniciar_subTipos(){
        $.post("/moradores/ajax_atualizar_subTipos", {
            tipo: $("#usuario_tipo").val(),
            selecionado: <?= (set_value("tipo") ? set_value("tipo") : 0) ?>
        }, function(result){
            $("#tipos").html(result);
        });
    }

    function iniciar_localizacoes(){
        $.post("/moradores/ajax_atualizar_localizacoes", {

        }, function(result){
            $("#localizacao").html(result);
            $("#localizacao_residencia").html(result);
        });
    }

    function iniciar_residencias(){
        $.post("/moradores/ajax_atualizar_residencias", {
            localizacao: $("#localizacao").val(),
            selecionado: <?= (set_value("residencia") ? set_value("residencia") : "\"\"") ?>
        }, function(result){
            $("#residencia").html(result);
        });
    }

    iniciar_subTipos();
    iniciar_residencias();
</script>
</body>
</html>