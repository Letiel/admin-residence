<a class="btn btn-info btn-outline input-group-addon" data-toggle="modal" data-target="#cadastro_tipo"><i class="fa fa-plus"></i></a>
<div class="modal fade" id="cadastro_tipo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar tipo de veículo</h4>
                <h5 class="text-danger" id="modal_cadastro_tipo_erro"></h5>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<label>Novo tipo:</label>
            		<input required class="form-control" type="text" id="nome" />
            	</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_tipo">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cadastrar_tipo").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/veiculos/ajax_cadastro_tipo", {
                nome: $("#nome").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$("#nome").val("");
					$('#cadastro_tipo').modal('hide');
					$("#modal_cadastro_tipo_erro").html("");
				}else
		        	$("#modal_cadastro_tipo_erro").html(result);
		    });
		});
	});
</script>