<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Editar Veículo | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">

    <!-- X-EDITABLE -->
    <link rel="stylesheet" href="/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />

    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />

    <style type="text/css">
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }

        .form-inline .select2-container--bootstrap{
            min-width: 150px !important;
        }
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }
        .container-image {
            position: relative;
            width: 80%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
      }

      .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          width: 100%;
          height: 100%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%)
      }

      .container-image:hover .image {
          opacity: 0.3;
      }

      .container-image:hover .middle {
          opacity: 1;
      }

      .text {
          background-color: transparent;
          color: #333;
          border:none;
          width: 100%;
          height: 100%;
          box-shadow: none;
          font-size: 16px;
          padding: 16px 32px;
      }
    </style>

</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">
    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <button class="btn btn-danger btn-lg btn-outline pull-right deletar hidden-xs" value="<?= $veiculo->id ?>" ><i class="fa fa-trash"></i> Excluir Veículo</button>
                <h2 class="font-light m-b-xs">
                    Veículo <?= $veiculo->placa ?>
                </h2>
                <button class="btn btn-danger btn-lg btn-outline btn-block visible-xs deletar" value="<?= $veiculo->id ?>" ><i class="fa fa-trash"></i> Excluir Veículo</button>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <h4 class="text-info text-center" style="margin-bottom: 25px;"><strong>Dica! </strong>Clique sobre a informação que deseja alterar.</h4>
                        <form action="/" method="post">
                            <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                                    <div class="container-image">
                                    <img src="<?= ($veiculo->foto ? 'https://residence.acessonaweb.com.br/imagens/veiculos/'.$veiculo->foto : 'https://residence.acessonaweb.com.br/images/veiculo.png') ?>" alt="Veículo" class="image" style="width:100%" id="avatar2" >
                                      <div class="middle">
                                        <button class="text" type="button" data-ip-modal="#avatarModal">
                                            <i class="pe-7s-camera fa-2x"></i><br>
                                            Alterar Imagem</button>
                                            <input type="hidden" name="foto" id="url_imagem" value="<?= $veiculo->foto ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm visible-xs" style="margin-bottom: 20px;"></div>
                            <div class="col-md-2 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Placa:</label>
                                <div class="clearfix"></div>
                                <a style="text-transform: uppercase;" href="#" id="placa" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="placa" data-title="Placa" class="editable-padding"><?= $veiculo->placa ?></a>
                            </div>
                            <div class="col-md-1 form-group animated-panel zoomIn">
                                <label>UF:</label>
                                <div class="clearfix"></div>
                                <a style="text-transform: uppercase;" href="#" id="uf" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="uf" data-title="uf" class="editable-padding"><?= $veiculo->uf ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cidade:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cidade" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="cidade" data-title="Cidade" class="editable editable-click editable-padding"><?= $veiculo->cidade ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Tipo:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="tipo" data-name="tipo" data-type="select2" data-pk="<?= $veiculo->id ?>" data-title="Tipo" class="editable-padding" style="color: gray;"><?= $veiculo->tipo_veiculo ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Fabricante:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="fabricante" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="fabricante" data-title="Fabricante" class="editable editable-click editable-padding"><?= $veiculo->fabricante ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Modelo:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="modelo" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="modelo" data-title="Modelo" class="editable editable-click editable-padding"><?= $veiculo->modelo ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cor:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cor" data-type="text" data-pk="<?= $veiculo->id ?>" data-name="cor" data-title="Cor" class="editable editable-click editable-padding"><?= $veiculo->cor ?></a>
                            </div>

                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Usuário responsável:</label>
                                <div class="clearfix"></div>
                                <select class='form-control' id="usuario_responsavel">
                                    <option value="<?= $veiculo->id_usuario ?>"><?= $veiculo->nome_usuario." ".$veiculo->nome_u_residencia." ".$veiculo->u_localizacao ?></option>
                                </select>
                            </div>

                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Residência:</label>
                                <div class="clearfix"></div>
                                <select class='form-control' id="residencia">
                                    <option value="<?= $veiculo->id_residencia ?>"><?= $veiculo->nome_residencia." ".$veiculo->localizacao ?></option>
                                </select>
                            </div>
                            
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Situação:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="situacao" data-type="select" data-pk="<?= $veiculo->id ?>" data-title="Situação" class="editable-padding"></a>
                            </div>
                            <div class="col-md-offset-3 col-md-9 form-group animated-panel zoomIn">
                                <label>Situação obs:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="obs_situacao" data-type="textarea" data-pk="<?= $veiculo->id ?>" data-name="obs_situacao" data-title="Obs da situação" class="editable editable-click editable-padding"><?= $veiculo->obs_situacao ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
    <div class="ip-modal-dialog">
        <div class="ip-modal-content">
            <div class="ip-modal-header">
                <a class="ip-close" title="Close">&times;</a>
                <h4 class="ip-modal-title">Enviar imagem</h4>
            </div>
            <div class="ip-modal-body">
                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                <button type="button" class="btn btn-info ip-edit">Editar</button>
                <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                <div class="alert ip-alert"></div>
                <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                <div class="ip-preview"></div>
                <div class="ip-rotate">
                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                </div>
                <div class="ip-progress">
                    <div class="text">Enviando</div>
                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                </div>
            </div>
            <div class="ip-modal-footer">
                <div class="ip-actions">
                    <button type="button" class="btn btn-success ip-save">Salvar</button>
                    <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                </div>
                <button type="button" class="btn btn-default ip-close">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<!-- JavaScript CROPP -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<script src="/scripts/jquery.mask.min.js"></script>
<script src="/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<!-- SELECT2 -->
<script src="/js/select2.full.min.js"></script>
<script>
    $(function() {
        // Avatar setup
        $('#avatarModal').imgPicker({
            url: '/server/upload_avatar_veiculo.php',
            aspectRatio: 1,
            data: {
                uniqid: '<?= ($veiculo->foto != null && $veiculo->foto != "" ? $veiculo->foto : uniqid()) ?>',
                id: <?= $veiculo->id ?>
            },
            deleteComplete: function() {
                $('#avatar2').attr('src', 'https://residence.acessonaweb.com.br/images/veiculo.png');
                $.post("/veiculos/atualizar_foto", {
                    foto: "",
                    id: <?= $veiculo->id ?>
                }, function(result){
                    
                });
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                        [(image.width-image.height)/2, 0, image.height, image.height] :
                        [0, (image.height-image.width)/2, image.width, image.width];      
                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', 'https://residence.acessonaweb.com.br/images/veiculo.png');
                $('#avatar2').attr('src', "https://residence.acessonaweb.com.br/imagens/veiculos/"+image.name);
                console.log(image.name);
                $.post("/veiculos/atualizar_foto", {
                    foto: image.name,
                    id: <?= $veiculo->id ?>
                }, function(result){
                    $('#avatar2').attr('src', "https://residence.acessonaweb.com.br/imagens/veiculos/"+image.name);
                    location.reload();
                });
                this.modal('hide');
            }
        });

        var maskBehavior = function (val) {
         return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {onKeyPress: function(val, e, field, options) {
         field.mask(maskBehavior.apply({}, arguments), options);
         }
        };

        $.fn.editable.defaults.mode = 'popup';
        $.fn.editable.defaults.url = '/veiculos/editable';

        $('.editable').editable({
            emptytext: 'Não informado',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#uf').editable({
            tpl:"<input type='text' id='editable_uf' maxlength='2' />",
            emptytext: '--',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#placa').editable({
            tpl:'<input type="text" id ="editable_placa" class="form-control input-sm" style="padding-right: 24px;">',
            emptytext: 'Não informado',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#editable_placa").mask("SSS 0000");
        });

        $('#tipo').editable({
            tpl:'   <select id ="editable_tipo" class="form-control input-sm" style="padding-right: 24px; min-width: 150px;"></select>',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }else{
                    location.reload();
                }
            }
        }).on('shown',function(){
            $("#editable_tipo").select2({
                theme: "bootstrap",
                minimumInputLength: 0,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                      // args.minimum is the minimum required length
                      // args.input is the user-typed text
                      return "Digite mais...";
                    },
                    inputTooLong: function(args) {
                      // args.maximum is the maximum allowed length
                      // args.input is the user-typed text
                      return "Excesso de caracteres";
                    },
                    errorLoading: function() {
                      return "Digite...";
                    },
                    loadingMore: function() {
                      return "Carregando";
                    },
                    noResults: function() {
                      return "Nenhum resultado";
                    },
                    searching: function() {
                      return "Procurando...";
                    },
                    maximumSelected: function(args) {
                      // args.maximum is the maximum number of items the user may select
                      return "Erro ao carregar resultados";
                    }
                },
                ajax: {
                    url: "/veiculos/select2_listaTipos",
                    dataType: "json",
                    type: "post",
                    data: function (params) {

                        var queryParameters = {
                            term: params.term
                        }
                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.nome,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
            $("#editable_tipo").empty().append('<option value="<?= $veiculo->tipo ?>"><?= ($veiculo->tipo_veiculo != "" ? $veiculo->tipo_veiculo : "") ?></option>').val('<?= $veiculo->tipo ?>').trigger('change');
        });

        $("#usuario_responsavel").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/veiculos/select2_listaUsuarios",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome +" "+item.localizacao+" "+item.residencia,
                                id: item.id
                            }
                        })
                    };
                }
            }
        }).on("change", function(){
            $.post("/veiculos/editable_trocar_usuario_responsavel", {
                usuario_responsavel: $("#usuario_responsavel").val(),
                veiculo: <?= $veiculo->id ?>
            }, function(result, status){
                if(status != "success" && result != ""){
                    alert("Ocorreu um erro ao realizar alteração. Entre em contato com os administradores do sistema.");
                }
            });
        });

        $("#residencia").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/veiculos/select2_listaResidencias",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome +" "+item.localizacao,
                                id: item.id
                            }
                        })
                    };
                }
            }
        }).on("change", function(){
            $.post("/veiculos/editable_trocar_residencia", {
                residencia: $("#residencia").val(),
                veiculo: <?= $veiculo->id ?>
            }, function(result, status){
                if(status != "success" && result != ""){
                    alert("Ocorreu um erro ao realizar alteração. Entre em contato com os administradores do sistema.");
                }
            });
        });

        $('#situacao').editable({
            value: "<?= $veiculo->situacao ?>",
            source: [
                {value: '1', text: 'Liberado'},
                {value: '0', text: 'Bloqueado'},
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });
</script>
<script type="text/javascript">

    $('.deletar').click(function () {
        var value = $(this).val();
        swal({
            title: "Tem certeza?",
            text: "Você tem certeza que quer excluir este veículo?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, quero excluir!",
            cancelButtonText: "Cancelar",
        },
        function () {
         $.post( "/veiculos/deletar", {id: value}, function( data ) {
          location.reload();
      });
     });
    });
</script>
</body>
</html>