<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Veículos | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    
    <!-- TABELA -->
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <a class='btn btn-info btn-lg btn-outline pull-right hidden-xs' href="/veiculos/cadastrar"><i class="fa fa-plus"></i> Cadastrar Veículo</a>
                    <h2 class="font-light m-b-xs">
                        Veículos
                    </h2>
                    <a class='btn btn-info btn-lg btn-outline btn-block visible-xs' href="/veiculos/cadastrar"><i class="fa fa-plus"></i> Cadastrar Veículo</a>
                    <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_veiculo") ?></h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/veiculos" method="get">
                                <div class="input-group">
                                    <input name="k" class="form-control" type="text" placeholder="Pesquisar veículos...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php if ($veiculos != null): ?>
                            <?php foreach ($veiculos as $veiculo): ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10 forum-heading  m-b-xs">                                            
                                            <a href="/veiculos/editar/<?= $veiculo->id?>"><h4 style="text-transform: uppercase;"><b>Placa</b> <?= $veiculo->placa ?></h4></a>
                                            <div class="desc"><b>Veículo: </b><?= ($veiculo->fabricante != "" ? $veiculo->fabricante : "").($veiculo->modelo != "" ? " - ".$veiculo->modelo : "").($veiculo->cor != "" ? " [ ".$veiculo->cor." ]" : "") ?> <?= $veiculo->tipo_veiculo ?></div>
                                            <div class="desc"><b>Responsável: </b><?= ($veiculo->usuario != "" ? $veiculo->usuario : "").($veiculo->residencia != "" ? " - ".$veiculo->residencia : "").($veiculo->localizacao != "" ? " [ ".$veiculo->localizacao." ]" : "") ?></div>
                                            <div class="desc">
                                                <br class="hidden-xs">
                                                <?= ($veiculo->situacao ? '<span class="label label-success m-b-xs">Liberado</span>' : '<span class="label label-danger m-b-xs">Bloqueado</span>') ?></div>
                                        </div>
                                        <div class="col-md-1 forum-info m-b-xs">
                                            <a href="/veiculos/editar/<?= $veiculo->id ?>" class="btn btn-primary btn-block btn-outline"><span class="fa fa-pencil"></span></a> 
                                        </div>
                                        <div class="col-md-1 forum-info m-b-xs">
                                            <button value="<?= $veiculo->id ?>" class="btn btn-danger btn-block btn-outline deletar"><span class="fa fa-trash"></span></button>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                            <?php if ($paginacao): ?>
                                <div class="panel-body">
                                    <center><?= $paginacao ?></center>
                                </div>
                            <?php endif ?>
                        <?php else: ?>
                            <div class="panel-body">
                                <div class="row">
                                    <h3 class="text-center text-info">Nenhum Veículo</h3>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#tbl_veiculos').footable({ paginate:false });

            // $(".deletar").click(function(){
            //     if(confirm("Tem certeza?")){
            //         $.post("/veiculos/deletar", {
            //             id: $(this).val()
            //         }, function(result){
            //             console.log(result);
            //             location.reload();
            //         });
            //     }
            // });

            $('.deletar').click(function () {
                var value = $(this).val();
                var item = $(this);
                var anterior = item.html();
                swal({
                    title: "Tem certeza?",
                    text: "Você tem certeza que quer excluir este veículo?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero excluir!",
                    cancelButtonText: "Cancelar",
                },
                function () {
                item.html("<i class='fa fa-cog fa-spin'></i>");
                 $.post( "/veiculos/deletar", {id: value}, function( data ) {
                    item.html(anterior);
                    location.reload();
              });
             });
            });
        });
    </script>
</body>
</html>