<div class="modal fade" id="edicao_box" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title"><?= $reserva->nome_area ?></h4>
            </div>
            <div class="modal-body">
                <h4>Morador: <?= $reserva->nome_usuario. " - Residência ".$reserva->nome_residencia." ".$reserva->nome_localizacao ?></h4>
                <h4>Data: <?= date("d/m/Y", strtotime($reserva->hora_inicio))." às ".date("H:i", strtotime($reserva->hora_inicio)) ?></h4>
                <h4>Até: <?= date("d/m/Y", strtotime($reserva->hora_fim))." às ".date("H:i", strtotime($reserva->hora_fim)) ?></h4>
                <?php if (strtotime($reserva->hora_inicio) > strtotime(date("Y-m-d H:i:s"))): ?>
                <?= $reserva->autorizado ? "<button value='$reserva->id' class='btn btn-danger btn-block autorizacao'>Desautorizar</button>" : "<button value='$reserva->id' class='btn btn-success btn-block autorizacao'>Autorizar</button>" ?>
                <?php endif ?>
                <?php if ($convidados != null){ ?>
                    <a href="/reservas/imprimir/<?= $reserva->id ?>  " target='_blank' class="btn btn-success btn-block btn-outline pull-right">Imprimir lista de convidados</a>
                    <table class="table table-stripped">
                        <tr>
                            <th colspan='2'><h3 class='text-center text-info'>Convidados</h3></th>
                        </tr>
                        <tr>
                            <th>Nome</th>
                            <th>RG</th>
                        </tr>
                        <?php foreach ($convidados as $convidado): ?>
                            <tr>
                                <td><?= $convidado->nome ?></td>
                                <td><?= $convidado->rg ?></td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                <?php }else{
                    ?>
                    <h3 class="text-info text-center">Nenhum convidado</h3>
                    <?php
                } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#edicao_box").modal("show");
        $(".autorizacao").click(function(){
            if(confirm("Autorizar / Desautorizar?")){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/reservas/autorizar", {
                    id: <?= $reserva->id ?>
                }, function(result){
                    item.html(anterior);
                    location.reload();
                });
            }
        });
    });
</script>