<!DOCTYPE html>
<html>
<head>
	<title>Imprimir</title>
	<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	<h4>Morador: <?= $reserva->nome_usuario. " - Residência ".$reserva->nome_residencia." ".$reserva->nome_localizacao ?></h4>
	<h4>Data: <?= date("d/m/Y", strtotime($reserva->hora_inicio))." às ".date("H:i", strtotime($reserva->hora_inicio)) ?></h4>
	<h4>Até: <?= date("d/m/Y", strtotime($reserva->hora_fim))." às ".date("H:i", strtotime($reserva->hora_fim)) ?></h4>
	<?php if ($convidados != null){ ?>
	    <table class="table table-bordered">
	        <tr>
	            <th colspan='4'><h3 class='text-center text-info'>Convidados</h3></th>
	        </tr>
	        <tr>
	            <th>Nome</th>
	            <th>RG</th>
	            <th>Assinatura</th>
	            <th>Presente</th>
	        </tr>
	        <?php foreach ($convidados as $convidado): ?>
	            <tr>
	                <td><?= $convidado->nome ?></td>
	                <td><?= $convidado->rg ?></td>
	                <td></td>
	                <td></td>
	            </tr>
	        <?php endforeach ?>
	    </table>
    <?php } ?>
</body>
<script type="text/javascript">
	window.print();
</script>
</html>