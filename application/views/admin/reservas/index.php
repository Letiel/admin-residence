<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    Calendário
                </div>
                <div class="panel-body">
                    <h2 class="text-info text-center"><strong><?= $this->session->flashdata("mensagemReserva") ?></strong></h2>
                    <div id="calendario"></div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<div class="container_modal"></div>

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<!-- CALENDAR -->
<script src="/vendor/moment/min/moment.min.js"></script>
<script src="/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/vendor/fullcalendar/dist/lang/pt-br.js"></script>
<script type="text/javascript">

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false,
        events: [
            <?php foreach ($reservas as $reserva): ?>
                {
                    title: "<?= $reserva->nome_area ?>",
                    start: new Date(<?= date("Y", strtotime($reserva->hora_inicio)) ?>, <?= date("m", strtotime($reserva->hora_inicio)) ?> -1, <?= date("d", strtotime($reserva->hora_inicio)) ?>, <?= date("H", strtotime($reserva->hora_inicio)) ?>, <?= date("i", strtotime($reserva->hora_inicio)) ?>),
                    end: new Date(<?= date("Y", strtotime($reserva->hora_fim)) ?>, <?= date("m", strtotime($reserva->hora_fim)) ?> -1, <?= date("d", strtotime($reserva->hora_fim)) ?>, <?= date("H", strtotime($reserva->hora_fim)) ?>, <?= date("i", strtotime($reserva->hora_fim)) ?>),
                    allDay: false,
                    id: <?= $reserva->id ?>,
                    <?= $reserva->autorizado ? "backgroundColor: '#62cb31',
                    borderColor: '#62cb31'" : "" ?>
                },
            <?php endforeach ?>
        ],
        eventClick: function(calEvent) {
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/reservas/ver", {
                id: calEvent.id
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        }
    });
</script>
</body>
</html>