<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
    <style type="text/css">
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }

        .message-content{
            overflow-wrap: break-word;
        }
    </style>

</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

        <div class="content animate-panel">
            <div class="row">
                <div class="col-xs-12 visible-xs visible-sm">
                    <div class="hpanel collapsed">
                        <div class="panel-heading hbuilt">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Informações da comunicação
                        </div>
                        <div class="panel-body">
                            <h2 class="font-light m-b-xs">
                                <?= $solicitacao->titulo ?>
                            </h2>                            
                            <p class="text-center"><?= $solicitacao->descricao ?></p>
                            <?= $solicitacao->resolvida ? "<span class='label label-success'>Resolvida.</span><div class='clearfix'></div>" : "" ?>
                            <?php if (!$solicitacao->resolvida && $solicitacao->remetente == $this->session->userdata("id")): ?>
                                <button class="marcarResolvido btn btn-outline btn-block btn-lg btn-success">Marcar como <strong>resolvida</strong>.</button>
                                <button data-toggle="modal" data-target="#adicionarParticipante" class="btn btn-outline btn-block btn-lg btn-info">Adicionar participante</button>
                            <?php endif ?>
                            <hr />
                            <div style="max-height: 400px; overflow: auto;">
                                <h4 class="text-center text-info">Participantes</h4>
                                <?php foreach ($participantes as $participante): ?>
                                    <img style="width: 75px; float: left;" alt="<?= $solicitacao->nome_remetente ?>" class="img-circle m-r" src="<?= ($participante->foto ? 'https://residence.acessonaweb.com.br/imagens/moradores/'.$participante->foto.'?'.uniqid() : '/images/usuario.svg') ?>">
                                    <h3>
                                        <a href=""> <?= $participante->nome ?> </a> 
                                        <?php if ($solicitacao->remetente != $participante->id && ($solicitacao->remetente == $this->session->userdata("id") || $participante->id == $this->session->userdata("id")) && !$solicitacao->resolvida): ?>
                                            <button title="Remover" value="<?= $participante->id ?>" class="text-danger pull-right btn-link removerParticipante"><i class="fa fa-close"></i></button>
                                        <?php endif ?>
                                    </h3>
                                    <div class="text-muted font-bold m-b-xs">Residência: <?= $participante->residencia_participante ?></div>
                                    <div class="text-muted font-bold m-b-xs">Localização: <?= $participante->localizacao_participante ?></div>
                                    <hr>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 pull-right hidden-xs hidden-sm">
                    <div class="hpanel"  >
                        <div class="panel-body" style="min-height: 557px !important;">
                            <h2 class="font-light m-b-xs">
                                <?= $solicitacao->titulo ?>
                            </h2>                            
                            <p class="text-center"><?= $solicitacao->descricao ?></p>
                            <?= $solicitacao->resolvida ? "<span class='label label-success'>Resolvida.</span><div class='clearfix'></div>" : "" ?>
                            <?php if (!$solicitacao->resolvida && $solicitacao->remetente == $this->session->userdata("id")): ?>
                                <button class="marcarResolvido btn btn-outline btn-block btn-lg btn-success">Marcar como <strong>resolvida</strong>.</button>
                                <button data-toggle="modal" data-target="#adicionarParticipante" class="btn btn-outline btn-block btn-lg btn-info">Adicionar participante</button>
                            <?php endif ?>
                            <hr />
                            <div style="max-height: 400px; overflow: auto;">
                                <h4 class="text-center text-info">Participantes</h4>
                                <?php foreach ($participantes as $participante): ?>
                                    <img style="width: 75px; float: left;" alt="<?= $solicitacao->nome_remetente ?>" class="img-circle m-r" src="<?= ($participante->foto ? 'https://residence.acessonaweb.com.br/imagens/moradores/'.$participante->foto.'?'.uniqid() : '/images/usuario.svg') ?>">
                                    <h3>
                                        <a href=""> <?= $participante->nome ?> </a> 
                                        <?php if ($solicitacao->remetente != $participante->id && ($solicitacao->remetente == $this->session->userdata("id") || $participante->id == $this->session->userdata("id")) && !$solicitacao->resolvida): ?>
                                            <button title="Remover" value="<?= $participante->id ?>" class="text-danger pull-right btn-link removerParticipante"><i class="fa fa-close"></i></button>
                                        <?php endif ?>
                                    </h3>
                                    <div class="text-muted font-bold m-b-xs">Residência: <?= $participante->residencia_participante ?></div>
                                    <div class="text-muted font-bold m-b-xs">Localização: <?= $participante->localizacao_participante ?></div>
                                    <hr>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="hpanel ">
                        <div class="panel-heading hbuilt">
                                <h4>Criado por <strong><?= $solicitacao->nome_remetente ?></strong> <?= $solicitacao->resolvida ? "<span class='label label-success pull-right visible-xs visible-sm'>Resolvida.</span><div class='clearfix'></div>" : "" ?></h4>
                            </div>
                            <div class="panel-body no-padding">
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="chat-discussion">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <form id="form_envio" method="post">
                                    <div class="input-group">
                                        <input autocomplete="off" <?= $solicitacao->resolvida ? "disabled" : "" ?> type="text" class="form-control" id="textMensagem" placeholder="Digite sua mensagem aqui">
                                        <span class="input-group-btn">
                                            <button <?= $solicitacao->resolvida ? "disabled" : "" ?> id="enviarMensagem" class="btn btn-success">
                                                Enviar
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("admin/inc/footer"); ?>
        </div>

        <div class="modal fade" id="adicionarParticipante" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Adicionar particpante</h4>
                        <h5 class="text-danger" id="modal_cadastro_participante"></h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Usuário:</label>
                            <select style="width: 100%;" class="form-control" id="participante"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="adicionar">Adicionar</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/select2.full.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $("#participante").select2({
                    placeholder: "Selecione...",
                    theme: "bootstrap",
                    minimumInputLength: 2,
                    minimumResultsForSearch: 0,
                    language: {
                        inputTooShort: function(args) {
                          // args.minimum is the minimum required length
                          // args.input is the user-typed text
                          return "Digite mais...";
                      },
                      inputTooLong: function(args) {
                          // args.maximum is the maximum allowed length
                          // args.input is the user-typed text
                          return "Excesso de caracteres";
                      },
                      errorLoading: function() {
                          return "Carregando...";
                      },
                      loadingMore: function() {
                          return "Carregando";
                      },
                      noResults: function() {
                          return "Nenhum resultado";
                      },
                      searching: function() {
                          return "Procurando...";
                      },
                      maximumSelected: function(args) {
                          // args.maximum is the maximum number of items the user may select
                          return "Erro ao carregar resultados";
                      }
                  },
                  ajax: {
                    url: "/comunique/select2_listaUsuarios",
                    dataType: "json",
                    type: "post",
                    data: function (params) {

                        var queryParameters = {
                            term: params.term,
                            type: ""
                        }
                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.nome+" "+item.residencia+" "+item.localizacao,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
        });
            
        </script>

        <?php $this->load->view("admin/inc/scripts_gerais") ?>
        <script type="text/javascript">
            var audio = new Audio('/assets/mensagem.mp3');
            <?php
            if(!$solicitacao->resolvida){
                ?>
                function enviarMensagem(){
                    if($("#textMensagem").val() != null && $("#textMensagem").val() != ""){
                        textMensagem = $("#textMensagem").val();
                        $("#textMensagem").val('');
                        item = $("#enviarMensagem");
                        anterior = item.html();
                        item.html("<i class='fa fa-cog fa-spin'></i>");
                        $.post("/comunique/enviarMensagemSolicitacao", {
                            id: <?= $solicitacao->id ?>,
                            mensagem: textMensagem
                        }, function(result){
                            console.log(result);
                            item.html(anterior);
                            // location.reload();
                        });
                    }
                }
                <?php
            }
            ?>

            var source = new EventSource("/comunique/server/"+<?= $solicitacao->id ?>);
            source.onmessage = function(event) {
                $(".chat-discussion").append(event.data);
                if(event.data != null && event.data != ""){
                    var div = $('.chat-discussion');
                    div.prop("scrollTop", div.prop("scrollHeight"));
                    // if(!document.hasFocus())
                        // audio.play();
                }
            };

            $(function(){
                var div = $('.chat-discussion');
                div.prop("scrollTop", div.prop("scrollHeight"));
                $("#textMensagem").focus();
                $("#form_envio").submit(function(){
                    <?php if (!$solicitacao->resolvida): ?>
                        enviarMensagem();
                    <?php endif ?>
                    return false;
                });

                <?php
                if($solicitacao->remetente == $this->session->userdata("id")){
                    ?>
                    $(".marcarResolvido").click(function(){
                        if(confirm("Marcar como resolvida?")){
                            item = $(this);
                            anterior = item.html();
                            item.html("<i class='fa fa-cog fa-spin'></i>");
                            $.post("/comunique/marcarResolvido", {
                                id: <?= $solicitacao->id ?>
                            }, function(result){
                                console.log(result);
                                location.href = "/comunique"
                            });
                        }
                    });
                    <?php
                }
                ?>

                $("#adicionar").click(function(){
                    item = $(this);
                    anterior = item.html();
                    item.html("<i class='fa fa-cog fa-spin'></i>");
                    $.post("/comunique/adicionarParticipante", {
                        id: <?= $solicitacao->id ?>,
                        usuario: $("#participante").val()
                    }, function(result){
                        item.html(anterior);
                        if(result == ""){
                            $('#adicionarParticipante').modal('hide');
                            $("#modal_cadastro_participante").html("");
                            location.reload();
                        }else
                            $("#modal_cadastro_participante").html(result);
                    });
                });

                $(".removerParticipante").click(function(){
                    if(confirm("Tem certeza?")){
                        item = $(this);
                        anterior = item.html();
                        item.html("<i class='fa fa-cog fa-spin'></i>");
                        $.post("/comunique/removerParticipante", {
                            id: <?= $solicitacao->id ?>,
                            usuario: $(this).val()
                        }, function(result){
                            item.html(anterior);
                            if(result == ""){
                                location.reload();
                            }else
                                alert(result);
                        });
                    }
                });
            });
        </script>
    </body>
    </html>