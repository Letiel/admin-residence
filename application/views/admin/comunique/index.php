<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Comunique | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
    <style type="text/css">
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }
    </style>

</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">


        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <button data-toggle="modal" data-target="#modal_criar_comunicacao" class="btn btn-info btn-outline btn-lg pull-right hidden-xs"><i class="fa fa-plus"></i> Criar comunicação</button>
                    <h2 class="font-light m-b-xs">
                        Comunique
                    </h2>
                    <button data-toggle="modal" data-target="#modal_criar_comunicacao" class="btn btn-info btn-outline btn-lg btn-block visible-xs"><i class="fa fa-plus"></i> Criar comunicação</button>
                    <?= $this->session->flashdata("mensagem_comunique"); ?>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-mailbox">
                                    <tbody>
                                        <?php if ($solicitacoes): ?>
                                            <?php foreach ($solicitacoes as $sol): ?>
                                                <tr class="<?= (strtotime($sol->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))) ? 'unread' : '' ; ?>">
                                                    <td class="">
                                                        <i class="fa fa-envelope-o "></i>
                                                    </td>
                                                    <td style="width: 30%;">
                                                        <a href="/comunique/ver/<?= $sol->id ?>">
                                                            <?php if ($sol->remetente == $this->session->userdata('id')): ?>
                                                                Criado por: Você
                                                            <?php else: ?>
                                                                Criado por: <?= $sol->nome_remetente ?>
                                                            <?php endif ?>
                                                        </a>                                                        
                                                    </td>
                                                    <td><a href="/comunique/ver/<?= $sol->id ?>"><?= $sol->titulo ?></a></td>
                                                    <td class="text-right mail-date">
                                                        <?php if ($sol->resolvida == 1): ?>
                                                            <span class="label label-success pull-left">Resolvida</span>
                                                        <?php endif ?>
                                                        <?= date('d/m/Y H:i', strtotime($sol->data_alteracao)) ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                            <h3 class="text-center">Nenhuma comunicação</h3>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php if ($paginacao != null): ?>
                            <div class="panel-body">
                                <center><?= $paginacao ?></center>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <!-- <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 m-b-md">
                                    <h3>Recebidas</h3>
                                </div>
                                <div class="col-md-6 mailbox-pagination m-b-md">
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" href="/comunique/recebidas">Ver Todas</a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-mailbox">
                                    <tbody>

                                        <?php if ($solicitacoes_recebidas != null): ?>
                                            <?php foreach ($solicitacoes_recebidas as $sol): ?>
                                                <tr>
                                                    <td style="width: 18%;" >
                                                        <a href="/comunique/ver/<?= $sol->id ?>"><?= $sol->nome_remetente ?></a>
                                                        <?php if ($sol->resolvida == 1): ?>
                                                            <span class="label label-success">Resolvida</span>
                                                        <?php endif ?>
                                                    </td>
                                                    <td style="width: 64%;">
                                                        <a href="/comunique/ver/<?= $sol->id ?>"><b><?= $sol->titulo ?></b> 
                                                            <span class="hidden-xs">
                                                                : <?= substr($sol->descricao, 0, 60); ?>
                                                                <?php if (strlen($sol->descricao) >= 60): ?>
                                                                    ...
                                                                <?php endif ?>                                                  
                                                            </span> 
                                                            <?php if (strtotime($sol->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                                                <span class="label label-success pull-right">Novo</span>
                                                            <?php endif ?>
                                                        </a>
                                                    </td>
                                                    <td style="width: 18%;" class="text-right mail-date">
                                                        <?= date('d/m/Y h:i', strtotime($sol->data_alteracao)) ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                            <h3 class="text-center text-primary">Nenhuma Comunicação</h3>
                                        <?php endif ?>
                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="col-md-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 m-b-md">
                                    <h3>Enviadas</h3>
                                </div>
                                <div class="col-md-6 mailbox-pagination m-b-md">
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" href="/comunique/enviadas">Ver Todas</a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-mailbox">
                                    <tbody>

                                        <?php if ($solicitacoes_enviadas!= null): ?>
                                            <?php foreach ($solicitacoes_enviadas as $sol): ?>
                                                <tr>
                                                    <td style="width: 18%;" >
                                                        <a href="/comunique/ver/<?= $sol->id ?>"><?= $sol->nome_destinatario ?></a>
                                                        <?php if ($sol->resolvida == 1): ?>
                                                            <span class="label label-success">Resolvida</span>
                                                        <?php endif ?>
                                                    </td>
                                                    <td style="width: 64%;">
                                                        <a href="/comunique/ver/<?= $sol->id ?>"><b><?= $sol->titulo ?></b> 
                                                            <span class="hidden-xs">
                                                                : <?= substr($sol->descricao, 0, 60); ?>
                                                                <?php if (strlen($sol->descricao) >= 60): ?>
                                                                    ...
                                                                <?php endif ?>                                                  
                                                            </span>
                                                            <?php if (strtotime($sol->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                                                <span class="label label-success pull-right">Novo</span>
                                                            <?php endif ?> 
                                                        </a>
                                                    </td>
                                                    <td style="width: 18%;" class="text-right mail-date">
                                                        <?= date('d/m/Y h:i', strtotime($sol->data_alteracao)) ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                            <h3 class="text-center text-primary">Nenhuma Comunicação</h3>
                                        <?php endif ?>
                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>    


        <?php $this->load->view("admin/inc/footer"); ?>
    </div>



    <div class="modal fade" id="modal_criar_comunicacao" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Criar comunicação</h4>
                    <h5 class="text-danger" id="modal_criacao_comunicacao_erro"></h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Título:</label>
                        <input type="text" class="form-control" id="titulo_comunique" placeholder="Título" />
                    </div>

                    <div class="form-group">
                        <label>Usuário:</label>
                        <select style="width: 100%;" class="form-control" id="destinatario_comunique"></select>
                    </div>

                    <div class="form-group">
                        <label>Descrição</label>
                        <textarea required class="form-control" id="descricao_comunique"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="criar_comunicacao">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#criar_comunicacao").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/comunique/ajax_criar_comunique", {
                    titulo: $("#titulo_comunique").val(),
                    destinatario:$("#destinatario_comunique").val(),
                    descricao:$("#descricao_comunique").val()
                }, function(result){
                    item.html(anterior);
                    if(result == ""){
                        $("#titulo_comunique").val("");
                        $("#destinatario_comunique").select2("val", "");
                        $("#descricao_comunique").val("");
                        $('#modal_criar_comunicacao').modal('hide');
                        $("#modal_criacao_comunicacao_erro").html("");
                        location.reload();
                    }else
                    $("#modal_criacao_comunicacao_erro").html(result);
                });
            });

            $("#destinatario_comunique").select2({
                placeholder: "Selecione...",
                theme: "bootstrap",
                minimumInputLength: 2,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/comunique/select2_listaUsuarios",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    type: ""
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome+" "+item.residencia+" "+item.localizacao,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
        });
    </script>
</body>
</html>