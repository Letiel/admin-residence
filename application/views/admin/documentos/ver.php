<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- DATEPICKER -->
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <h2 class="text-justify" style="text-transform: capitalize"><?= $documento->titulo ?></h2>
                            <p class="text-justify"><?= $documento->descricao ?></p>
                            <?php if ($documento->caminho_arquivo != null && $documento->caminho_arquivo != ""): ?>
                                <h3 class="text-center">Documento:</h3>
                                <iframe class="col-md-12" style="min-height: 500px;" src="https://residence.acessonaweb.com.br/documentos/<?= $documento->caminho_arquivo ?>"></iframe>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
        <div class="container_modal"></div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
</body>
</html>