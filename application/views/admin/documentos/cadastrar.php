<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">

                    <h2 class="font-light m-b-xs">
                        Cadastrar Documento
                    </h2>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form enctype="multipart/form-data" action="/documentos/cadastrar" method="post">
                                <center><h4 class='text-danger'><?= $this->upload->display_errors('', '') ?></h4></center>
                                <center><?= $this->session->flashdata('cadastro_documento') ?></center>
                                <div class="col-md-12 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                    <label>Título:</label>
                                    <input class="form-control" required value="<?= set_value('titulo')  ?>" name="titulo" placeholder="Título" />
                                    <span class="text-danger"><?= form_error("titulo") ?></span>
                                </div>

                                <div class="col-md-12 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                    <label>Descrição:</label><small>&nbsp;Também pode ser usado para conteúdo.</small>
                                    <textarea name="descricao" id="descricao" required class="form-control">
                                        <?= set_value("descricao") ?>
                                    </textarea>
                                    <span class="text-danger"><?= form_error("descricao") ?></span>
                                </div>

                                <div class="col-md-12 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                    <label>Anexar arquivo:</label><small>&nbsp;Arquivo <b>.pdf</b></small>
                                    <input type="file" class="form-control" name="documento" />
                                    <span class="text-danger"><?= form_error("documento") ?></span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-12">
                                    <button class="btn btn-primary btn-lg pull-right">Salvar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<script src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        CKEDITOR.replace('descricao');
    });
</script>
</body>
</html>