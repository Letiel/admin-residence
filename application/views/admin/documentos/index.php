<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- DATEPICKER -->
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <?php //$this->load->view("admin/areas/modals/cadastrar_documento") ?>
                    <h2 class="font-light m-b-xs pull-left">
                        Documentos
                    </h2>
                    <a class="btn btn-info btn-outline btn-lg pull-right" href="/documentos/cadastrar"><i class="fa fa-plus"></i> Cadastrar documento</a>
                    <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_documento") ?></h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/documentos" method="get">
                                <div class="input-group">
                                    <input name="k" class="form-control" type="text" placeholder="Pesquisar documentos...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <center><?= $this->session->flashdata('cadastro_documento') ?></center>
                        <?php if ($documentos != null): ?>
                            <div class="panel-body">
                                <table id="tbl_documentos" class="footable table table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Documento</th>
                                            <th>Data de cadastro</th>
                                            <th>Remover</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($documentos as $documento): ?>
                                            <tr>
                                                <td style="text-transform: capitalize"><?= $documento->titulo ?></td>
                                                <td><?= date("d/m/Y", strtotime($documento->data_alteracao)) ?></td>
                                                <td>
                                                  <a href="/documentos/ver/<?= $documento->id ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                                  <button value="<?= $documento->id ?>" class="btn btn-danger excluir"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>                                    
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ($paginacao): ?>
                                <div class="panel-body">
                                    <center><?= $paginacao ?></center>
                                </div>
                            <?php endif ?>
                        <?php else: ?>
                            <div class="panel-body">
                                <div class="row">
                                    <h3 class="text-center text-info">Nenhum documento</h3>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
        <div class="container_modal"></div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#tbl_documentos').footable({paginate: false});

        $(".excluir").click(function(){
          if(confirm("Tem certeza que deseja remover este documento? Ele não poderá mais ser recuperado.")){
            $.post("/documentos/excluir", {
                id: $(this).val()
              }, function(result){
                $("body").append(result);
            });
          }
        });
      });
    </script>
</body>
</html>