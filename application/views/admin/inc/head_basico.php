<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Page title -->

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" type="image/ico" href="https://residence.acessonaweb.com.br/images/favicon.ico" />

<!-- Vendor styles -->
<link rel="stylesheet" href="/vendor/fontawesome/css/font-awesome.css" />
<link rel="stylesheet" href="/vendor/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="/vendor/animate.css/animate.css" />
<link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" href="/vendor/sweetalert/lib/sweet-alert.css" />
<!-- App styles -->
<link rel="stylesheet" href="/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
<link rel="stylesheet" href="/fonts/pe-icon-7-stroke/css/helper.css" />
<link rel="stylesheet" href="/styles/style.css">
<script src="/vendor/jquery/dist/jquery.min.js"></script>