<style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Raleway:300,700');
    body, html{
        background-color: #f1f3f6;
        height: fit-content;
    }

    .modal-open{
        overflow: initial !important;
        padding-right: 0 !important;
    }

    /*HEADER*/
    .color-line{
        background: #34495e !important;
    }
    #header{
        /*background-color: #4051b5;*/
        background-color: #303F9F;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        border:none;
    }

    .header-link{
        background-color: transparent;
        color: white;
        border:none !important;
        font-family: 'Raleway', sans-serif;
        text-transform: uppercase;
        font-size: 16px;
        font-weight: 700;
    }

    @media(min-width: 768px){
        .header-link{
            width: 200px;
        }
    }
    .header-link:hover{
        background-color: rgba(255, 255, 255, 0.06);
    }

    .small-logo span{
        color: white !important;
        font-family: 'Raleway', sans-serif;
        text-transform: uppercase;
        font-size: 16px;
        font-weight: 700;

    }

    nav .navbar-right .nav li a i{
        color: white;
    }

    nav .navbar-right .nav li a:hover i{
        color: black;
    } 
    /*HEADER*/
    #menu {
        width: 200px;
        margin-right: -17px;
        background: #262930;
        height: fit-content;
    }

    #menu .fa{
        width: 20px;
        text-align: center;
    }
    #side-menu, #side-menu li{
        background: #262930;
        border:none;
    }
    #side-menu li:first-child {
        border:none;
    }

    #side-menu li a {
        color: white;       
    }

    #side-menu.nav > li > a:hover, #side-menu.nav > li > a:focus, #side-menu li.active a {
        background: #16191e;
        color: white;
    }

    #side-menu li .nav-second-level li a {
        padding: 10px 10px 10px 30px;
        color: #fff;
        text-transform: none;
        font-weight: 600;
    }



    #wrapper, body.page-small.show-sidebar #wrapper {
        margin-left: 200px;
    }
    @media(max-width: 953px){
        .normalheader h2 {
            margin-bottom: 20px;
            text-align: center;
        }

    }

    .form-control{
        /*background: white !important;*/
        /*border-color: #c6c6c6 !important;*/
        box-shadow: 0px 0px 22px 0px #EEE inset;
    }

    .form-control:focus{
        /*background: white !important;*/
        /*border-color: #66afe9 !important;*/
        box-shadow: none;
    }

    .select2-container--bootstrap .select2-selection--single {
        height: 34px;
        line-height: 1.428571429;
        padding: 6px 24px 6px 12px;
        border: 1px solid #c6c6c6;
        box-shadow: 0px 0px 22px 0px #EEE inset;
        background: white !important;

    }

    .select2-container--open .select2-selection--single, .select2-container--focus .select2-selection--single {
        background: white !important;
    }

    .select2-container--bootstrap.select2-container--focus .select2-selection, .select2-container--bootstrap.select2-container--open .select2-selection{
        box-shadow: none !important;
    }

    .chat-discussion {
        overflow-y: auto;
        height: 440px;
    }

    .splash{
        background: #303F9F;
        color: white;
    }

    .splash-title h1{
        font-size: 30px; 
        font-weight: 700; 
        font-family: 'Raleway'; 
        color: white; 
        text-align: center; 
        text-transform: uppercase; 
        letter-spacing: -1px;
        margin: 0 0 5px 0;
    }
    
</style>


<script type="text/javascript">
    $("body").addClass("fixed-navbar");
</script>

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Residence Online</h1><p>Gerencie seu condomínio facilmente através da interface web. </p><img src="/images/spinner.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">Você está usando um navegador <strong>ultrapassado</strong>. Por favor <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar sua experiência.</p>
<![endif]-->

<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <!-- <div id="logo" class="light-version">
        <span>
            Residence Online
        </span>
    </div> -->
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i> <span class="hidden-xs">Menu</span></div>
        <div class="small-logo">
            <span class="text-primary">Residence Online</span>
        </div><!-- 
        <form role="search" class="navbar-form-custom" method="post" action="#">
            <div class="form-group">
                <input type="text" placeholder="Pesquisar" class="form-control" name="search">
            </div>
        </form> -->
        <div class="navbar-right">
            <ul class="nav navbar-nav no-borders">
                <li class="dropdown">
                    <!-- <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <i class="pe-7s-keypad"></i>
                    </a>

                    <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <a href="/">
                                        <i class="pe pe-7s-mail text-warning"></i>
                                        <h5>Notificações</h5>
                                    </a>
                                </td>
                                <td>
                                    <a href="/">
                                        <i class="pe pe-7s-users text-success"></i>
                                        <h5>Moradores</h5>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/">
                                        <i class="pe pe-7s-comment text-info"></i>
                                        <h5>Forum</h5>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div> -->
                </li>
                <li class="dropdown">
                    <a href="/sair">
                        <i class="pe-7s-upload pe-rotate-90"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="/">
                <?php if ($this->session->userdata('imagem') == null): ?>
                    <img src="http://residence.acessonaweb.com.br/images/user.svg" style="border-radius: 100%; width: 70%; align-self: center;margin-bottom: 20px;">
                <?php else: ?>
                    <img src="http://residence.acessonaweb.com.br/imagens/moradores/<?= $this->session->userdata('imagem').'?'.uniqid()?>" style="border-radius: 100%; width: 70%; align-self: center;margin-bottom: 20px;">
                <?php endif ?>
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase"></span>

                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" style="cursor: pointer;color: #f9fbfe;font-weight: bold;">
                        <?= $this->session->userdata("nome") ?> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="/sair">Sair</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav" id="side-menu">
            <li id="admin_mural">
                <a href="/"> <i class="fa fa-hashtag"></i> <span class="nav-label">Mural</span> </a>
            </li>
            <li id="admin_avisos">
                <a href="/avisos"> <i class="fa fa-info"></i> <span class="nav-label"> <span class="nav-label">Avisos</span> </a>
            </li>
            <li id="admin_localizacoes">
                <a href="/localizacoes"> <i class="fa fa-building"></i> <span class="nav-label">Localizações</span></a>
            </li>
            <li id="admin_residencias">
                <a href="/residencias"> <i class="fa fa-home"></i> <span class="nav-label">Residências</span></a>
            </li>
            <li id="admin_boxes">
                <a href="/boxes"> <i class="fa fa-flag"></i> <span class="nav-label">Boxes</span></a>
            </li>
            <li id="admin_cadastro_moradores">
                <a href="/moradores"> <i class="fa fa-users"></i> <span class="nav-label">Usuários</span> </a>
            </li>
            <li id="admin_cadastro_veiculos">
                <a href="/veiculos"> <i class="fa fa-car"></i> <span class="nav-label">Veículos</span> </a>
            </li>
            <li id="admin_solicitacoes">
                <a href="/comunique"> <i class="fa fa-comments"></i> <span class="nav-label">Comunique</span> </a>
            </li>
            <li id="admin_areas">
                <a href="/areas"> <i class="fa fa-futbol-o"></i>  <span class="nav-label">Áreas</span> </a>
            </li>
            <li id="admin_reservas">
                <a href="/reservas"> <i class="fa fa-calendar-o"></i>  <span class="nav-label">Reservas</span> </a>
            </li>
            <li id="admin_enquetes">
                <a href="/enquetes"> <i class="fa fa-question"></i>  <span class="nav-label">Enquetes</span> </a>
            </li>
            <li id="admin_forum">
                <a href="/forum"> <i class="fa fa-commenting-o"></i> <span class="nav-label">Fórum</span> </a>
            </li>
            <li id="admin_documentos">
                <a href="/documentos"> <i class="fa fa-file-o"></i> <span class="nav-label">Documentos</span> </a>
            </li>
            <li id="admin_condominio">
                <a href="/condominio"> <i class="fa fa-globe"></i> <span class="nav-label">Condomínio</span> </a>
            </li>
            <li class="visible-xs visible-sm">
                <a href="/sair"> <i class="fa fa-sign-out"></i> <span class="nav-label">Sair</span> </a>
            </li>
        </ul>
    </div>
</aside>

<script type="text/javascript">
    $(document).ready(function(){
        $("#menu").css({height: $("html").height()});
    });
</script>