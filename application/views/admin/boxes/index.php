<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">

                    <?php $this->load->view("admin/boxes/modals/cadastrar_box") ?>
                    <h2 class="font-light m-b-xs">
                        Boxes
                    </h2>
                    <a class="btn btn-info btn-outline btn-lg btn-block visible-xs" data-toggle="modal" data-target="#cadastro_box"><i class="fa fa-plus"></i> Cadastrar Box</a>
                    <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_box") ?></h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel">
                                
                            </div>
                        </div>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/boxes" method="get">
                                <div class="input-group">
                                    <input name="k" class="form-control" type="text" placeholder="Pesquisar boxes...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="panel-body">
                         
                            <table id="tbl_boxes" class="footable table table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th>BOX</th>
                                        <th data-hide="phone">Localização</th>
                                        <th data-hide="phone">Residência</th>
                                        <th data-hide="phone">Tipo</th>
                                        <th data-hide="phone, tablet">Descricao</th>
                                        <th data-hide="phone">Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($boxes as $box): ?>
                                        <tr>
                                            <td><?= $box->nome ?></td>
                                            <td><?= $box->nome_localizacao ?></td>
                                            <td><?= $box->nome_residencia." ".$box->nome_r_localizacao ?></td>
                                            <td>
                                                <?php
                                                if($box->tipo == "PUBLICO")
                                                    echo "Público";
                                                else if($box->tipo == "MORADOR")
                                                    echo "Morador";
                                                ?>      
                                            </td>
                                            <td><?= $box->descricao ?></td>
                                            <td>
                                                <button value="<?= $box->id ?>" class="btn btn-success btn-outline editar"><span class="fa fa-pencil"></span></button>
                                                <button value="<?= $box->id ?>" class="btn btn-danger btn-outline deletar"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>                                    
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <div class="btn-group pull-right">
                                <?= $paginacao ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
        <div class="container_modal"></div>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script src="/vendor/fooTable/dist/footable.all.min.js"></script>
    <!-- SELECT2 -->
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $('#tbl_boxes').footable({paginate: false});

            $(".editar").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/boxes/editar", {
                    id: $(this).val()
                }, function(result){
                    $(".container_modal").html(result);
                    item.html(anterior);
                });
            });

            $(".deletar").click(function(){
                if(confirm("Tem certeza?")){
                    item = $(this);
                    anterior = item.html();
                    item.html("<i class='fa fa-cog fa-spin'></i>");
                    $.post("/boxes/deletar", {
                        id: $(this).val()
                    }, function(result){
                        console.log(result);
                        item.html(anterior);
                        location.reload();
                    });
                }
            });

            $("#localizacao").select2({
                theme: "bootstrap",
                minimumInputLength: 2,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/residencias/select2_listaLocalizacoes",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

            $("#residencia").select2({
                theme: "bootstrap",
                minimumInputLength: 2,
                minimumResultsForSearch: 0,
                language: {
                    inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/veiculos/select2_listaResidencias",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome+" "+item.localizacao,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
        });
    </script>
</body>
</html>