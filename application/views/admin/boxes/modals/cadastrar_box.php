<a class="btn btn-info btn-outline btn-lg pull-right hidden-xs" data-toggle="modal" data-target="#cadastro_box"><i class="fa fa-plus"></i> Cadastrar Box</a>
<div class="modal fade" id="cadastro_box" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar novo box</h4>
                <h5 class="text-danger" id="modal_cadastro_box_erro"></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nome:</label>
                    <input placeholder="Identificação" required class="form-control" type="text" id="nome" />
                </div>
                <div class="form-group">
                    <label>Tipo:</label>
                    <select class='form-control' style="width: 100%;" id="tipo" placeholder="Tipo">
                        <option value="">Selecione...</option>
                        <option value="PUBLICO">Público</option>
                        <option value="MORADOR">Morador</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Localização:</label>
                    <select class='form-control' style="width: 100%;" id="localizacao" placeholder="Localização"><option value="">Selecione...</option></select>
                </div>
                <div class="form-group">
                    <label>Residência vinculada:</label>
                    <select class='form-control' style="width: 100%;" id="residencia" placeholder="Residência"><option value="">Selecione...</option></select>
                </div>
                <div class="form-group">
                    <label>Descrição:</label>
                    <textarea placeholder="Descrição" required class="form-control" id="descricao"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_box">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cadastrar_box").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/boxes/ajax_cadastro_box", {
                nome: $("#nome").val(),
                tipo:$("#tipo").val(),
                localizacao:$("#localizacao").val(),
                residencia:$("#residencia").val(),
                descricao:$("#descricao").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$('#cadastro_box').modal('hide');
					$("#modal_cadastro_box_erro").html("");
                    location.reload();
				}else
		        	$("#modal_cadastro_box_erro").html(result);
		    });
		});
	});
</script>