<div class="modal fade" id="edicao_box" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Editar box</h4>
                <h5 class="text-danger" id="modal_edicao_box_erro"></h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nome:</label>
                    <input placeholder="Identificação" value="<?= $box->nome ?>" required class="form-control" type="text" id="editar_nome" />
                </div>
                <div class="form-group">
                    <label>Tipo:</label>
                    <select class='form-control' style="width: 100%;" id="editar_tipo" placeholder="Tipo">
                        <option value="">Selecione...</option>
                        <option <?= ($box->tipo == "PUBLICO" ? "selected" : "") ?> value="PUBLICO">Público</option>
                        <option <?= ($box->tipo == "MORADOR" ? "selected" : "") ?> value="MORADOR">Morador</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Localização:</label>
                    <select class='form-control' style="width: 100%;" id="editar_localizacao" placeholder="Localização"><option selected value="<?= $box->id_localizacao ?>"><?= $box->nome_localizacao ?></option></select>
                </div>
                <div class="form-group">
                    <label>Residência vinculada:</label>
                    <select class='form-control' style="width: 100%;" id="editar_residencia" placeholder="Residência"><option selected value="<?= $box->id_residencia ?>"><?= $box->nome_residencia." - ".$box->nome_r_localizacao ?></option></select>
                </div>
                <div class="form-group">
                    <label>Descrição:</label>
                    <textarea placeholder="Descrição" required class="form-control" id="editar_descricao"><?= $box->descricao ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="editar_box">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#edicao_box").modal("show");
        $("#editar_box").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/boxes/ajax_edicao_box", {
                id: <?= $box->id ?>,
                nome: $("#editar_nome").val(),
                tipo:$("#editar_tipo").val(),
                localizacao:$("#editar_localizacao").val(),
                residencia:$("#editar_residencia").val(),
                descricao:$("#editar_descricao").val()
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $('#cadastro_box').modal('hide');
                    $("#modal_edicao_box_erro").html("");
                    location.reload();
                }else
                    $("#modal_edicao_box_erro").html(result);
            });
        });

        $("#editar_localizacao").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/residencias/select2_listaLocalizacoes",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#editar_residencia").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
                },
                inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
                },
                errorLoading: function() {
                  return "Carregando...";
                },
                loadingMore: function() {
                  return "Carregando";
                },
                noResults: function() {
                  return "Nenhum resultado";
                },
                searching: function() {
                  return "Procurando...";
                },
                maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
                }
            },
            ajax: {
                url: "/veiculos/select2_listaResidencias",
                dataType: "json",
                type: "post",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome+" "+item.localizacao,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
    });
</script>