<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">
                        Criar enquete
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            <div id="opcoes">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="titulo" class="form-control" placeholder="Título da enquete" value="<?= set_value('titulo') ?>" />
                                    <span class="text-danger"><?php echo form_error("titulo") ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea name="descricao" class="form-control" placeholder="Descrição"><?= set_value("descricao") ?></textarea>
                                    <span class="text-danger"><?php echo form_error("descricao") ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Data de validade</label>
                                    <input class="form-control" id="data_validade" type="text" name="data_validade">
                                    <span class="text-danger"><?php echo form_error("data_validade") ?></span>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label>Opção</label>
                                    <input required type="text" class="form-control" name="opcoes[]" placeholder="Opção" />
                                </div>
                                <div class="form-group">
                                    <label>Opção</label>
                                    <input required type="text" class="form-control" name="opcoes[]" placeholder="Opção" />
                                </div>
                            </div>
                            <span class="text-danger"><?php echo form_error("opcoes[]") ?></span>

                            <button id="adicionar" type="button" class="btn btn-link btn-block">Adicionar opção</button>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button class="btn btn-primary">Criar enquete</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<?php $this->load->view("admin/inc/scripts_gerais") ?>
<script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    $(function(){
        $("#adicionar").click(function(){
            $("#opcoes").append('<div class="form-group"><label>Opção</label><div class="input-group"><input required type="text" class="form-control" name="opcoes[]" placeholder="Opção" /><span class="input-group-btn"><button type="button" onclick="remover(this)" class="btn btn-danger"><i class="fa fa-remove"></i></button></span></div></div>');
        });

        $.datetimepicker.setLocale('pt-BR');
        $("#data_validade").datetimepicker({
            format:'d/m/Y H:i',
        });
    });

    function remover(elemento){
        $(elemento).parent().parent().parent().remove();
    }
</script>
</body>
</html>