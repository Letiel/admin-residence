<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Enquetes | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <a class='btn btn-info btn-outline btn-lg pull-right hidden-xs' href="/enquetes/cadastrar"><i class="fa fa-plus"></i> Criar enquete</a>
                    <h2 class="font-light m-b-xs">
                        Enquetes
                    </h2>
                    <a class='btn btn-info btn-outline btn-lg btn-block visible-xs' href="/enquetes/cadastrar"><i class="fa fa-plus"></i> Criar enquete</a>
                    <h3 class="text-info text-center">
                        <?= $this->session->flashdata("mensagem_enquete") ?>
                    </h3>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <?php if ($enquetes != null): ?>                            
                            <?php foreach ($enquetes as $enquete): ?>

                                <a href="/enquetes/ver/<?= $enquete->id ?>">
                                    <div class="panel-body">
                                        <div class="pull-right text-right">
                                            <?php if (strtotime($enquete->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                                <span class="label label-success">Novo</span>
                                            <?php endif ?>
                                            <small class="stat-label">Até <?= date("d/m/Y H:i", strtotime($enquete->data_validade)) ?></small>
                                            <h4><?= $enquete->nome_usuario ?> <i class="fa fa-user text-success"></i></h4>
                                        </div>
                                        <h4 class="m-b-xs"><?= $enquete->titulo ?></h4>
                                        <p><span class="label label-info"><?= ($enquete->total_votos > 0 ? ($enquete->total_votos == 1 ? "$enquete->total_votos voto" : "$enquete->total_votos votos") : "Nenhum voto") ?></span></p>
                                        <?php
                                        if(strtotime($enquete->data_validade) < strtotime(date("Y-m-d H:i:s")) || !$enquete->ativado){
                                            echo "<span class='label label-danger'>Fechada</span>";
                                        }
                                        ?>
                                        <p style='margin-top: 5px;' class="small">Criado em: <strong><?= date("d/m/Y H:i", strtotime($enquete->data_abertura)) ?></strong></p>
                                    </div>
                                </a>

                            <?php endforeach ?>
                        <?php else: ?>
                            <div class="panel-body">
                                <h2 class="text-info text-center">Nenhuma enquete</h2>
                            </div>
                        <?php endif ?>
                        <?php if ($paginacao): ?>
                           <div class="panel-body">                       
                            <center>
                                <?= $paginacao ?>
                            </center>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<?php $this->load->view("admin/inc/scripts_gerais") ?>
</body>
</html>