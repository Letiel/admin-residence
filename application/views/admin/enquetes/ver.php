<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">
                        Enquete
                    </div>
                    <div class="panel-body">
                        <h2 class="text-info text-center"><?= $enquete->titulo ?><br /><small><?= $enquete->descricao ?></small></h2>
                        <h2 class="text-info text-center"><?= $this->session->flashdata("mensagem_enquete") ?></h2>
                        <h3 class="text-danger text-center"><?= validation_errors() ?></h3> 
                        <?php
                            $bloqueio = "";
                            if(strtotime($enquete->data_validade) < strtotime(date("Y-m-d H:i:s"))){
                                echo "<div class='alert alert-warning' style='margin-bottom: 10px;'>
                                    <span class='text-warning text-center'>O prazo de resposta acabou.</span>
                                </div>";
                                $bloqueio = "disabled";
                            }

                            if(!$enquete->ativado){
                                echo "<div class='alert alert-warning' style='margin-bottom: 10px;'>
                                    <span class='text-warning text-center'>A enquete está fechada.</span>
                                </div>";
                                $bloqueio = "disabled";
                            }
                            ?>

                        <div class="form-group">
                            <label>Data de validade:</label>
                            <div class="input-group">
                                <input class="form-control" id="data_validade" value="<?= date("d/m/Y H:i:s", strtotime($enquete->data_validade)) ?>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" id="alterar_validade">Alterar</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-<?= ($enquete->ativado ? "danger" : "success") ?> btn-block btn-outline" id="desativar"><?= ($enquete->ativado ? "Desativar" : "Reativar") ?> enquete</button>
                        </div>
                        <?php @$voto_enquete->voto = ($voto_enquete == null ? null : $voto_enquete->voto) ?>                           
                        <?= ($voto_enquete->voto == null ? '<form method="post">' : "") ?>
                            <?php foreach ($opcoes as $opcao): ?>
                                <div class="radio">
                                    <label style="width: 100%;">
                                        <h3>
                                            <input required type="radio" <?= $bloqueio ?> name="voto" value="<?= $opcao->id ?>" <?= ($voto_enquete->voto == $opcao->id ? "checked" : "") ?> <?= ($voto_enquete->voto != null ? "disabled" : "") ?> class="i-checks"> <?= $opcao->titulo ?>
                                            <span class="label label-info pull-right"><?= $opcao->total_votos ?></span>
                                        </h3>
                                    </label>
                                </div>
                                <hr>
                            <?php endforeach ?>
                            <button class="btn btn-primary btn-block <?= $bloqueio ?> <?= ($voto_enquete->voto != null ? "disabled" : "") ?>"><?= ($voto_enquete->voto == null ? "Responder" : "Você já votou.") ?></button>
                        <?= ($voto_enquete->voto == null ? '</form>' : "") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<?php $this->load->view("admin/inc/scripts_gerais") ?>
<script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    $(function(){
        $.datetimepicker.setLocale('pt-BR');
        $("#data_validade").datetimepicker({
            format:'d/m/Y H:i',
        });
        $("#alterar_validade").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/enquetes/alterar_validade", {
                id: <?= $enquete->id ?>,
                data_validade: $("#data_validade").val()
            }, function(result){
                location.reload();
            });
        });

        $("#desativar").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/enquetes/desativar", {
                id: <?= $enquete->id ?>,
            }, function(result){
                location.href="/enquetes";
            });
        });
    });
</script>
</body>
</html>