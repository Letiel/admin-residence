<div class="modal fade" id="modal_verMoradores" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Moradores<br /><small><small><?= (isset($moradores[0]->nome_residencia) ? $moradores[0]->nome_residencia : "Residência desconhecida")." - ".(isset($moradores[0]->nome_localizacao) ? $moradores[0]->nome_localizacao : "Localização desconhecida") ?></small></small></h4>
                <h5 class="text-danger" id="modal_ver_moradores_erro"></h5>
            </div>
            <div class="modal-body">
                <?php if ($moradores == null): ?>
                    <center><h2 class="text-center text-info">Nenhum morador vinculado.</h2></center>
                <?php endif ?>
                <?php foreach ($moradores as $morador): ?>
                    <div class="hpanel filter-item">
                        <div class="panel-body">
                            <a target="_blank" href="/moradores/editar/<?= $morador->id ?>">
                                <div class="pull-right text-right">
                                    <small class="stat-label"></small>
                                    <h4><span class="label label-info"><?= $morador->nome_tipo ?></span></h4>
                                </div>
                                <h4 class="m-b-xs"><?= $morador->nome ?></h4>
                            </a>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#modal_verMoradores").modal("show");
        $('#tbl_modal_moradores').footable();
    });
</script>