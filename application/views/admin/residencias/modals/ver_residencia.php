<div class="modal fade" id="ver_residencia" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title"><?= $residencia->nome_localizacao ?> - <?= $residencia->nome ?></h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab_residencia">Residência</a></li>
                    <li class=""><a data-toggle="tab" href="#tab_moradores">Usuários</a></li>
                    <li class=""><a data-toggle="tab" href="#tab_boxes">Boxes</a></li>
                    <li class=""><a data-toggle="tab" href="#tab_veiculos">Veículos</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab_residencia" class="tab-pane active">
                        <div class="panel-body">
                            <h2 class="text-center text-info"><?= ($residencia->descricao != null && $residencia->descricao != "" ? $residencia->descricao : "Nenhuma descrição da residência.") ?></h2>
                        </div>
                    </div>
                    <div id="tab_moradores" class="tab-pane">
                        <div class="panel-body">
                            <?php if ($moradores == null): ?>
                                <center><h2 class="text-info">Nenhum usuário vinculado.</h2></center>
                            <?php endif ?>
                            <?php foreach ($moradores as $morador): ?>
                                <div class="hpanel filter-item">
                                    <div class="panel-body" style="border: 1px solid #e4e5e7;">
                                        <a target="_blank" href="/moradores/editar/<?= $morador->id ?>">
                                            <div class="pull-right text-right">
                                                <small class="stat-label"></small>
                                                <h4><span class="label label-info"><?= $morador->nome_tipo ?></span></h4>
                                            </div>
                                            <h4 class="m-b-xs"><?= $morador->nome ?></h4>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div id="tab_boxes" class="tab-pane">
                        <div class="panel-body">
                            <?php if ($boxes == null): ?>
                                <center><h2 class="text-info">Nenhum box vinculado.</h2></center>
                            <?php endif ?>
                            <?php foreach ($boxes as $box): ?>
                                <div class="hpanel filter-item">
                                    <div class="panel-body" style="border: 1px solid #e4e5e7;">
                                        <div class="pull-right text-right">
                                            <small class="stat-label"></small>
                                            <h4><span class="label label-info"><?= $box->nome_localizacao ?></span></h4>
                                        </div>
                                        <h4 class="m-b-xs"><?= $box->nome ?></h4>
                                    </div>
                                </div>
                            <?php endforeach ?>                         
                        </div>
                    </div>
                    <div id="tab_veiculos" class="tab-pane">
                        <div class="panel-body">
                            <?php if ($veiculos == null): ?>
                                <center><h2 class="text-info">Nenhum veículo vinculado.</h2></center>
                            <?php endif ?>
                            <?php foreach ($veiculos as $veiculo): ?>
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <img class="img-responsive" src="https://residence.acessonaweb.com.br<?= ($veiculo->foto != null && $veiculo->foto != '' ? '/imagens/veiculos/'.$veiculo->foto : '/images/veiculo.png') ?>" />
                                    </div>
                                    <div class="col-xs-4">
                                        <h3 class="text-primary"><?= strtoupper($veiculo->placa) ?></h3>
                                        <small><?= ($veiculo->uf != null && $veiculo->uf != "" ? $veiculo->uf : "") ?> <?= ($veiculo->cidade != null && $veiculo->cidade != "" ? $veiculo->cidade : "") ?></small>
                                    </div>
                                    <div class="col-xs-5">
                                        <a href="/veiculos/editar/<?= $veiculo->id ?>" class="btn btn-info btn-outline pull-right">Ver/Editar</a>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                </div>
                            <?php endforeach ?> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#ver_residencia").modal("show");
    });
</script>