<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <!-- DATEPICKER -->
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        Condomínio
                    </div>
                    <div class="panel-body">
                        <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_condominio") ?></h3>
                        <h2 class="text-center text-info"><strong><?= $condominio->nome ?></strong></h2>
                        <p class="text-center"><small>CNPJ: <strong><?= $condominio->cnpj ?></strong></small></p>
                        <p class="text-center"><small>Validade da licença: <strong><?= date("d/m/Y H:i:s", strtotime($condominio->data_validade)) ?></strong></small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>

<?php $this->load->view("admin/inc/scripts_gerais") ?>
</body>
</html>