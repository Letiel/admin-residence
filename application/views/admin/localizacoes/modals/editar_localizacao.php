<div class="modal fade" id="edicao_localizacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Editar localização</h4>
                <h5 class="text-danger" id="modal_edicao_localizacao_erro"></h5>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<label>Nome:</label>
                    <input class="form-control" value="<?= $localizacao->nome ?>" placeholder="Nome da localização" type="text" id="nome_localizacao">
            	</div>

            	<div class="form-group">
            		<label>Descrição:</label>
            		<textarea class="form-control" placeholder="Descrição" id="descricao_localizacao"><?= $localizacao->descricao ?></textarea>
            	</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="atualizar_localizacao">Atualizar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
        $("#edicao_localizacao").modal("show");
		$("#atualizar_localizacao").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/localizacoes/ajax_edicao_localizacao", {
                id: <?= $localizacao->id ?>,
                nome: $("#nome_localizacao").val(),
                descricao: $("#descricao_localizacao").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$('#edicao_localizacao').modal('hide');
					$("#modal_edicao_localizacao_erro").html("");
                    location.reload();
				}else
		        	$("#modal_edicao_localizacao_erro").html(result);
		    });
		});
	});
</script>