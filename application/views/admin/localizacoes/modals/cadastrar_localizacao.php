<a class="btn btn-info btn-outline btn-lg pull-right hidden-xs" data-toggle="modal" data-target="#cadastro_localizacao"><i class="fa fa-plus"></i> Cadastrar localização</a>
<div class="modal fade" id="cadastro_localizacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar localização</h4>
                <h5 class="text-danger" id="modal_cadastro_localizacao_erro"></h5>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<label>Nome:</label>
                    <input class="form-control" placeholder="Nome da localização" type="text" id="nome_localizacao">
            	</div>

            	<div class="form-group">
            		<label>Descrição:</label>
            		<textarea class="form-control" placeholder="Descrição" id="descricao_localizacao"></textarea>
            	</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_localizacao">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cadastrar_localizacao").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/moradores/ajax_cadastro_localizacao", {
                nome: $("#nome_localizacao").val(),
                descricao: $("#descricao_localizacao").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$("#nome_localizacao").val("");
					$("#descricao_localizacao").val("");
					$('#cadastro_localizacao').modal('hide');
					$("#modal_cadastro_localizacao_erro").html("");
					iniciar_localizacoes();
				}else
		        	$("#modal_cadastro_localizacao_erro").html(result);
		    });
		});
	});
</script>