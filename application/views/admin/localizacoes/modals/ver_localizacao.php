<div class="modal fade" id="ver_localizacao" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title"><?= $localizacao->nome ?></h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab_localizacao">Localização</a></li>
                    <li class=""><a data-toggle="tab" href="#tab_residencias">Residências</a></li>
                    <li class=""><a data-toggle="tab" href="#tab_l_boxes">Boxes</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab_localizacao" class="tab-pane active">
                        <div class="panel-body">
                            <h2 class="text-center text-info"><?= ($localizacao->descricao != null && $localizacao->descricao != "" ? $localizacao->descricao : "<b>Nenhuma descrição da localizacao.</b>") ?></h2>
                        </div>
                    </div>
                    <div id="tab_residencias" class="tab-pane">
                        <div class="panel-body">
                            <?php if ($residencias == null): ?>
                                <center><h2 class="text-info">Nenhuma residência vinculada.</h2></center>
                            <?php endif ?>
                            <?php foreach ($residencias as $residencia): ?>
                                <div class="hpanel filter-item">
                                    <div class="panel-body" style="border: 1px solid #e4e5e7;">
                                        <button class=" btn-link ver_residencia" value="<?= $residencia->id ?>">
                                            <div class="pull-right text-right">
                                                <small class="stat-label"></small>
                                                <!-- <h4><span class="label label-info"><?= $morador->nome_tipo ?></span></h4> -->
                                            </div>
                                            <h4 class="m-b-xs"><?= $residencia->nome ?></h4>
                                        </button>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div id="tab_l_boxes" class="tab-pane">
                        <div class="panel-body">
                            <?php if ($boxes == null): ?>
                                <center><h2 class="text-info">Nenhum box vinculado.</h2></center>
                            <?php endif ?>
                            <?php foreach ($boxes as $box): ?>
                                <div class="hpanel filter-item">
                                    <div class="panel-body" style="border: 1px solid #e4e5e7;">
                                        <div class="pull-right text-right">
                                            <small class="stat-label"></small>
                                            
                                        </div>
                                        <h4 class="m-b-xs"><?= $box->nome ?></h4>
                                    </div>
                                </div>
                            <?php endforeach ?>                         
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#ver_localizacao").modal("show");

        $(".ver_residencia").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/residencias/ver", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").append(result);
                item.html(anterior);
            });
            return false;
        });
    });
</script>