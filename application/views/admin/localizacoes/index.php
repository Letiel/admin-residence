<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    
    <!-- TABELA -->
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">
    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">

                <?php $this->load->view("admin/localizacoes/modals/cadastrar_localizacao") ?>
                <h2 class="font-light m-b-xs">
                    Localizações
                </h2>
                <a class="btn btn-info btn-outline btn-lg btn-block visible-xs" data-toggle="modal" data-target="#cadastro_localizacao"><i class="fa fa-plus"></i> Cadastrar localização</a>
                <h3 class="text-success text-center"><?= $this->session->flashdata("mensagem_cadastro_localizacao") ?></h3>
            </div>
        </div>
    </div>
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            
                        </div>
                    </div>
                </div>
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="/localizacoes" method="get">
                            <div class="input-group">
                                <input name="k" class="form-control" type="text" placeholder="Pesquisar localizações...">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body">
                        <table id="tbl_localizacoes" class="footable table table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Descrição</th>
                                    <th data-hide="phone">Edição</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($localizacoes as $localizacao): ?>
                                    <tr>
                                        <td><?= $localizacao->nome ?></td>
                                        <td><?= $localizacao->descricao ?></td>
                                        <td>
                                            <button value="<?= $localizacao->id ?>" class="btn btn-info btn-outline ver"><span class="fa fa-eye"></span></a> 
                                            <button value="<?= $localizacao->id ?>" class="btn btn-success btn-outline editar"><span class="fa fa-pencil"></span>
                                            </button> 
                                            <button value="<?= $localizacao->id ?>" class="btn btn-danger btn-outline deletar"><span class="fa fa-trash"></span></button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        <div class="btn-group pull-right">
                            <?= $paginacao ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
    <div class="container_modal"></div>

<?php $this->load->view("admin/inc/scripts_gerais") ?>
<script src="/vendor/fooTable/dist/footable.all.min.js"></script>
<script type="text/javascript">
    function iniciar_localizacoes(){
        location.reload();
    }
    $(function(){
        $('#tbl_localizacoes').footable({ paginate:false });

        $(".ver").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/localizacoes/ver", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        $(".editar").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/localizacoes/editar", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        $(".deletar").click(function(){
            if(confirm("Tem certeza?")){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/localizacoes/deletar", {
                    id: $(this).val()
                }, function(result){
                    location.reload();                  
                });
            }
        });
    });
</script>
</body>
</html>