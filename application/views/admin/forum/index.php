<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Fórum | Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
</head>
<body>
<?php $this->load->view("admin/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">
    <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <a class='btn btn-info btn-outline btn-lg hidden-xs pull-right' href="/forum/cadastrar"><i class="fa fa-plus"></i> Abrir Discussão</a>
                <h2 class="font-light m-b-xs">
                    Fórum
                </h2>
                <a class='btn btn-info btn-outline btn-block btn-lg visible-xs pull-right' href="/forum/cadastrar"><i class="fa fa-plus"></i> Abrir Discussão</a>
                <h3 class="text-info text-center">
                    <?= $this->session->flashdata("mensagem_forum") ?>
                </h3>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <?php if ($discussoes != null): ?>                            
                            <?php foreach ($discussoes as $discussao): ?>

                                <a href="/forum/ver/<?= $discussao->id ?>">
                                    <div class="panel-body">
                                        <div class="pull-right text-right">
                                            <h4><?= $discussao->nome_usuario ?> <i class="fa fa-user text-success"></i></h4>
                                            <?php if ($discussao->ativado == 0): ?>
                                                <span class="label label-danger"> Fechado </span>
                                            <?php endif ?>

                                        </div>
                                        <h4 class="m-b-xs">
                                            <?php if (strtotime($discussao->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                                <span class="label label-success">Novo</span>
                                            <?php endif ?>
                                            <?= $discussao->titulo ?>                                            
                                        </h4>
                                        <p class="small">Criado em: <strong><?= date("d/m/Y H:i", strtotime($discussao->data_abertura)) ?></strong></p>
                                    </div>
                                </a>

                            <?php endforeach ?>
                        <?php else: ?>
                            <div class="panel-body">
                                <h2 class="text-info text-center">Nenhuma discussão</h2>
                            </div>
                        <?php endif ?>
                        <?php if ($paginacao): ?>
                           <div class="panel-body">                       
                            <center>
                                <?= $paginacao ?>
                            </center>
                        </div>
                    <?php endif ?>
                </div>
            </div>
    </div>
    
    <?php $this->load->view("admin/inc/footer"); ?>
</div>
<?php $this->load->view("admin/inc/scripts_gerais") ?>
</body>
</html>