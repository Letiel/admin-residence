<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            Abrir Discussão
                        </div>
                        <div class="panel-body">
                            <form method="post">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="titulo" class="form-control" placeholder="Título da discussão" value="<?= set_value('titulo') ?>" />
                                    <span class="text-danger"><?php echo form_error("titulo") ?></span>
                                </div>
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea name="descricao" class="form-control" placeholder="Descrição"><?= set_value("descricao") ?></textarea>
                                    <span class="text-danger"><?php echo form_error("descricao") ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="pull-right">
                                        <button class="btn btn-primary">Abrir Discussão</button>
                                    </div>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
    </div>
    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script type="text/javascript">
        $(function(){

        });
    </script>
</body>
</html>