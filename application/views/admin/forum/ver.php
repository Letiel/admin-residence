<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("admin/inc/head_basico"); ?>
</head>
<body>
    <?php $this->load->view("admin/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel forum-box">
                        <div class="panel-heading hbuilt">
                            Fórum
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <button class="btn btn-<?= ($discussao->ativado ? "danger" : "success") ?> btn-block btn-outline" id="desativar"><?= ($discussao->ativado ? "Desativar" : "Reativar") ?> discussão</button>
                            </div>
                            <div class="media">
                                <div class="media-image pull-left">
                                    <img src="https://residence.acessonaweb.com.br/imagens/moradores/<?=  $discussao->foto_usuario ?>" alt="profile-picture">
                                    <div class="author-info">
                                        <strong><?= $discussao->nome_usuario ?></strong><br>
                                        <?= date("d/m/Y H:i", strtotime($discussao->data_alteracao)) ?>
                                        <?php if ($discussao->ativado == 0): ?>
                                            <br>
                                            <br>
                                            <span class="label label-danger"> Fechado </span>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4> <?= $discussao->titulo ?> </h4>
                                    <h5 class="text-justify"><?= strip_tags($discussao->descricao) ?></h5>
                                </div>
                            </div>
                            <h2 class="text-info text-center">
                                <?= $this->session->flashdata("mensagem_discussao") ?>
                                <?= ($total_respostas == 0 ? "Nenhuma resposta." : "") ?>
                            </h2>
                        </div>       
                        <?php foreach ($respostas as $resposta): ?>
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-image pull-left">
                                        <img src="https://residence.acessonaweb.com.br/imagens/moradores/<?= $resposta->foto_usuario; ?>" alt="profile-picture">
                                        <div class="author-info">
                                            <strong><?= $resposta->nome_usuario ?></strong><br>
                                            <?= date("d/m/Y H:i", strtotime($resposta->data_alteracao)) ?>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="text-justify"><?= strip_tags($resposta->resposta); ?></h5>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <div class="panel-body">
                            <div class="pull-right">
                                <?= $paginacao ?>
                            </div>
                            <?php if ($discussao->ativado == 1): ?>
                                <form method="post">
                                    <div class="form-group">
                                        <textarea name="resposta" class="form-control" placeholder="Sua Resposta"><?= set_value("descricao") ?></textarea>  
                                    </div>
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view("admin/inc/footer"); ?>
    </div>

    <?php $this->load->view("admin/inc/scripts_gerais") ?>
    <script type="text/javascript">
        $(function(){
            $("#desativar").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/forum/desativar", {
                    id: <?= $discussao->id ?>,
                }, function(result){
                    location.href="/forum";
                });
            });
        });
    </script>
</body>
</html>