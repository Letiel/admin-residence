<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Residence Online</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="/styles/style.css">

    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Raleway:300,700');
        body{
            background-color: #303F9F !important;
        }

        #login-content{
             padding-top: 200px;
        }
        #house{
            width: 30%;
            padding: 24px 44px;
        }

        form{
            width: 60%;
            padding: 0 30px;
        }
        form h5{
            color: white;
            margin: 0 0 20px 0;
        }
        #house, form{
            float: left;
        }

        h2.logotext{
            font-size: 30px;
            font-weight: 700;
            font-family: 'Raleway';
            color: white;
            text-align: left;
            text-transform: uppercase;
            letter-spacing: -3px;
            margin: 0 0 5px 0;
        }

        h2.logotext span{
             font-weight: 300;
        }


        .form-control{
            border-radius: 0;
            box-shadow: none;
            border: none;
        }
        .help-block{
            color: white !important;
        }

        .erro-login{
            color: white;
            font-weight: 700;
        }

        .btn-white-border,.btn-white-border:focus, .btn-white-border:hover, .btn-white-border:active {
            border-color: white;
            color:white;
            border-radius: 0;
            width: 150px;
            background-color: transparent;
        }

        .color-line{
                background: #34495e !important;
            }

        .splash{
            background: #303F9F;
            color: white;
        }

        .splash-title h1{
            font-size: 30px; 
            font-weight: 700; 
            font-family: 'Raleway'; 
            color: white; 
            text-align: center; 
            text-transform: uppercase; 
            letter-spacing: -1px;
            margin: 0 0 5px 0;
        }



        @media(max-width: 818px){
            #login-content{
                 padding-top: 67px;
            }
            #house{
                float: none;
                margin: auto;
            }

            form{
                float: none;
                margin: auto;
            }

            form h2.logotext, form h5{
                text-align: center;
            }

            .btn-white-border{
                width: 100%;
            }
        }

        @media(max-width: 518px){
            #house{
                width: 37%;
                padding: 17px 40px 26px 40px;
            }

            form{
                 width: 80%;
            }
        }
    </style>

</head>
<body>

    <!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Residence Online</h1><p>Gerencie seu condomínio facilmente através da interface web. </p><img src="/images/spinner.svg" width="64" height="64" /> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<div class="animate-panel" data-effect="fadeInUp">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" id="login-content">
            <div  id="house">
                <img src="/images/house.png" class="img-responsive" title="Residence Online" alt="Residence Online">
            </div>
            <form action="/" method="post">
                <h2 class="logotext">Residence Online <span>Admin</span></h2>
                <h5>Gerencie seu condomínio na internet.</h5>
                <h3 class="text-danger text-center"></h3>
                <div class="form-group">
                    <!-- <label class="control-label" for="username">Usuário</label> -->
                    <input type="text" placeholder="Usuário" title="Informe seu nome de usuário ou email" required="" value="" name="login" id="login" class="form-control">
                    <span class="help-block small">Seu nome de usuário de acesso.</span>
                </div>
                <div class="form-group">
                    <!-- <label class="control-label" for="password">Senha</label> -->
                    <input type="password" title="Informe sua senha." placeholder="Senha" required="" value="" name="senha" id="senha" class="form-control">
                    <span class="help-block small">Sua senha.</span>
                </div>
                <h4 class="pull-left erro-login"><?= $this->session->flashdata("mensagem_login") ?></h4>
                <button class="btn btn-white-border pull-right">Entrar</button>
            </form>
        </div>
    </div>
    <div class="row" style="margin-top: 90px;">
        <div class="col-md-12 text-center" style="color: white;">
            <strong>Residence Online</strong> - Gerencie seu condomínio na internet. <br/> 2017 Copyright Residence Online
        </div>
    </div>
</div>


<!-- Vendor scripts -->
<script src="/vendor/jquery/dist/jquery.min.js"></script>
<script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="/vendor/iCheck/icheck.min.js"></script>
<script src="/vendor/sparkline/index.js"></script>

<!-- App scripts -->
<script src="/scripts/homer.js"></script>

</body>
</html>