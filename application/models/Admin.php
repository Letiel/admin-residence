<?php
defined('BASEPATH') OR exit('Acesso negado');

class Admin extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function logado($admin = false){
		if($this->session->userdata("logado")){
			$this->db->where("usuarios.id", $this->session->userdata('id'));
			$this->db->join("condominios", "condominios.cnpj = usuarios.cnpj");
			$this->db->select("usuarios.id, usuarios.nome, usuarios.cnpj, usuarios.usuario_tipo, usuarios.foto,  condominios.data_validade");
			$usuarios = $this->db->get("usuarios");
			if($usuarios->num_rows() == 1){
				$usuario = $usuarios->first_row();
				$this->session->set_userdata(array(
					'logado'=>true,
					'id'=>$usuario->id,
					'nome'=>$usuario->nome,
					'cnpj'=>$usuario->cnpj,
					'admin'=>($usuario->usuario_tipo == "COLABORADOR" ? true : false),
					'imagem'=>$usuario->foto,
					'validade'=>$usuario->data_validade,
				));
			}
			if($admin){
				if($this->session->userdata("admin")){
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function login(){
		$post = (object) $this->input->post();
		if($post->login != null && $post->senha != null){
			$this->db->where("(login LIKE '$post->login' OR email LIKE '$post->login')");
			$this->db->where("senha", sha1($post->senha));
			$this->db->where("usuario_tipo", "COLABORADOR");
			$this->db->join("condominios", "condominios.cnpj = usuarios.cnpj");
			$this->db->select("usuarios.id, usuarios.nome, usuarios.cnpj, usuarios.usuario_tipo, condominios.data_validade, usuarios.ultimo_login, usuarios.foto");
			$usuarios = $this->db->get("usuarios");
			if($usuarios->num_rows() == 1){
				$usuario = $usuarios->first_row();
				if($usuario->data_validade >= date("Y-m-d H:i:s")){
					$this->session->set_userdata(array(
						'logado'=>true,
						'id'=>$usuario->id,
						'nome'=>$usuario->nome,
						'cnpj'=>$usuario->cnpj,
						'imagem'=>$usuario->foto,
						'ultimo_login'=>$usuario->ultimo_login,
						'validade'=>$usuario->data_validade,
						'admin'=>($usuario->usuario_tipo == "COLABORADOR" ? true : false)
					));

					// Calcular tempo de licença
					$data = $this->session->userdata("validade");
					$data_inicial = date("d/m/Y");
					$data_final = date("d/m/Y", strtotime($data));
					function geraTimestamp($data) {
						$partes = explode('/', $data);
						return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
					}
					$time_inicial = geraTimestamp($data_inicial);
					$time_final = geraTimestamp($data_final);
					$diferenca = $time_final - $time_inicial;
					$dias = (int)floor( $diferenca / (60 * 60 * 24));
					if ($dias <= 10) {
						$this->session->set_flashdata("validade", true);
						$this->session->set_flashdata("dias", $dias);
					}
					// Fim Calcular tempo de licença


					$this->db->where("usuarios.id", $usuario->id);
					$this->db->update("usuarios", array("ultimo_login"=>date("Y-m-d H:i:s")));
				}else{
					$this->session->set_flashdata("mensagem_login", "<p>Lincença vencida. <br /><small><small>Em ".date("d/m/Y", strtotime($usuario->data_validade))."</small></small></p>");
				}
			}else{
				$this->session->set_flashdata("mensagem_login", "Usuário ou senha inválidos.");
			}
		}
	}

	function enviarEmail($assunto, $para, $mensagem, $email = null){
		$this->load->library('email');

	    // Get full html:
		$body =
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>'.$assunto.'</title>
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			</head>
			<body style="margin: 0; padding: 0;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
								<tr>
									<td align="center" bgcolor="#4051b5" style="padding: 50px 40px 40px 40px">
										<img src="https://residence.acessonaweb.com.br/images/ResidenceOnline.png" alt="Residence Online" width="50%"  style="display: block;" />
									</td>
								</tr>
								<tr>
									<td bgcolor="#ffffff" style="padding: 60px 30px 60px 30px;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td>
													'.$assunto.'
												</td>
											</tr>
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<p>'.$mensagem.'</p>
												</td>
											</tr>
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<p>Esta é uma mensagem automática. Não responda este email.</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td bgcolor="#4051b5">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td bgcolor="#4051b5" style="padding: 30px 30px 30px 30px;">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="75%" style="color:#ffffff;">
																<a style="color: #FFF; text-decoration: none; font-size: 20px;" href="https://residence.acessonaweb.com.br">
																	&reg; Residence Online<br/>
																</a>
															</td>
															<td align="right">
																<table border="0" cellpadding="0" cellspacing="0">
																	<tr>
																		
																		<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</html>';

	    $endereco_remetente = (isset($email) ? $email : 'acessonaweb@acessonaweb.com.br');

	    $config['protocol'] = 'sendmail';
	    $config['mailpath'] = '/usr/sbin/sendmail';
	    $config['charset'] = 'utf-8';
	    $config['wordwrap'] = TRUE;
	    $config['mailtype'] = "html";

	    $this->email->initialize($config);

	    $result = $this->email
	        ->from($endereco_remetente)
	        ->to($para)
	        ->subject($assunto)
	        ->message($body)
	        ->send();
	}

	function enviarNotificacao($id_usuario, $titulo, $mensagem, $show = "true"){
		$this->db->where("usuarios.id", $id_usuario);
		$usuario = $this->db->get("usuarios")->first_row();
		$url = 'https://fcm.googleapis.com/fcm/send';
		if($usuario != null)
			$token = $usuario->token_app;
		else
			$token = null;
		if($token != null && $token != ""){
		    $fields = array (
		            'registration_ids' => array (
		                    $token
		            ),

		            'data' => array (
		                    "title" => $titulo,
		                    "message" => $mensagem,
		                    'show'=>$show
		            )
		    );
		    $fields = json_encode ( $fields );

		    $headers = array (
		            'Authorization: key=' . "AAAAQrEcs8Y:APA91bHhfL5ue-C3J78jmS1JyQOzt06c9cbR9OcjTUYU-eAoAeqS-Q2Cy9BZrAwTeB5X06VDXdopwMZrxRqk2PJGZqtmZzRKph6k-9RIYiAH5_Zglbw5j0JYZd6PoYOj10f-SJIFNV0i",
		            'Content-Type: application/json'
		    );

		    $ch = curl_init ();
		    curl_setopt ( $ch, CURLOPT_URL, $url );
		    curl_setopt ( $ch, CURLOPT_POST, true );
		    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		    $result = curl_exec ( $ch );
		    // echo $result;
		    curl_close ( $ch );
		}
	}

	function enviarNotificacaoAdmin($id_usuario, $titulo, $mensagem, $show = "true"){
		$this->db->where("usuarios.id", $id_usuario);
		$usuario = $this->db->get("usuarios")->first_row();
		$url = 'https://fcm.googleapis.com/fcm/send';
		if($usuario != null)
			$token = $usuario->token_app_admin;
		else
			$token = null;
		if($token != null && $token != ""){
		    $fields = array (
		            'registration_ids' => array (
		                    $token
		            ),

		            'data' => array (
		                    "title" => $titulo,
		                    "message" => $mensagem,
		                    'show'=>$show
		            )
		    );
		    $fields = json_encode ( $fields );

		    $headers = array (
		            'Authorization: key=' . "AAAA6x7jgXk:APA91bEBaXTW0_i_8V7TgRznsCkgZ3zmvBtY7CHzD6dfBnQFkdss5IR7ZV2KD0nNxFDV2pGZqpoiujX5wBXQlYeYIxPIm6aPCPJbaBSaxpRMmT3XXtPavAtoKa0lY_2smIAoMXw36q-i",
		            'Content-Type: application/json'
		    );

		    $ch = curl_init ();
		    curl_setopt ( $ch, CURLOPT_URL, $url );
		    curl_setopt ( $ch, CURLOPT_POST, true );
		    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		    $result = curl_exec ( $ch );
		    // echo $result;
		    curl_close ( $ch );
		}
	}

	function cadastros_gerais($tipo, $relacao = NULL){
		$this->db->where("tipo", $tipo);
		if($relacao != NULL)
			$this->db->where("relacao", $relacao);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("cadastros_gerais");
	}

	function ajax_cadastro_tipo_morador(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->insert("cadastros_gerais", $post);
	}

	function ajax_cadastro_localizacao(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("localizacoes", $post);
	}

	function ajax_cadastro_residencia(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("residencias", $post);
	}

	function select2_cadastros_gerais($tipo, $relacao = NULL){
		$post = $this->input->post();
		$this->db->order_by('nome', 'asc');
		$this->db->where("tipo", $tipo);
		$this->db->like("nome", $post['term']);
		if($relacao != NULL)
			$this->db->where("relacao", $relacao);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		return $this->db->get("cadastros_gerais");
	}

	function select2_getMoradores(){
		$post = $this->input->post();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		if($post['type'] != NULL && $post['type'] != "")
			$this->db->where("cadastros_gerais.tipo", $post['type']);
		$this->db->where("(usuarios.nome LIKE '%$post[term]%' OR residencias.nome LIKE '%$post[term]%' OR localizacoes.nome LIKE '%$post[term]%')");
		$this->db->join('residencias', 'residencias.id = usuarios.residencia', 'left');
		$this->db->join('localizacoes', 'residencias.localizacao = localizacoes.id', 'left');
		$this->db->join('cadastros_gerais', 'cadastros_gerais.id = usuarios.tipo', 'left');
		$this->db->select("usuarios.id, usuarios.nome, cadastros_gerais.nome as tipo, usuarios.situacao, IFNULL(residencias.nome, '') as residencia, IFNULL(localizacoes.nome, '') as localizacao");
		return $this->db->get("usuarios");
	}

	function select2_getLocalizacoes(){
		$post = $this->input->post();
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("localizacoes.nome LIKE '%$post[term]%' OR localizacoes.descricao LIKE '%$post[term]%'");
		$this->db->select("localizacoes.id, localizacoes.nome");
		return $this->db->get("localizacoes");
	}

	function select2_getResidencias(){
		$post = $this->input->post();
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		if(isset($post['term'])){
			$this->db->where("(residencias.nome LIKE '%$post[term]%' OR localizacoes.nome LIKE '%$post[term]%')");
		}
		$this->db->join('localizacoes', 'residencias.localizacao = localizacoes.id', 'left');
		$this->db->select("residencias.id, residencias.nome, localizacoes.nome as localizacao");
		return $this->db->get("residencias");
	}

	// INÍCIO MURAL ---------------------------------------------------------------------------------
	function getComuniqueMural(){
		$this->db->limit(5);
		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("(solicitacoes.remetente = ".$this->session->userdata("id")." OR solicitacoes.destinatario = ".$this->session->userdata("id").")");
		$this->db->where("mensagens_solicitacoes.id IN (SELECT MAX(id) FROM mensagens_solicitacoes WHERE mensagens_solicitacoes.solicitacao = solicitacoes.id)");
		// $this->db->where("solicitacoes.resolvida", 0);
		$this->db->join("mensagens_solicitacoes", "mensagens_solicitacoes.solicitacao = solicitacoes.id", "left");
		$this->db->join("usuarios", "mensagens_solicitacoes.remetente = usuarios.id", "left");
		$this->db->select("solicitacoes.*, mensagens_solicitacoes.mensagem, usuarios.nome as nome_remetente, mensagens_solicitacoes.data_alteracao as ultima_mensagem");
		$this->db->order_by("mensagens_solicitacoes.id DESC");
		$this->db->group_by("solicitacoes.id");
		return $this->db->get("solicitacoes");
		// $this->db->get("solicitacoes");
		// echo $this->db->last_query();
		// exit();
	}
	// FIM MURAL ------------------------------------------------------------------------------------
	// INÍCIO CRUD DE MORADORES ---------------------------------------------------------------------
	
	function getMoradores($inicio = null, $offset = null, $keyword){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);
		$this->db->where("(usuarios.nome LIKE '%$keyword%' OR cadastros_gerais.nome LIKE '%$keyword%')");
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		$this->db->join('cadastros_gerais', 'cadastros_gerais.id = usuarios.tipo', 'left');
		$this->db->join('residencias', 'residencias.id = usuarios.residencia', 'left');
		$this->db->join('localizacoes', 'localizacoes.id = residencias.localizacao', 'left');
		$this->db->select("usuarios.id, usuarios.nome, cadastros_gerais.nome as tipo, usuarios.situacao, residencias.nome as nome_residencia, localizacoes.nome as nome_localizacao");
		return $this->db->get("usuarios");
	}

	function cadastrar_morador(){
		if($this->logado(true)){
			$post = $this->input->post();
			$post['operador_alteracao'] = $this->session->userdata("id");
			$post['cnpj'] = $this->session->userdata("cnpj");
			$login = substr($post['nome'], 0, 3).substr(uniqid(), 10, 13);
			$senha = substr(uniqid(), 7, 13);
			$post['login'] = $login;
			$post['senha'] = sha1($senha);
			$post['data_alteracao'] = date("Y-m-d H:i:s");
			if($this->db->insert("usuarios", $post)){
				if($post['email'] != null && $post['email'] != ""){
					$mensagem = "
					<p>Você foi cadastrado(a) no <b><a style='text-decoration: none;' href='https://residence.acessonaweb.com.br'>Residence Online</a></b></p>
					<p>Seu login e senha para acesso no sistemas são:</p>
					<p>Login: <b>$login</b></p>
					<p>Senha: <b>$senha</b></p>
					<p><b>Atualize seu usuário e senha no primeiro acesso ao sistema!</b></p>
					";
					$this->enviarEmail("Você foi cadastrado no Residence Online!", $post['email'], $mensagem);
				}
				$this->session->set_flashdata("mensagem_cadastro_morador", "Morador cadastrado com sucesso!");
				redirect("/moradores");
			}
		}
	}

	function getMorador($id = NULL){
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("id", $id);
		return $this->db->get("usuarios");
	}

	function getTiposPorUsuario(){ //pegando para preencher os sub-tipos de usuário no form de edição de morador
		$post = $this->input->post();
		// $this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("cadastros_gerais.tipo", $post['tipo']);
		$this->db->select("id as value, nome as text");
		$subTipos = $this->db->get("cadastros_gerais");
		$total = $subtipos->num_rows();
		$subTipos = $subTipos->result();
		if($total == 0){
			echo "{\"value\": '0', \"text\": 'Nenhum cadastrado.'},";
		}else{
			echo "[";
			foreach($subTipos as $subTipo){
				echo "{\"value\": '$subTipo->value', \"text\": '$subTipo->text'},";
			}
			echo "]";
		}
	}

	function getLocalizacoes($inicio = NULL, $offset = NULL, $keyword = NULL){
		$this->db->limit($offset, $inicio);
		if($keyword != NULL)
			$this->db->where("localizacoes.nome LIKE '%$keyword%'");
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("localizacoes");
	}

	function getLocalizacao(){
		$post = $this->input->post();
		$this->db->where("localizacoes.id", $post['id']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("localizacoes");
	}

	function getResidenciasLocalizacao(){
		$post = $this->input->post();
		$this->db->where("residencias.localizacao", $post['id']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("residencias");
	}

	function getBoxesLocalizacao(){
		$post = $this->input->post();
		$this->db->where("boxes.localizacao", $post['id']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("boxes");
	}

	function ajax_edicao_localizacao(){
		$post = $this->input->post();
		$this->db->where("localizacoes.id", $post['id']);
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		unset($post['id']);
		$this->db->update("localizacoes", $post);
	}

	function deletar_localizacao(){
		$post = $this->input->post();
		$this->db->where("localizacoes.id", $post['id']);
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->delete("localizacoes");
	}

	function ajax_atualizar_subTipos(){
		$post = $this->input->post();
		$this->db->where("tipo", $post['tipo']);
		// $this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$tipos = $this->db->get("cadastros_gerais");
		$total = $tipos->num_rows();
		$tipos = $tipos->result();
		if($total == 0){
			echo "<option value=''>Nenhum resultado encontrado</option>";
		}else{
			foreach($tipos as $tipo){
				echo "<option ".($tipo->id == $post['selecionado'] ? 'selected' : '')." value='$tipo->id'>$tipo->nome</option>";
			}
		}
	}

	function ajax_atualizar_localizacoes(){ //retorna novas residencias sempre que o campo localização é alterado
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$localizacoes = $this->db->get("localizacoes");

		if($localizacoes->num_rows() > 0){
			$localizacoes = $localizacoes->result();
			foreach($localizacoes as $localizacao){
				echo "<option value='$localizacao->id'>$localizacao->nome</option>";
			}
		}else{
			echo "<option disabled value=''>Nenhuma encontrada</option>";
		}
	}

	function ajax_atualizar_residencias(){ //retorna novas residencias sempre que o campo localização é alterado
		$post = $this->input->post();
		$this->db->where("localizacao", $post['localizacao']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$residencias = $this->db->get("residencias");

		if($residencias->num_rows() > 0){
			$residencias = $residencias->result();
			foreach($residencias as $residencia){
				echo "<option ".($residencia->id == $post['selecionado'] ? 'selected' : '')." value='$residencia->id'>$residencia->nome</option>";
			}
		}else{
			echo "<option disabled value=''>Nenhuma encontrada</option>";
		}
	}

	function editable_morador($name, $value, $pk){
		$this->db->where("id", $pk);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$array = array(
			"$name"=>"$value"
		);
		$this->db->update("usuarios", $array);
	}

	function editable_atualizar_subTipos(){ //retorna novos sub-tipos sempre que o campo tipos é alterado
		$post = $this->input->post();
		$this->db->where("tipo", $post['tipo']);
		// $this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$tipos = $this->db->get("cadastros_gerais");
		$total = $tipos->num_rows();
		$tipos = $tipos->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($tipos as $tipo){
				$print = "{\"value\": \"$tipo->value\", \"text\": \"$tipo->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function editable_atualizar_localizacoes(){
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$localizacoes = $this->db->get("localizacoes");
		$total = $localizacoes->num_rows();
		$localizacoes = $localizacoes->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($localizacoes as $localizacao){
				$print = "{\"value\": \"$localizacao->value\", \"text\": \"$localizacao->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function editable_atualizar_residencias(){ //retorna novas residências para o x-editable sempre que o campo localizações é alterado
		$post = $this->input->post();
		$this->db->where("localizacao", $post['localizacao']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$residencias = $this->db->get("residencias");
		$total = $residencias->num_rows();
		$residencias = $residencias->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($residencias as $residencia){
				$print = "{\"value\": \"$residencia->value\", \"text\": \"$residencia->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function atualizar_foto_morador(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("usuarios.id", $post['id']);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("usuarios.foto, usuarios.id");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		$total = $usuario->num_rows();
		if($total == 1){
			$usuario = $usuario->first_row();
			$this->db->where("usuarios.id", $post['id']);
			$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
			$this->db->update("usuarios", array('foto'=>$post['foto']));
		}
	}

	function deletar_morador(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("usuarios");
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->session->set_flashdata("mensagem_cadastro_morador", "Removido com sucesso");
		}
	}

	function ajax_reenviar_login_senha(){
		$post = $this->input->post();

		$login = substr($post['email'], 0, 3).substr(uniqid(), 10, 13);
		$senha = substr(uniqid(), 7, 13);
		$post['login'] = $login;
		$post['senha'] = sha1($senha);
		$this->db->where("usuarios.id", $post['id']);
		$this->db->set($post);
		if($this->db->update('usuarios')){
			if($post['email'] != null && $post['email'] != ""){
				$mensagem = "
				<p>Olá. Um dos administradores do seu condomínio solicitou o envio de novos dados de acesso do <a style='text-decoration: none;' href='https://residence.acessonaweb.com.br'>Residence Online</a></b>.</p>
				<p>Seu login e senha para acesso no sistemas são:</p>
				<p>Login: <b>$login</b></p>
				<p>Senha: <b>$senha</b></p>
				<p><b>Atualize seu usuário e senha no primeiro acesso ao sistema!</b></p>
				";
				$this->enviarEmail("Residence Online! - Dados de acesso", $post['email'], $mensagem);
			}
			$this->session->set_flashdata("mensagem_cadastro_morador", "Dados enviados com sucesso!");
		}
	}

	// FIM CRUD DE MORADORES ---------------------------------------------------------------------------
	// CRUD DE VEÍCULOS --------------------------------------------------------------------------------

	function getVeiculos($inicio = null, $offset = null, $keyword = null){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);
		$this->db->where("(veiculos.placa LIKE '%$keyword%' OR veiculos.modelo LIKE '%$keyword%' OR veiculos.fabricante LIKE '%$keyword%' OR usuarios.nome LIKE '%$keyword%' OR residencias.nome LIKE '%$keyword%' OR localizacoes.nome LIKE '%$keyword%')");
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("(cadastros_gerais.cnpj = ".$this->session->userdata('cnpj')." OR cadastros_gerais.cnpj = '' OR cadastros_gerais.cnpj IS NULL)");
		$this->db->where("(cadastros_gerais.tipo = 'VEICULOS' OR cadastros_gerais.tipo = '' OR cadastros_gerais.tipo IS NULL)");
		$this->db->join("usuarios", "usuarios.id = veiculos.usuario_responsavel", "left");
		$this->db->join("residencias", "usuarios.residencia = residencias.id", "left");
		$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", "left");
		$this->db->select("veiculos.*, cadastros_gerais.nome as tipo_veiculo, usuarios.nome as usuario, residencias.nome as residencia, localizacoes.nome as localizacao");
		$this->db->join("cadastros_gerais", "veiculos.tipo = cadastros_gerais.id", "left");
		return $this->db->get("veiculos");
	}

	function getVeiculo($id = NULL){
		if($id != null)
			$this->db->where("veiculos.id", $id);
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("(cadastros_gerais.cnpj = ".$this->session->userdata('cnpj')." OR cadastros_gerais.cnpj = '' OR cadastros_gerais.cnpj IS NULL)");
		$this->db->join("usuarios", "usuarios.id = veiculos.usuario_responsavel", "left");
		$this->db->join("residencias", "veiculos.residencia = residencias.id", "left");
		$this->db->join("residencias as u_residencias", "usuarios.residencia = u_residencias.id", "left");
		$this->db->join("localizacoes as u_localizacoes", "u_residencias.localizacao = u_localizacoes.id", "left");
		$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", "left");
		$this->db->join("cadastros_gerais", "veiculos.tipo = cadastros_gerais.id", "left");
		$this->db->select("veiculos.*, cadastros_gerais.nome as tipo_veiculo, usuarios.id as id_usuario, usuarios.nome as nome_usuario, residencias.id as id_residencia, residencias.nome as nome_residencia, u_residencias.id as id_u_residencia, u_residencias.nome as nome_u_residencia, localizacoes.nome as localizacao, u_localizacoes.nome as u_localizacao, u_localizacoes.id as id_u_localizacao");
		return $this->db->get("veiculos");
	}

	function ajax_cadastro_tipo_veiculo(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['tipo'] = "VEICULOS";
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("cadastros_gerais", $post);
	}

	function cadastrar_veiculo(){
		$post = $this->input->post();
		if($this->logado(true)){
			$post = $this->input->post();
			$post['operador_alteracao'] = $this->session->userdata("id");
			$post['cnpj'] = $this->session->userdata("cnpj");
			$post['data_alteracao'] = date("Y-m-d H:i:s");
			if($this->db->insert("veiculos", $post)){
				$this->session->set_flashdata("mensagem_cadastro_veiculo", "Veículo cadastrado com sucesso!");
				redirect("/veiculos");
			}
		}
	}

	function preencher_select_veiculo_residencia(){
		$post = $this->input->post();
		$this->db->where("usuarios.id", $post['id']);
		$this->db->join("usuarios", "usuarios.residencia = residencias.id", 'left');
		$this->db->join("localizacoes", "localizacoes.id = residencias.localizacao", 'left');
		$this->db->select("residencias.id, residencias.nome, localizacoes.nome as localizacao");
		$residencia = $this->db->get("residencias");
		if($residencia->num_rows() == 1){
			$residencia = $residencia->first_row();
			echo "<option value='$residencia->id' selected>$residencia->nome $residencia->localizacao</option>";
		}
	}

	function editable_veiculo($name, $value, $pk){
		$this->db->where("id", $pk);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$array = array(
			"$name"=>"$value"
		);
		$this->db->update("veiculos", $array);
	}

	function atualizar_foto_veiculo(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("veiculos.id", $post['id']);
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("veiculos.foto, veiculos.id");
		$veiculo = $this->db->get("veiculos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		$total = $veiculo->num_rows();
		if($total == 1){
			$veiculo = $veiculo->first_row();
			$this->db->where("veiculos.id", $post['id']);
			$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
			$this->db->update("veiculos", array('foto'=>$post['foto']));
		}
	}

	function editable_pegar_residencia(){ //x-editable: atualizando residências baseado no usuário
		$post = $this->input->post();
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.id", $post['usuario']);
		$this->db->join("usuarios", "usuarios.residencia = residencias.id", "left");
		$residencias = $this->db->get("residencias");
		if($residencias->num_rows() == 1){
			$residencias = $residencias->first_row();
			echo "<option value=''>Selecione...</option>";
			foreach($residencias as $residencia){
				echo "<option value='$residencia->id'>$residencia->nome</option>";
			}
		}
	}

	function editable_trocar_usuario_responsavel_veiculo(){
		$post = $this->input->post();
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("veiculos.id", $post['veiculo']);
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->update("veiculos", array("usuario_responsavel"=>$post['usuario_responsavel']));
	}

	function editable_trocar_residencia_veiculo(){
		$post = $this->input->post();
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("veiculos.id", $post['veiculo']);
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->update("veiculos", array("residencia"=>$post['residencia']));
	}

	function deletar_veiculo(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("veiculos");
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->session->set_flashdata("mensagem_cadastro_veiculo", "Removido com sucesso");
		}
	}

	// FIM CRUD DE VEÍCULOS --------------------------------------------------------------------
	// INÍCIO CRUD DE ÁREAS --------------------------------------------------------------------

	function ajax_cadastro_area(){
		$post = $this->input->post();
		$dados['nome'] = $post['nome'];
		$dados['localizacao'] = $post['localizacao'];
		$dados['usuario_responsavel'] = $post['usuario_responsavel'];
		$dados['descricao'] = $post['descricao'];
		$dados['cnpj'] = $this->session->userdata("cnpj");
		$dados['operador_alteracao'] = $this->session->userdata("id");
		$dados['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("areas", $dados);
		$inserido = $this->db->insert_id();
		if($post['domingo'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_domingo'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_domingo'])),
				'dia_repetivel'=>"DOMINGO",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['segunda'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_segunda'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_segunda'])),
				'dia_repetivel'=>"SEGUNDA",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['terca'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_terca'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_terca'])),
				'dia_repetivel'=>"TERCA",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['quarta'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_quarta'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_quarta'])),
				'dia_repetivel'=>"QUARTA",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['quinta'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_quinta'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_quinta'])),
				'dia_repetivel'=>"QUINTA",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['sexta'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_sexta'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_sexta'])),
				'dia_repetivel'=>"SEXTA",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		if($post['sabado'] == "true"){
			$horario = array(
				'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_sabado'])),
				'horario_final'=>date("H:i:s", strtotime($post['h_final_sabado'])),
				'dia_repetivel'=>"SABADO",
				'area'=>$inserido,
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("horarios_areas", $horario);
		}
		$this->session->set_flashdata("mensagem_cadastro_area", "Área cadastrada com sucesso.");
	}

	function getAreas($inicio = null, $offset = null, $keyword = null){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);
		$this->db->where("(areas.nome LIKE '%$keyword%' OR usuarios.nome LIKE '%$keyword%' OR localizacoes.nome LIKE '%$keyword%')");
		$this->db->where("areas.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = areas.usuario_responsavel", "left");
		$this->db->join("localizacoes", "localizacoes.id = areas.localizacao", "left");
		$this->db->select("areas.*, usuarios.nome as nome_usuario, localizacoes.nome as nome_localizacao");
		return $this->db->get("areas");
	}

	function getArea(){
		$post = $this->input->post();
		$this->db->where("areas.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("areas.id", $post['id']);
		$this->db->join("usuarios", "usuarios.id = areas.usuario_responsavel", "left");
		$this->db->join("localizacoes", "localizacoes.id = areas.localizacao", "left");
		$this->db->join("residencias", "residencias.id = usuarios.residencia", "left");
		$this->db->join("localizacoes as u_localizacao", "u_localizacao.id = usuarios.localizacao", "left");
		$this->db->select("areas.*, usuarios.nome as nome_usuario, localizacoes.nome as nome_localizacao, residencias.nome as residencia_usuario, u_localizacao.nome as localizacao_usuario");
		return $this->db->get("areas");
	}

	function ajax_edicao_area(){
		$post = $this->input->post();
		$dados['operador_alteracao'] = $this->session->userdata("id");
		$dados['nome'] = $post['nome'];
		$dados['localizacao'] = $post['localizacao'];
		$dados['usuario_responsavel'] = $post['usuario_responsavel'];
		$dados['descricao'] = $post['descricao'];
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("id", $post['id']);
		$this->db->update("areas", $dados);

		if($post['domingo'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'DOMINGO' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_domingo'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_domingo'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'DOMINGO');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_domingo'],
					'horario_final'=>$post['h_final_domingo'],
					'dia_repetivel'=>"DOMINGO",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "DOMINGO");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}

		if($post['segunda'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'SEGUNDA' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_segunda'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_segunda'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'SEGUNDA');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_segunda'],
					'horario_final'=>$post['h_final_segunda'],
					'dia_repetivel'=>"SEGUNDA",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "SEGUNDA");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}

		if($post['terca'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'TERCA' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_terca'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_terca'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'terca');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_terca'],
					'horario_final'=>$post['h_final_terca'],
					'dia_repetivel'=>"TERCA",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "TERCA");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));

			$this->db->delete("horarios_areas");
		}

		if($post['quarta'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'QUARTA' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_quarta'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_quarta'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'QUARTA');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_quarta'],
					'horario_final'=>$post['h_final_quarta'],
					'dia_repetivel'=>"QUARTA",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "QUARTA");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}

		if($post['quinta'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'QUINTA' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_quinta'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_quinta'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'QUINTA');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_quinta'],
					'horario_final'=>$post['h_final_quinta'],
					'dia_repetivel'=>"QUINTA",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "QUINTA");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}

		if($post['sexta'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'SEXTA' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_sexta'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_sexta'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'SEXTA');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_sexta'],
					'horario_final'=>$post['h_final_sexta'],
					'dia_repetivel'=>"SEXTA",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "SEXTA");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}

		if($post['sabado'] == "true"){
			$this->db->start_cache();
			$horario = $this->db->query("SELECT id FROM horarios_areas WHERE dia_repetivel = 'SABADO' AND area = $post[id]")->first_row();
			if($horario != null){
				$array = array(
					'horario_inicio'=>date("H:i:s", strtotime($post['h_inicial_sabado'])),
					'horario_final'=>date("H:i:s", strtotime($post['h_final_sabado'])),
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				$this->db->where("horarios_areas.dia_repetivel", 'SABADO');
				$this->db->where("horarios_areas.id", $horario->id);
				$this->db->update("horarios_areas", $array);
			}else{
				$array = array(
					'horario_inicio'=>$post['h_inicial_sabado'],
					'horario_final'=>$post['h_final_sabado'],
					'dia_repetivel'=>"SABADO",
					'operador_alteracao'=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
					'area'=>$post['id'],
					'cnpj'=>$this->session->userdata("cnpj")
				);
				$this->db->insert("horarios_areas", $array);
			}
			$this->db->stop_cache();
			$this->db->flush_cache();
		}else{
			$this->db->where("horarios_areas.dia_repetivel", "SABADO");
			$this->db->where("horarios_areas.area", $post['id']);
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("horarios_areas");
		}
		$this->session->set_flashdata("mensagem_cadastro_area", "Área atualizada com sucesso.");
	}

	function deletar_area(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("areas");
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->session->set_flashdata("mensagem_cadastro_area", "Removido com sucesso");
		}
	}
	// FIM CRUD DE ÁREAS --------------------------------------------------------------------
	// INICIO CRUD DE RESIDÊNCIAS------------------------------------------------------------
	function getResidencias($inicio = null, $offset = null, $keyword = null){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);
		$this->db->where("(residencias.nome LIKE '%$keyword%' OR residencias.descricao LIKE '%$keyword%' OR usuarios.nome LIKE '%$keyword%' OR localizacoes.nome LIKE '%$keyword%')");
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.residencia = residencias.id", "left");
		$this->db->join("localizacoes", "localizacoes.id = residencias.localizacao", "left");
		$this->db->select("residencias.nome, residencias.id, residencias.descricao, residencias.localizacao, localizacoes.nome as nome_localizacao, COUNT(usuarios.id) as total_moradores");
		$this->db->group_by("residencias.id");
		$this->db->order_by("residencias.nome, localizacoes.nome");
		return $this->db->get("residencias");
	}

	function getResidencia(){
		$post = $this->input->post();
		$this->db->where("residencias.id", $post['id']);
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.residencia = residencias.id", "left");
		$this->db->join("localizacoes", "localizacoes.id = residencias.localizacao", "left");
		
		$this->db->select("residencias.nome, residencias.id, residencias.descricao, residencias.localizacao, localizacoes.nome as nome_localizacao, COUNT(usuarios.id) as total_moradores");
		return $this->db->get("residencias");
	}

	function getMoradoresResidencia(){
		$post = $this->input->post();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("cadastros_gerais", "cadastros_gerais.id = usuarios.tipo", 'left');
		$this->db->join("residencias", "residencias.id = usuarios.residencia");
		$this->db->join("localizacoes", "localizacoes.id = residencias.localizacao");
		$this->db->where("usuarios.residencia", $post['id']);
		$this->db->select("usuarios.id, usuarios.nome, cadastros_gerais.nome as nome_tipo, residencias.nome as nome_residencia, localizacoes.nome as nome_localizacao");
		return $this->db->get("usuarios");
	}

	function getBoxesResidencia(){
		$post = $this->input->post();
		$this->db->where("boxes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("residencias", "residencias.id = boxes.residencia");
		$this->db->join("localizacoes", "localizacoes.id = boxes.localizacao");
		$this->db->where("boxes.residencia", $post['id']);
		$this->db->select("boxes.*, localizacoes.nome as nome_localizacao, localizacoes.id as id_localizacao");
		return $this->db->get("boxes");
	}

	function getVeiculosResidencia(){
		$post = $this->input->post();
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		
		$this->db->where("veiculos.residencia", $post['id']);
		$this->db->select("veiculos.*");
		return $this->db->get("veiculos");
	}

	function ajax_edicao_residencia(){
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("id", $post['id']);
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->update("residencias", $post);
		$this->session->set_flashdata("mensagem_cadastro_area", "Área atualizada com sucesso.");
	}

	function deletar_residencia(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("usuarios.residencia", $post['id']);
			$dados = array("residencia"=>null);
			$this->db->update("usuarios", $dados);
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("residencias");
			$this->db->flush_cache();
			$this->db->stop_cache();
			$this->session->set_flashdata("mensagem_cadastro_residencia", "Removido com sucesso");
		}
	}
	// FIM CRUD DE RESIDÊNCIAS------------------------------------------------------------
	// INÍCIO CRUD DE BOXES------------------------------------------------------------
	function getBoxes($inicio = null, $offset = null, $keyword){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);

		$keyword = str_replace(" ", "%", $keyword);
		$this->db->where("(boxes.nome LIKE '%$keyword%' OR residencias.nome LIKE '%$keyword%' OR localizacoes.nome LIKE '%$keyword%')");

		$this->db->where("boxes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("residencias", "residencias.id = boxes.residencia", "left");
		$this->db->join("localizacoes as r_localizacoes", "residencias.localizacao = r_localizacoes.id", "left");
		$this->db->join("localizacoes", "boxes.localizacao = localizacoes.id", "left");
		$this->db->select("residencias.nome as nome_residencia, boxes.tipo, boxes.nome, boxes.id, boxes.descricao, boxes.residencia as id_residencia, residencias.descricao as descricao_residencia, localizacoes.nome as nome_localizacao, r_localizacoes.nome as nome_r_localizacao");
		$this->db->order_by("boxes.nome, residencias.nome");
		return $this->db->get("boxes");
	}

	function getBox(){
		$post = $this->input->post();
		$this->db->where("boxes.id", $post['id']);
		$this->db->where("boxes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("residencias", "residencias.id = boxes.residencia", "left");
		$this->db->join("localizacoes as r_localizacoes", "residencias.localizacao = r_localizacoes.id", "left");
		$this->db->join("localizacoes", "boxes.localizacao = localizacoes.id", "left");
		$this->db->select("residencias.nome as nome_residencia, boxes.tipo, boxes.nome, boxes.id, boxes.descricao, boxes.residencia as id_residencia, residencias.descricao as descricao_residencia, localizacoes.id as id_localizacao, localizacoes.nome as nome_localizacao, r_localizacoes.nome as nome_r_localizacao, r_localizacoes.nome as nome_r_localizacao");
		$this->db->order_by("boxes.nome, residencias.nome");
		return $this->db->get("boxes");
	}

	function ajax_cadastro_box(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("boxes", $post);
	}

	function ajax_edicao_box(){
		$post = $this->input->post();
		$this->db->where("id", $post['id']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->update("boxes", $post);
	}

	function deletar_box(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("boxes");
			$this->db->flush_cache();
			$this->db->stop_cache();
			$this->session->set_flashdata("mensagem_cadastro_boxes", "Removido com sucesso");
		}
	}
	// FIM CRUD DE BOXES---------------------------------------------------------------
	// INICIO CRUD DE RESERVAS---------------------------------------------------------
	function getReservas(){
		$this->db->where("reservas.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = reservas.usuario", "LEFT");
		$this->db->join("residencias", "residencias.id = usuarios.residencia", "LEFT");
		$this->db->join("localizacoes", "localizacoes.id = usuarios.localizacao", "LEFT");
		$this->db->join("areas", "areas.id = reservas.area", "LEFT");
		$this->db->order_by("id", "DESC");
		$this->db->select("reservas.*, areas.nome as nome_area, usuarios.id as id_usuario, usuarios.nome as nome_usuario, residencias.nome as nome_residencia, localizacoes.nome as nome_localizacao");
		return $this->db->get("reservas");
	}

	function getReserva(){
		$post = $this->input->post();
		$this->db->where("reservas.id", $post['id']);
		$this->db->where("reservas.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = reservas.usuario", "LEFT");
		$this->db->join("residencias", "residencias.id = usuarios.residencia", "LEFT");
		$this->db->join("localizacoes", "localizacoes.id = usuarios.localizacao", "LEFT");
		$this->db->join("areas", "areas.id = reservas.area", "LEFT");
		$this->db->order_by("id", "DESC");
		$this->db->select("reservas.*, areas.nome as nome_area, usuarios.id as id_usuario, usuarios.nome as nome_usuario, residencias.nome as nome_residencia, localizacoes.nome as nome_localizacao");
		return $this->db->get("reservas");
	}

	function getConvidados(){
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("reserva", $post['id']);
		$this->db->order_by("nome", "ASC");
		return $this->db->get("convidados_reservas");
	}

	function autorizar_reserva(){
		$post = $this->input->post();
		$this->db->query("UPDATE reservas set autorizado = !autorizado WHERE id = $post[id]");
		$this->session->set_flashdata("mensagemReserva", "Sucesso!");
		$this->db->start_cache();
		$this->db->where("reservas.id", "$post[id]");
		$this->db->join("areas", "areas.id = reservas.area");
		$this->db->join("usuarios", "usuarios.id = reservas.usuario", 'left');
		$this->db->select("reservas.*, usuarios.nome as nome_usuario, usuarios.email as email_usuario, areas.nome as nome_area");
		$reserva = $this->db->get("reservas")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($reserva != null){
			if($reserva->email_usuario != null && trim($reserva->email_usuario) != ""){
				$autorizada = ($reserva->autorizado ? "autorizada" : "desautorizada");
				$assunto = "Sua reserva foi $autorizada - Residence Online";
				$para = $reserva->email_usuario;
				$mensagem = "Sua reserva em <b>$reserva->nome_area</b> no período de <b>".date("d/m/Y H:i:s" , strtotime($reserva->hora_inicio))."</b> até <b>".date("d/m/Y H:i:s", strtotime($reserva->hora_fim))."</b> foi $autorizada. Você pode atualizar sua lista de convidados acessando a reserva no <a href='https://residence.acessonaweb.com.br'>sistema</a>.";

				if($reserva->token_app != null && $reserva->token_app != ""){
					if($reserva->id_usuario != $this->session->userdata("id"))
						$this->enviarNotificacao($reserva->id_usuario, $assunto, "Sua reserva em $reserva->nome_area foi autorizada.");
				}else{
					$this->enviarEmail($assunto, $para, $mensagem);
				}
			}
		}
	}
	// FIM CRUD DE RESERVAS------------------------------------------------------------
	// INÍCIO CRUD DE ENQUETES---------------------------------------------------------
	function criar_enquete(){
		$post = $this->input->post();
		$post['data_validade'] = explode(" ", $post['data_validade']);
		$post['data_validade'][0] = explode("/", $post['data_validade'][0]);
		$post['data_validade'] = $post['data_validade'][0][2]."-".$post['data_validade'][0][1]."-".$post['data_validade'][0][0]." ".$post['data_validade'][1];
		$this->db->start_cache();
		$dados = array(
			"titulo"=>$post['titulo'],
			"descricao"=>$post['descricao'],
			"data_validade"=>$post['data_validade'],
			"usuario"=>$this->session->userdata("id"),
			"cnpj"=>$this->session->userdata("cnpj"),
			"operador_alteracao"=>$this->session->userdata("id"),
			'data_abertura' => date("Y-m-d H:i:s"),
			'data_alteracao'=>date("Y-m-d H:i:s")
		);
		$this->db->insert("enquetes", $dados);
		$inserido = $this->db->insert_id();
		$this->db->stop_cache();
		$this->db->flush_cache();

		foreach($post['opcoes'] as $opcao){
			$dados = array(
				"titulo"=>$opcao,
				"enquete"=>$inserido,
				"cnpj"=>$this->session->userdata("cnpj"),
				"operador_alteracao"=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("opcoes_enquetes", $dados);
		}
		$this->session->set_flashdata("mensagem_enquete", "Enquete criada com sucesso.");

		$this->db->start_cache();
		$this->db->where("enquetes.id", $inserido);
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario");
		$this->db->select("usuarios.email as email_usuario, enquetes.*");
		$enquete = $this->db->get("enquetes")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.id != ".$this->session->userdata('id'));
		$this->db->select("usuarios.email as email_usuario, usuarios.id as id_usuario, usuarios.token_app");
		$usuarios = $this->db->get("usuarios")->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		foreach($usuarios as $usuario){
			if($usuario->email_usuario != null && trim($usuario->email_usuario) != ""){
				$mensagem = "
				Uma nova enquete está aberta no seu condomínio. Sua opinião é muito importante.
				<h3>$enquete->titulo</h3>
				Para ver a enquete, acesse o aplicativo ou site do <a href='https://residence.acessonaweb.com.br'><b>Residence Online</b></a> e dê sua opinião.<br />
				";

				if($usuario->token_app != null && $usuario->token_app != ""){
					if($usuario->id_usuario != $this->session->userdata("id"))
						$this->enviarNotificacao($usuario->id_usuario, "Nova enquete no condomínio!", $post['titulo']);
				}else{
					$this->enviarEmail("Nova enquete aberta no condomínio.", $usuario->email_usuario, $mensagem);
				}
			}
		}

		redirect("/enquetes");
	}

	function getEnquetes($inicio = NULL, $offset = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("enquetes.cnpj", $this->session->userdata("cnpj"));

		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');

		$this->db->join("opcoes_enquetes", "enquetes.id = opcoes_enquetes.enquete", 'left');
		$this->db->join("votos_enquetes", "opcoes_enquetes.id = votos_enquetes.voto", 'left');

		$this->db->select("enquetes.*, usuarios.nome as nome_usuario, COUNT(votos_enquetes.id) as total_votos");
		$this->db->order_by("enquetes.data_validade DESC, enquetes.ativado DESC, enquetes.id DESC");
		$this->db->group_by("enquetes.id");
		return $this->db->get("enquetes");
	}

	function getEnquete($id){
		$this->db->where("enquetes.id", $id);
		$this->db->where("enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');
		$this->db->select("enquetes.*, usuarios.nome as nome_usuario");
		$this->db->order_by("enquetes.id", "DESC");
		return $this->db->get("enquetes");
	}

	function getVotoEnquete($enquete){ //verificando se o usuário já votou na enquete
		$this->db->where("votos_enquetes.usuario", $this->session->userdata("id"));
		$this->db->where("enquetes.id", $enquete);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("opcoes_enquetes", "opcoes_enquetes.id = votos_enquetes.voto", 'left');
		$this->db->join("enquetes", "enquetes.id = opcoes_enquetes.enquete", 'left');
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');
		$this->db->select("votos_enquetes.voto, opcoes_enquetes.titulo, votos_enquetes.data_alteracao");
		return $this->db->get("votos_enquetes");
	}

	function votosEnquete(){
		$this->db->where("votos_enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->group_by("opcoes_enquetes.titulo");
		$this->db->join("opcoes_enquetes", "opcoes_enquetes.id = votos_enquetes.voto");
		$this->db->join("enquetes", "enquetes.id = opcoes_enquetes.enquete");
		$this->db->select("COUNT(votos_enquetes.id) as total_votos, opcoes_enquetes.titulo as nome_opcao, enquetes.titulo as nome_enquete");
		return $this->db->get("votos_enquetes");
	}

	function getOpcoesEnquete($enquete){
		$this->db->where("opcoes_enquetes.enquete", $enquete);
		$this->db->where("opcoes_enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("votos_enquetes", "votos_enquetes.voto = opcoes_enquetes.id", 'LEFT');
		$this->db->select("opcoes_enquetes.*, COUNT(votos_enquetes.id) as total_votos");
		$this->db->order_by("opcoes_enquetes.id", "ASC");
		$this->db->group_by("opcoes_enquetes.titulo");
		return $this->db->get("opcoes_enquetes");
	}

	function votarEnquete(){
		$post = $this->input->post();
		$post['usuario'] = $this->session->userdata("id");
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("votos_enquetes", $post);
		$this->session->set_flashdata("mensagem_enquete", "Você votou com sucesso!");
		redirect("/enquetes");
	}

	function alterar_validade_enquete(){
		$post = $this->input->post();
		$post['data_validade'] = explode(" ", $post['data_validade']);
		$post['data_validade'][0] = explode("/", $post['data_validade'][0]);
		$post['data_validade'] = $post['data_validade'][0][2]."-".$post['data_validade'][0][1]."-".$post['data_validade'][0][0]." ".$post['data_validade'][1];
		$dados = array(
			'data_validade'=>$post['data_validade']
		);
		$this->db->where("enquetes.id", $post['id']);
		$this->db->update("enquetes", $dados);
		$this->session->set_flashdata("mensagem_enquete", "Alterado com sucesso.");
	}

	function desativar_enquete(){
		$post = $this->input->post();
		
		// $dados = array(
		// 	'ativado'=>false
		// );
		// $this->db->where("enquetes.id", $post['id']);
		// $this->db->update("enquetes", $dados);
		$this->db->query("UPDATE enquetes SET ativado = !ativado WHERE id = $post[id]");
		$this->session->set_flashdata("mensagem_enquete", "Alterado com sucesso.");
	}
	// FIM CRUD DE ENQUETES------------------------------------------------------------
	// INICIO CRUD DE FORUM------------------------------------------------------------
	function cadastrar_discussao(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['usuario'] = $this->session->userdata("id");
		$post['data_abertura'] = date("Y-m-d H:i:s");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("discussoes_forum", $post);
		$this->session->set_flashdata("mensagem_forum", "Discussão criada com sucesso!");
		redirect("/forum");
	}

	function getDiscussoes($inicio = NULL, $offset = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("discussoes.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("(discussoes.ativado = 1 OR discussoes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = discussoes.usuario", "left");
		$this->db->select("discussoes.*, usuarios.nome as nome_usuario");
		$this->db->order_by("id", "DESC");
		return $this->db->get("discussoes_forum as discussoes");
	}

	function getDiscussao($id){
		$this->db->where("discussoes.id", $id);
		$this->db->where("discussoes.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("(discussoes.ativado = 1 OR discussoes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = discussoes.usuario", "left");
		$this->db->select("discussoes.*, usuarios.nome as nome_usuario, usuarios.foto as foto_usuario");
		return $this->db->get("discussoes_forum as discussoes");
	}

	function responder_discussao($id = NULL){
		if($id != NULL){
			$post = $this->input->post();
			$dados = array(
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'usuario'=>$this->session->userdata("id"),
				'discussao'=>$id,
				'resposta'=>$post['resposta'],
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("respostas_discussoes", $dados);
			$this->session->set_flashdata("mensagem_discussao", "Respondido com sucesso!");

			$inserido = $this->db->insert_id();
			$this->db->start_cache();
			$this->db->where("respostas_discussoes.id", $inserido);
			$this->db->where("discussoes_forum.usuario != ".$this->session->userdata("id"));
			$this->db->join("discussoes_forum", "respostas_discussoes.discussao = discussoes_forum.id");
			$this->db->join("usuarios as dono", "dono.id = discussoes_forum.usuario");
			$this->db->join("usuarios as respondente", "respondente.id = respostas_discussoes.usuario");
			$this->db->select("discussoes_forum.titulo as nome_discussao, dono.email as email_dono, dono.token_app, dono.id as id_usuario, respondente.email as email_respondente");
			$resposta = $this->db->get("respostas_discussoes")->first_row();
			$this->db->flush_cache();
			$this->db->stop_cache();

			if($resposta != null){
				if($resposta->email_dono != null){
					$mensagem = "
					<p>Você recebeu uma nova mensagem na sua discussão <b>$resposta->nome_discussao</b>.</p>
					<p>Para visualizar a nova mensagem, entre no <b>Residence Online</b>.</p>
					";

					if($resposta->token_app != null && $resposta->token_app != ""){
						if($resposta->id_usuario != $this->session->userdata("id"))
							$this->enviarNotificacao($resposta->id_usuario, "Novidade!", "Você recebeu uma resposta no fórum.");
					}else{
						$this->enviarEmail("Você recebeu uma resposta no fórum.", $resposta->email_dono, $mensagem);
					}
				}
			}
		}
	}

	function getRespostasDiscussao($inicio = NULL, $offset = NULL, $id = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("respostas_discussoes.discussao", $id);
		$this->db->join("usuarios", "usuarios.id = respostas_discussoes.usuario");
		$this->db->order_by("respostas_discussoes.id", "ASC");
		$this->db->select("respostas_discussoes.*, usuarios.nome as nome_usuario, usuarios.foto as foto_usuario");
		$this->db->order_by("id", "ASC");
		return $this->db->get("respostas_discussoes");
	}

	function desativar_discussao(){
		$post = $this->input->post();
		$this->db->query("UPDATE discussoes_forum SET ativado = !ativado WHERE id = $post[id]");
		$this->session->set_flashdata("mensagem_forum", "Alterado com sucesso.");
	}
	// FIM CRUD DE FORUM---------------------------------------------------------------

	// INÍCIO CRUD DE SOLICITACOES ------------------------------------------------------------------------
	// function getSolicitacoes($tipo = NULL, $inicio = NULL, $offset = NULL, $keyword = NULL){
	// 	if($tipo != NULL){
	// 		$this->db->limit($offset, $inicio);
	// 		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("$tipo.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where($tipo, $this->session->userdata("id"));
	// 		$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
	// 		$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
	// 		$this->db->order_by("id", "DESC");
	// 		$this->db->select("solicitacoes.*, destinatario.nome as nome_destinatario, remetente.nome as nome_remetente");
	// 		return $this->db->get("solicitacoes");
	// 	}
	// }

	// function getSolicitacoes($tipo = NULL, $inicio = NULL, $offset = NULL, $keyword = NULL){
	// 	if($tipo != NULL){
	// 		$this->db->limit($offset, $inicio);
	// 		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("$tipo.cnpj", $this->session->userdata("cnpj"));
	// 		// $this->db->where($tipo, $this->session->userdata("id"));
	// 		$this->db->where($this->session->userdata("id")." IN (SELECT usuario FROM participantes_solicitacoes WHERE usuario = ".$this->session->userdata("id").")");
	// 		$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
	// 		$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
	// 		$this->db->order_by("id", "DESC");
	// 		$this->db->select("solicitacoes.*, destinatario.nome as nome_destinatario, remetente.nome as nome_remetente");
	// 		return $this->db->get("solicitacoes");
	// 	}
	// }

	function getSolicitacoes($inicio = NULL, $offset = NULL, $keyword = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->order_by("solicitacoes.data_alteracao", 'desc');
		$this->db->join('participantes_solicitacoes', "participantes_solicitacoes.solicitacao = solicitacoes.id");
		$this->db->join('usuarios', "solicitacoes.remetente = usuarios.id");
		$this->db->where("participantes_solicitacoes.usuario", $this->session->userdata('id'));
		$this->db->where("solicitacoes.cnpj", $this->session->userdata('cnpj'));
		$this->db->select("solicitacoes.* , usuarios.nome AS nome_remetente");
		return $this->db->get("solicitacoes");
	}

	// function getSolicitacao($id = NULL){
	// 	if($id != NULL){
	// 		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("remetente.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("destinatario.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("(destinatario.id = ".$this->session->userdata("id")." OR remetente.id = ".$this->session->userdata("id").")");
	// 		$this->db->where("solicitacoes.id", $id);
	// 		$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
	// 		$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
	// 		$this->db->join("residencias as residencia_remetente", "remetente.residencia = residencia_remetente.id");
	// 		$this->db->join("residencias as residencia_destinatario", "destinatario.residencia = residencia_destinatario.id");

	// 		$this->db->join("localizacoes as localizacao_remetente", "remetente.localizacao = localizacao_remetente.id");
	// 		$this->db->join("localizacoes as localizacao_destinatario", "destinatario.localizacao = localizacao_destinatario.id");

	// 		$this->db->select("solicitacoes.*, remetente.nome as nome_remetente, remetente.foto as foto_remetente, destinatario.nome as nome_destinatario, destinatario.foto as foto_destinatario, residencia_remetente.nome as residencia_remetente, localizacao_remetente.nome as localizacao_remetente, residencia_destinatario.nome as residencia_destinatario, localizacao_destinatario.nome as localizacao_destinatario");
	// 		return $this->db->get("solicitacoes");
	// 	}
	// }

	function getSolicitacao($id = NULL){
		$this->session->unset_userdata("ultimaMensagem");
		if($id != NULL){
			$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("remetente.cnpj", $this->session->userdata("cnpj"));
			// $this->db->where("destinatario.cnpj", $this->session->userdata("cnpj"));
			// $this->db->where("(destinatario.id = ".$this->session->userdata("id")." OR remetente.id = ".$this->session->userdata("id").")");
			$this->db->where($this->session->userdata('id')." IN (SELECT usuario FROM participantes_solicitacoes WHERE usuario = ".$this->session->userdata('id')." AND participantes_solicitacoes.solicitacao = $id)");
			$this->db->where("solicitacoes.id", $id);
			$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
			$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
			$this->db->join("residencias as residencia_remetente", "remetente.residencia = residencia_remetente.id");
			$this->db->join("residencias as residencia_destinatario", "destinatario.residencia = residencia_destinatario.id");

			$this->db->join("localizacoes as localizacao_remetente", "remetente.localizacao = localizacao_remetente.id");
			$this->db->join("localizacoes as localizacao_destinatario", "destinatario.localizacao = localizacao_destinatario.id");

			$this->db->select("solicitacoes.*, remetente.nome as nome_remetente, remetente.foto as foto_remetente, destinatario.nome as nome_destinatario, destinatario.foto as foto_destinatario, residencia_remetente.nome as residencia_remetente, localizacao_remetente.nome as localizacao_remetente, residencia_destinatario.nome as residencia_destinatario, localizacao_destinatario.nome as localizacao_destinatario");
			return $this->db->get("solicitacoes");
		}
	}

	function getMensagensSolicitacao($id = NULL){
		$this->db->where("mensagens_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("mensagens_solicitacoes.solicitacao", $id);
		$this->db->join("usuarios as remetente", "remetente.id = mensagens_solicitacoes.remetente");
		// $this->db->join("usuarios as destinatario", "destinatario.id = mensagens_solicitacoes.destinatario");
		$this->db->select("mensagens_solicitacoes.*, remetente.nome as nome_remetente, remetente.foto as foto_remetente");
		$this->db->order_by("data_alteracao", "ASC");
		return $this->db->get("mensagens_solicitacoes");
	}

	function getParticipantesSolicitacao($id = NULL){
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.solicitacao", $id);
		$this->db->join("usuarios", "usuarios.id = participantes_solicitacoes.usuario", 'left');
		$this->db->join("residencias", "usuarios.residencia = residencias.id", 'left');
		$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", 'left');
		$this->db->join("solicitacoes", "participantes_solicitacoes.solicitacao = solicitacoes.id");
		$this->db->select("usuarios.id, usuarios.nome, usuarios.foto, localizacoes.nome as localizacao_participante, residencias.nome as residencia_participante");
		return $this->db->get("participantes_solicitacoes");
	}

	function testeParticipante($id){
		$this->db->start_cache();
		$this->db->where("usuario", $this->session->userdata("id"));
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("solicitacao", $id);
		return $this->db->get("participantes_solicitacoes");
	}

	function getMensagensServerSent($id = NULL){
		$ultimaMensagem = $this->session->userdata("ultimaMensagem");
		$this->db->where("mensagens_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("mensagens_solicitacoes.solicitacao", $id);
		$this->db->where("mensagens_solicitacoes.data_alteracao > '".$ultimaMensagem."'");
		$this->db->join("usuarios as remetente", "remetente.id = mensagens_solicitacoes.remetente");
		$this->db->join("usuarios as destinatario", "destinatario.id = mensagens_solicitacoes.destinatario", 'left');
		$this->db->select("mensagens_solicitacoes.*, remetente.nome as nome_remetente, destinatario.nome as nome_destinatario, remetente.foto as foto_remetente, destinatario.foto as foto_destinatario");
		$this->db->order_by("id", "ASC");
		return $this->db->get("mensagens_solicitacoes");
	}

	function enviarMensagemSolicitacao(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("id", $post['id']);
		$solicitacao = $this->db->get("solicitacoes");
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($solicitacao->num_rows() == 1){
			$solicitacao = $solicitacao->first_row();
			if($solicitacao->remetente == $this->session->userdata("id")){
				$destinatario = $solicitacao->destinatario;
			}else if($solicitacao->destinatario == $this->session->userdata("id")){
				$destinatario = $solicitacao->remetente;
			}else{
				$destinarario = NULL;
			}
			if($solicitacao->destinatario != NULL){
				$dados = array(
					'solicitacao'=>$post['id'],
					'mensagem'=>$post['mensagem'],
					'cnpj'=>$this->session->userdata("cnpj"),
					'remetente'=>$this->session->userdata("id"),
					// 'destinatario'=>$destinatario,
					"operador_alteracao"=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s")
				);
				if($this->db->insert("mensagens_solicitacoes", $dados)){
					$this->db->start_cache();
					$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
					$this->db->where("usuarios.id IN (SELECT usuario FROM participantes_solicitacoes WHERE solicitacao = $post[id] AND usuario != ".$this->session->userdata('id').")");
					$usuarios = $this->db->get("usuarios")->result();
					$this->db->stop_cache();
					$this->db->flush_cache();

					$this->db->start_cache();
					$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
					$this->db->where("usuarios.id", $this->session->userdata("id"));
					$remetente = $this->db->get("usuarios")->first_row();
					$this->db->stop_cache();
					$this->db->flush_cache();

					foreach ($usuarios as $usuario) {
						if($usuario->token_app != null && $usuario->token_app != ""){
							if($usuario->id != $this->session->userdata("id"))
								$this->enviarNotificacao($usuario->id, $remetente->nome, $post['mensagem'], 'false');
						}

						if($usuario->token_app_admin != null && $usuario->token_app_admin != ""){
							if($usuario->id != $this->session->userdata("id"))
								$this->enviarNotificacaoAdmin($usuario->id, $remetente->nome, $post['mensagem'], 'false');
						}	
					}
				}
			}
		}
	}

	function adicionarParticipanteComunique(){
		$post = $this->input->post();
		$this->db->where("participantes_solicitacoes.solicitacao", $post['id']);
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.usuario", $post['usuario']);
		$solicitacoes = $this->db->get("participantes_solicitacoes");
		if($solicitacoes->num_rows() == 0){
			$dados = array(
				"usuario"=>$post['usuario'],
				"solicitacao"=>$post['id'],
				"cnpj"=>$this->session->userdata("cnpj"),
			);
			$this->db->insert("participantes_solicitacoes", $dados);
		}else{
			echo "<p class='text-danger'>Usuário já está na comunicação.</p>";
		}
	}

	function removerParticipanteComunique(){
		$post = $this->input->post();
		$this->db->where("participantes_solicitacoes.solicitacao", $post['id']);
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.usuario", $post['usuario']);
		$solicitacoes = $this->db->get("participantes_solicitacoes");
		if($solicitacoes->num_rows() != 0){
			$this->db->query("DELETE FROM participantes_solicitacoes WHERE solicitacao = $post[id] AND usuario = $post[usuario]");
		}
	}

	function ajax_criar_comunique(){
		$post = $this->input->post();
		$dados = array(
			'cnpj'=>$this->session->userdata("cnpj"),
			'operador_alteracao'=>$this->session->userdata("id"),
			'remetente'=>$this->session->userdata("id"),
			'destinatario'=>$post['destinatario'],
			'titulo'=>$post['titulo'],
			'descricao'=>$post['descricao'],
			'data_alteracao'=>date("Y-m-d H:i:s")
		);
		$this->db->insert("solicitacoes", $dados);
		$id = $this->db->insert_id();

		$this->db->start_cache();
		$this->db->insert("participantes_solicitacoes", array("usuario"=>$this->session->userdata("id"),"cnpj"=>$this->session->userdata("cnpj"), "solicitacao"=>$id));
		$this->db->flush_cache();
		$this->db->stop_cache();

		$this->db->start_cache();
		$this->db->insert("participantes_solicitacoes", array("usuario"=>$dados['destinatario'],"cnpj"=>$this->session->userdata("cnpj"), "solicitacao"=>$id));
		$this->db->flush_cache();
		$this->db->stop_cache();

		$this->db->start_cache();
		$this->db->where("usuarios.id", $dados['destinatario']);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("usuarios.id, usuarios.email, usuarios.nome, usuarios.token_app, usuarios.token_app_admin");
		$destinatario = $this->db->get("usuarios")->first_row();
		$this->db->flush_cache();
		$this->db->stop_cache();
		if($destinatario != null){
			if($destinatario->email != null && trim($destinatario->email) != ""){
				$mensagem = "
				<p>Uma nova comunicação com você foi aberta no <b><a style='text-decoration: none;' href='".base_url()."'>Residence Online</a></b>.</p>
				<p>Para visualizar a nova comunicação, acesse o <b><a style='text-decoration: none;' href='".base_url()."'>Residence Online</a></b>.</p>
				";
				$this->enviarEmail("Uma comunicação com você foi aberta.", $destinatario->email, $mensagem);
			}

			if($destinatario->token_app != null && $destinatario->token_app != ""){
				if($destinatario->id != $this->session->userdata("id"))
					$this->enviarNotificacao($destinatario->id, "Comunique", "Uma nova conversa com você foi aberta.");
			}

			if($destinatario->token_app_admin != null && $destinatario->token_app_admin != ""){
				if($destinatario->id != $this->session->userdata("id"))
					$this->enviarNotificacaoAdmin($destinatario->id, "Comunique", "Uma nova conversa com você foi aberta.");
			}
		}

		echo '<script language="JavaScript">window.location="/comunique/ver/'.$id.'";</script> ';
	}

	function marcarResolvido(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("solicitacoes.id", $post['id']);
		$this->db->join("usuarios", "usuarios.id = solicitacoes.destinatario");
		$this->db->select("solicitacoes.*, usuarios.email, usuarios.token_app, usuarios.id as id_usuario");
		$solicitacao = $this->db->get("solicitacoes");
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($solicitacao->num_rows() == 1){
			$solicitacao = $solicitacao->first_row();
			$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("solicitacoes.id", $post['id']);
			$this->db->update("solicitacoes", array("resolvida"=>true));
			$this->session->set_flashdata("mensagem_comunique", "<div class='alert alert-info'><h3 class='text-center'>Comunicação resolvida!</h3></div>");
			if($solicitacao->email != null && trim($solicitacao->email) != ""){
				$mensagem = "<p>A comunicação <b>$solicicatao->titulo</b> enviada para você foi marcada como resolvida.</p>";

				if($solicitacao->token_app != null && $solicitacao->token_app != ""){
					if($solicitacao->id_usuario != $this->session->userdata("id"))
						$this->enviarNotificacao($solicitacao->id_usuario, "Comunicação resolvida", "$solicitacao->titulo foi resolvida");
				}else{
					$this->enviarEmail("A comunicação foi resolvida!", $solicitacao->email, $mensagem);
				}
			}
		}
	}

	// FIM CRUD DE SOLICITACOES ---------------------------------------------------------------------------
	// INÍCIO CONDOMÍNIO ----------------------------------------------------------------------------------
	function getCondominio(){
		$this->db->where("condominios.cnpj", $this->session->userdata("cnpj"));
		return$this->db->get("condominios");
	}
	// FIM CONDOMÍNIO -------------------------------------------------------------------------------------

	// INÍCIO AVISOS ----------------------------------------------------------------------------------
	function getAvisos($inicio = null, $offset = null, $keyword){
		// if($inicio != null && $offset != null)
			$this->db->limit($offset, $inicio);

		$keyword = str_replace(" ", "%", $keyword);
		$this->db->where("(avisos.titulo LIKE '%$keyword%')");
		$this->db->where("avisos.data_validade >= '".date("Y-m-d H:i:s")."'");
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("avisos.id, avisos.titulo, avisos.descricao, avisos.data_validade, avisos.data_alteracao, avisos.administradores");
		$this->db->order_by("avisos.id", 'DESC');
		return $this->db->get("avisos");
	}

	function getAviso(){
		$post = $this->input->post();
		$this->db->where("avisos.id", $post['id']);
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("avisos.id, avisos.titulo, avisos.descricao, avisos.data_validade, avisos.data_alteracao, avisos.administradores");
		$this->db->order_by("avisos.id", 'DESC');
		return $this->db->get("avisos");
	}

	function cadastrar_aviso(){
		$post = $this->input->post();
		$post['data_validade'] = explode("/", $post['data_validade']);
		$post['data_validade'] = $post['data_validade'][2]."-".$post['data_validade'][1]."-".$post['data_validade'][0];
		$dados = array(
			'administradores'=>$post['administradores'],
			'titulo'=>$post['titulo'],
			'descricao'=>$post['descricao'],
			'data_validade'=>$post['data_validade']." 23:59:59",
			'cnpj'=>$this->session->userdata("cnpj"),
			'operador_alteracao'=>$this->session->userdata("id"),
			'data_alteracao'=>date("Y-m-d H:i:s")
		);
		$this->db->insert("avisos", $dados);
		$this->session->set_flashdata("mensagem_cadastro_aviso", "Aviso cadastrado com sucesso.");
		$this->db->start_cache();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.email != ''");
		$this->db->where("usuarios.email IS NOT null");
		$this->db->where("usuarios.id != ".$this->session->userdata("id"));
		$usuarios = $this->db->get("usuarios")->result();
		$this->db->flush_cache();
		$this->db->stop_cache();

		foreach($usuarios as $usuario){
			if($usuario->token_app != null && $usuario->token_app != ""){
				if($usuario->id != $this->session->userdata("id"))
					$this->enviarNotificacao($usuario->id, "Novo aviso no condomínio", $dados['titulo']);
			}else if($usuario->email != null && $usuario->email != ""){
				$mensagem = "
				<p>Olá $usuario->nome,</p>
				<p>Um novo aviso para os moradores está disponível.</p>
				<p>Verifique o <b><a href='".base_url()."'>Residence Online</a></b> para visualizar o aviso.</p>
				";
				$this->enviarEmail("Novo aviso no condomínio.", $usuario->email, $mensagem);
			}
		}
	}

	function deletarAviso(){
		$post = $this->input->post();
		$this->db->where("avisos.id", $post['id']);
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->delete("avisos");
		$this->session->set_flashdata("mensagem_cadastro_aviso", "Aviso removido com sucesso.");
	}
	// FIM AVISOS -------------------------------------------------------------------------------------
	// INÍCIO DOCUMENTOS ------------------------------------------------------------------------------
	function getDocumentos($inicio = null, $offset = null, $keyword = null){
		$this->db->limit($offset, $inicio);
		if($keyword != null && $keyword != ""){
			$this->db->like("titulo", $keyword);
		}
		$this->db->where("documentos.cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("documentos");
	}

	function getDocumento($id = null){
		$this->db->where("documentos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("documentos.id", $id);
		return $this->db->get("documentos");
	}

	function cadastrar_documento($dados = null){
		$post = $this->input->post();
		$data = array(
			'titulo'=>$post['titulo'],
			'descricao'=>$post['descricao'],
			'operador_alteracao'=>$this->session->userdata("id"),
			'data_alteracao'=>date("Y-m-d"),
			'cnpj'=>$this->session->userdata("cnpj"),
		);
		if($dados != null)
			$data['caminho_arquivo'] = $dados['file_name'];
		$this->db->insert("documentos", $data);
	}

	function deletar_documento(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("documentos.id", $post['id']);
		$documento = $this->db->get("documentos")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();
		unlink("../residence.acessonaweb.com.br/documentos/".$documento->caminho_arquivo);
		$this->db->where("documentos.id", $post['id']);
		$this->db->delete("documentos");
		$this->session->set_flashdata("cadastro_documento", "<h4 class='text-info'>Removido com sucesso.</h4>");
		echo '<meta http-equiv="refresh" content="0; url=/documentos" />';
	}
	// FIM DOCUMENTOS ---------------------------------------------------------------------------------
}