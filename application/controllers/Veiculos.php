<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/veiculos?k=".$keyword;
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getVeiculos(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"admin_cadastro_veiculos",
				'veiculos'=>$this->admin->getVeiculos($inicio, $maximo, $keyword)->result(),
				'paginacao'=>$this->pagination->create_links()
			);
			$this->load->view("admin/veiculos/index", $dados);
		}
	}

	public function cadastrar(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->form_validation->set_rules("placa", "Placa", "required|addslashes|strtoupper");
			$this->form_validation->set_rules("uf", "UF", "addslashes");
			$this->form_validation->set_rules("cidade", "Cidade", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("tipo", "Tipo", "addslashes");
			$this->form_validation->set_rules("fabricante", "Fabricante", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("modelo", "Modelo", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("cor", "Cor", "addslashes");
			$this->form_validation->set_rules("usuario_responsavel", "Usuário Responsável", "required|addslashes");
			$this->form_validation->set_rules("residencia", "Residência", "addslashes");
			$this->form_validation->set_rules("situacao", "Situação cadastral", "required|addslashes");
			$this->form_validation->set_rules("obs_situacao", "Obs Situação", "addslashes|nl2br");

			$this->form_validation->set_error_delimiters('', '');

			if($this->form_validation->run()){
				$this->admin->cadastrar_veiculo();
			}else{
				$post = $this->input->post();
				if(isset($post['usuario_responsavel'])){
					$this->db->where("usuarios.id", $post['usuario_responsavel']);
					$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
					$this->db->join("residencias", "residencias.id = usuarios.residencia", "left");
					$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", "left");
					$this->db->select("usuarios.id, usuarios.nome, localizacoes.nome as localizacao, residencias.nome as residencia");
					$usuario_responsavel = $this->db->get("usuarios");
					if($usuario_responsavel->num_rows() == 1){
						$usuario_responsavel = $usuario_responsavel->first_row();
						$this->session->set_flashdata("value_select_usuario_responsavel", $usuario_responsavel->nome." ".$usuario_responsavel->localizacao." ".$usuario_responsavel->residencia);
					}
				}

				if(isset($post['residencia'])){
					$this->db->where("residencias.id", $post['residencia']);
					$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
					$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", "left");
					$this->db->select("residencias.nome, localizacoes.nome as localizacao");
					$residencia = $this->db->get("residencias");
					if($residencia->num_rows() == 1){
						$residencia = $residencia->first_row();
						$this->session->set_flashdata("value_select_residencia", $residencia->nome." ".$residencia->localizacao);
					}
				}
			}

			$dados = array(
				'menu_selecionado'=>"admin_cadastro_veiculos",
				'tipos_veiculos'=>$this->admin->cadastros_gerais("VEICULOS")->result(),
			);
			$this->load->view("admin/veiculos/cadastrar", $dados);
		}
	}

	function editar(){
		if($this->admin->logado(true)){
			$veiculo = $this->admin->getVeiculo($this->uri->segment(3))->first_row();
			if ($veiculo != null) {
				$dados = array(
					'menu_selecionado'=>'admin_cadastro_veiculos',
					'veiculo'=> $veiculo,
				);
				$this->load->view("admin/veiculos/editar", $dados);
			}else{
				redirect("/veiculos");
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function select2_listaTipos(){
		echo json_encode($this->admin->select2_cadastros_gerais("VEICULOS")->result());
	}

	function select2_listaUsuarios(){
		echo json_encode($this->admin->select2_getMoradores()->result());
	}

	function select2_listaResidencias(){
		echo json_encode($this->admin->select2_getResidencias()->result());
	}

	function ajax_cadastro_tipo(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");

			if($this->form_validation->run()){
				$this->admin->ajax_cadastro_tipo_veiculo();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function preencher_select_residencia(){
		$this->form_validation->set_rules("id", "Id", "required|is_numeric");
		if($this->form_validation->run())
			$this->admin->preencher_select_veiculo_residencia();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable(){ //ação do x-editable para alterar informações de moradores
		if($this->admin->logado(true)){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];
			$result = array('success'=>false, 'msg'=>"Campo desconhecido");

			switch($name){
				case "placa":
					$this->form_validation->set_rules('value', 'Placa', 'required|addslashes|max_length[50]|strtoupper', array('required'=>'Não pode ficar vazio!'));
					break;
				case "uf":
					$this->form_validation->set_rules('value', 'UF', 'addslashes|max_length[2]|strtoupper');
					break;
				case "cidade":
					$this->form_validation->set_rules('value', 'Cidade', 'addslashes');
					break;
				case "tipo":
					$this->form_validation->set_rules('value', 'Tipo', 'is_numeric|addslashes', array("is_numeric"=>"Tipo inválido"));
					break;
				case "fabricante":
					$this->form_validation->set_rules('value', 'Fabricante', 'addslashes');
					break;
				case "modelo":
					$this->form_validation->set_rules('value', 'Modelo', 'addslashes');
					break;
				case "cor":
					$this->form_validation->set_rules('value', 'Cor', 'addslashes');
					break;
				case "usuario_responsavel":
					$this->form_validation->set_rules('value', 'Usuário Responsável', 'is_numeric|addslashes|required', array("is_numeric"=>"Usuário inválido."));
					break;
				case "residencia":
					$this->form_validation->set_rules('value', 'Residência', 'addslashes|required');
					break;
				case "situacao":
					$this->form_validation->set_rules('value', 'Situação', 'addslashes');
					break;
				case "obs_situacao":
					$this->form_validation->set_rules('value', 'Obs. situação', 'addslashes');
					break;
			}

			if($this->form_validation->run() == TRUE){
				$this->admin->editable_veiculo($name, $value, $pk);
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
			}
		}else{
			$result = array('success'=>false, 'msg'=>'Você não está logado no sistema. <meta http-equiv="refresh" content="0; url=/" />');
		}
		echo json_encode($result);
	}

	function atualizar_foto(){
		if($this->admin->logado(true))
			$this->admin->atualizar_foto_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_trocar_usuario_responsavel(){
		if($this->admin->logado(true))
			$this->admin->editable_trocar_usuario_responsavel_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_trocar_residencia(){
		if($this->admin->logado(true))
			$this->admin->editable_trocar_residencia_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function deletar(){
		if($this->admin->logado(true))
			$this->admin->deletar_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}
}
