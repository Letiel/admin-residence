<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avisos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;
			$config['per_page'] = $maximo;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/avisos";
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getAvisos(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"admin_avisos",
				'avisos'=>$this->admin->getAvisos($inicio, $maximo, $keyword)->result(),
				'paginacao'=>$this->pagination->create_links()
			);
			$this->load->view("admin/avisos/index", $dados);
		}
	}

	function ajax_cadastro_aviso(){
		$this->form_validation->set_rules("titulo", "Título", "required");
		$this->form_validation->set_rules("descricao", "Descrição", "required|nl2br");
		$this->form_validation->set_rules("data_validade", "Data de validade", "required");
		$this->form_validation->set_rules("administradores", "Destino", "required");
		if($this->form_validation->run()){
			$this->admin->cadastrar_aviso();
		}else{
			echo validation_errors();
		}
	}

	function ver(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules('id', 'Id', 'trim|required|is_numeric');
			if($this->form_validation->run()){
				$dados= array(
					'aviso'=>$this->admin->getAviso()->first_row()
				);
				$this->load->view("admin/avisos/modals/ver", $dados);
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function deletar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules('id', 'Id', 'trim|required|is_numeric');
			if($this->form_validation->run()){
				$this->admin->deletarAviso();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}
