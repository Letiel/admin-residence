<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Condominio extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$dados = array(
				'menu_selecionado'=>'admin_condominio',
				'condominio'=>$this->admin->getCondominio()->first_row()
			);
			$this->load->view("admin/condominio/index", $dados);
		}
	}
}
