<?php
defined('BASEPATH') OR exit('Acesso negado');

class Forum extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if($this->admin->logado(true)){
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/forum";
			if ($this->input->get('p')) {
				$sgm = (int) trim($this->input->get('p'));
				$inicio = $config['per_page'] * ($sgm - 1);
			} else {
				$inicio = 0;
			}

			$config['total_rows'] = $this->admin->getDiscussoes(null, null)->num_rows();
			$this->pagination->initialize($config);


			$discussoes = $this->admin->getDiscussoes($inicio, $maximo);
			$dados = array(
				'total_discussoes'=>$discussoes->num_rows(),
				'discussoes'=>$discussoes->result(),
				'menu_selecionado'=>'admin_forum',
				'paginacao'=> $this->pagination->create_links(),
				);

			$this->load->view("admin/forum/index", $dados);
		}
	}

	public function cadastrar(){		
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("titulo", "Título", "required");
			$this->form_validation->set_rules("descricao", "Descrição", "required|nl2br");
			if($this->form_validation->run()){
				$this->admin->cadastrar_discussao();
			}
			$dados = array(
				'menu_selecionado'=>'admin_forum'
				);

			$this->load->view("admin/forum/cadastrar", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	public function ver(){
		if($this->admin->logado(true)){
			$id = $this->uri->segment(3);
			if(is_numeric($id)){

				$this->load->library("pagination");

				$maximo = 4;
				$config['per_page'] = $maximo;
				$config['first_link'] = '<<';
				$config['last_link'] = '>>';
				$config['next_link'] = '>';
				$config['prev_link'] = '<';   
				$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
				$config['full_tag_close'] = '</ul></nav>';
				$config['cur_tag_open'] = '<li class="active"><a href="">';
				$config['cur_tag_close'] = '</a></li>';
				$config['num_tag_open'] = '<li>';
				$config['num_tag_close'] = '</li>';
				$config['next_tag_open'] = '<li>';
				$config['next_tag_close'] = '</li>';
				$config['prev_tag_open'] = '<li>';
				$config['prev_tag_close'] = '</li>';
				$config['last_tag_open'] = '<li>';
				$config['last_tag_close'] = '</li>';
				$config['first_tag_open'] = '<li>';
				$config['first_tag_close'] = '</li>';
				$config['use_page_numbers'] = TRUE;
				$config['num_links'] = 3;

				$config['enable_query_strings'] = TRUE;
				$config['query_string_segment'] = 'p';
				$config['page_query_string'] = TRUE;
				$config['base_url'] = "/forum/ver/" . $id;
				if ($this->input->get('p')) {
					$sgm = (int) trim($this->input->get('p'));
					$inicio = $config['per_page'] * ($sgm - 1);
				} else {
					$inicio = 0;
				}

				$config['total_rows'] = $this->admin->getRespostasDiscussao(null, null, $id)->num_rows();
				$this->pagination->initialize($config);


				$respostas = $this->admin->getRespostasDiscussao($inicio, $maximo, $id);
				
				$this->form_validation->set_rules("resposta", "Resposta", "required|nl2br");
				if($this->form_validation->run()){
					$this->admin->responder_discussao($id);
					$ult_pagina = ceil($config['total_rows'] / $config['per_page']);
					redirect("/forum/ver/".$id."?p=".$ult_pagina);
				}

				$discussao = $this->admin->getDiscussao($id);
				if($discussao->first_row() != null){
					$dados = array(
						'menu_selecionado'=>'admin_forum',
						'discussao'=>$discussao->first_row(),
						'total_respostas'=>$respostas->num_rows(),
						'respostas'=>$respostas->result(),
						'paginacao'=> $this->pagination->create_links(),
					);
				}else
					redirect("/forum");

				$this->load->view("admin/forum/ver", $dados);
			}else{
				redirect("/");
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function desativar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric|trim");
			if ($this->form_validation->run()) {
				$this->admin->desativar_discussao();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}