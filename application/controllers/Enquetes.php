<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquetes extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/enquetes";
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getEnquetes(null, null)->num_rows();
			$this->pagination->initialize($config);


			$enquetes = $this->admin->getEnquetes($inicio, $maximo);
			$dados = array(
				'total_enquetes'=>$enquetes->num_rows(),
				'enquetes'=>$enquetes->result(),
				'menu_selecionado'=>'admin_enquetes',
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("admin/enquetes/index", $dados);
		}
	}

	function cadastrar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("titulo", "Título", "required");
			$this->form_validation->set_rules("opcoes[]", "Opções", "required", array("required"=>"Informe pelo menos duas opções."));
			$this->form_validation->set_rules("descricao", "Descrição", "nl2br");
			$this->form_validation->set_rules("data_validade", "Data de validade", "required");
			
			if($this->form_validation->run()){
				$this->admin->criar_enquete();
			}

			$dados = array(
				'menu_selecionado'=>'admin_enquetes',
			);
			$this->load->view("admin/enquetes/cadastrar", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ver(){
		if($this->admin->logado(true)){
			$id = (int) trim($this->uri->segment(3));
			$opcoes = $this->admin->getOpcoesEnquete($id);
			$this->form_validation->set_rules('voto', 'Voto', 'trim|required', array("required"=>"Selecione uma opção."));
			if ($this->form_validation->run()) {
				$this->admin->votarEnquete();
			}

			$enquete = $this->admin->getEnquete($id);
			if($enquete->first_row() != null){
				$dados = array(
					'enquete'=>$enquete->first_row(),
					'opcoes'=>$opcoes->result(),
					'total_opcoes'=>$opcoes->num_rows(),
					'menu_selecionado'=>'admin_enquetes',
					'voto_enquete'=>$this->admin->getVotoEnquete($id)->first_row(),
					'votos'=>$this->admin->votosEnquete()->result()
				);
				$this->load->view("admin/enquetes/ver", $dados);
			}else
				redirect("/enquetes");
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function alterar_validade(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric|trim");
			$this->form_validation->set_rules("data_validade", "Data de validade", "required");
			if ($this->form_validation->run()) {
				$this->admin->alterar_validade_enquete();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function desativar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric|trim");
			if ($this->form_validation->run()) {
				$this->admin->desativar_enquete();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}