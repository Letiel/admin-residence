<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservas extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$dados = array(
				'menu_selecionado'=>"admin_reservas",
				'reservas'=>$this->admin->getReservas()->result(),
			);
			$this->load->view("admin/reservas/index", $dados);
		}
	}

	public function ver(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			$dados = array(
				'reserva'=>$this->admin->getReserva()->first_row(),
				'convidados'=>$this->admin->getConvidados()->result()
			);
			$this->load->view("admin/reservas/modals/ver_reserva", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	public function imprimir(){
		if($this->admin->logado(true)){
			$_POST['id'] = $this->uri->segment(3);
			$dados = array(
				'reserva'=>$this->admin->getReserva()->first_row(),
				'convidados'=>$this->admin->getConvidados()->result()
			);
			$this->load->view("admin/reservas/imprimir", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	public function autorizar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$this->admin->autorizar_reserva();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}
