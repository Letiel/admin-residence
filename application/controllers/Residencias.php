<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Residencias extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/residencias?k=".$keyword;
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getResidencias(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"admin_residencias",
				'residencias'=>$this->admin->getResidencias($inicio, $maximo, $keyword)->result(),
				'paginacao'=>$this->pagination->create_links()
			);
			$this->load->view("admin/residencias/index", $dados);
		}
	}

	public function cadastrar(){
		if($this->admin->logado(true)){
			$dados = array(
				'menu_selecionado'=>"admin_residencias"
			);
			$this->load->view("admin/residencias/cadastrar", $dados);
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function select2_listaLocalizacoes(){
		echo json_encode($this->admin->select2_getLocalizacoes()->result());
	}

	function ajax_cadastro_residencia(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("localizacao", "Localizacao", "required|addslashes");
			$this->form_validation->set_rules("descricao", "Descrição", "addslashes|nl2br");

			if($this->form_validation->run()){
				$this->admin->ajax_cadastro_residencia();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editar(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules('id', 'Id', 'trim|required|is_numeric');
			if($this->form_validation->run()){
				$dados= array(
					'residencia'=>$this->admin->getResidencia()->first_row()
				);
				$this->load->view("admin/residencias/modals/editar_residencia", $dados);
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_edicao_residencia(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			$this->form_validation->set_rules("nome", "Nome", "required|is_numeric");
			$this->form_validation->set_rules("localizacao", "Localizacao", "required|is_numeric");
			$this->form_validation->set_rules("descricao", "Descrição", "nl2br");
			if($this->form_validation->run()){
				$this->admin->ajax_edicao_residencia();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function moradores(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$dados = array(
					'moradores'=>$this->admin->getMoradoresResidencia()->result()
				);
				$this->load->view("admin/residencias/modals/ver_moradores", $dados);
			}
		}
	}

	function deletar(){
		if($this->admin->logado()){
			$this->admin->deletar_residencia();
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ver(){
		if($this->admin->logado()){
			$dados = array(
				'residencia'=>$this->admin->getResidencia()->first_row(),
				'moradores'=>$this->admin->getMoradoresResidencia()->result(),
				'boxes'=>$this->admin->getBoxesResidencia()->result(),
				'veiculos'=>$this->admin->getVeiculosResidencia()->result(),
			);
			$this->load->view("admin/residencias/modals/ver_residencia", $dados);
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}
}
