<?php
defined('BASEPATH') OR exit('Acesso negado');

class Comunique extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	// public function index(){
	// 	if($this->admin->logado(true)){
	// 		$dados = array(
	// 			'menu_selecionado'=>'admin_solicitacoes',
	// 			'solicitacoes_recebidas'=>$this->admin->getSolicitacoes("destinatario", 0, 5)->result(),
	// 			'total_solicitacoes_recebidas'=>$this->admin->getSolicitacoes("destinatario")->num_rows(),
	// 			'solicitacoes_enviadas'=>$this->admin->getSolicitacoes("remetente", 0, 5)->result(),
	// 			'total_solicitacoes_enviadas'=>$this->admin->getSolicitacoes("remetente")->num_rows(),
	// 		);
	// 		$this->load->view("admin/comunique/index", $dados);
	// 	}else{
	// 		redirect("/");
	// 	}
	// }

	function index(){
		if($this->admin->logado(true)){

			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/comunique";
			if ($this->input->get('p')) {
				$sgm = (int) trim($this->input->get('p'));
				$inicio = $config['per_page'] * ($sgm - 1);
			} else {
				$inicio = 0;
			}

			$config['total_rows'] = $this->admin->getsolicitacoes(null, null)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>'admin_solicitacoes',
				'solicitacoes'=>$this->admin->getSolicitacoes($inicio, $maximo)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("admin/comunique/index", $dados);
		}else{
			redirect("/");
		}
	}


	function enviadas(){
		if($this->admin->logado(true)){

			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/comunique/enviadas";
			if ($this->input->get('p')) {
				$sgm = (int) trim($this->input->get('p'));
				$inicio = $config['per_page'] * ($sgm - 1);
			} else {
				$inicio = 0;
			}

			$config['total_rows'] = $this->admin->getsolicitacoes("remetente", null, null)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>'admin_solicitacoes',
				'solicitacoes_enviadas'=>$this->admin->getSolicitacoes("remetente", $inicio, $maximo)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("admin/comunique/enviadas", $dados);
		}else{
			redirect("/");
		}
	}

	function recebidas(){
		if($this->admin->logado(true)){

			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/comunique/recebidas";
			if ($this->input->get('p')) {
				$sgm = (int) trim($this->input->get('p'));
				$inicio = $config['per_page'] * ($sgm - 1);
			} else {
				$inicio = 0;
			}

			$config['total_rows'] = $this->admin->getsolicitacoes("destinatario", null, null)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>'admin_solicitacoes',
				'solicitacoes_recebidas'=>$this->admin->getSolicitacoes("destinatario", $inicio, $maximo)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("admin/comunique/recebidas", $dados);
		}else{
			redirect("/");
		}
	}

	function ver(){
		if($this->admin->logado()){
			$solicitacao = $this->uri->segment(3);
			$solicitacao = trim($solicitacao);
			if(is_numeric($solicitacao)){
				$solic = $this->admin->getSolicitacao($solicitacao);
				if($solic->first_row() != null){
					$dados = array(
						'menu_selecionado'=>'admin_solicitacoes',
						'solicitacao'=>$solic->first_row(),
						'participantes'=>$this->admin->getParticipantesSolicitacao($solicitacao)->result(),
						'mensagens'=>$this->admin->getMensagensSolicitacao($solicitacao)->result(),
					);
					$this->load->view("admin/comunique/ver", $dados);
				}else
					redirect("/comunique");
			}
		}else{
			redirect("/");
		}
	}

	function server(){
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		$id = $this->uri->segment(3);
		$mensagens = $this->admin->getMensagensServerSent($id)->result();

		if($this->admin->testeParticipante($id)->num_rows() == 0){
			echo "data: <META http-equiv=\"refresh\" content=\"1;URL=/comunique\"> \n\n";
		}else
		foreach ($mensagens as $mensagem){
			echo "data: <div class=\"chat-message ".($mensagem->remetente == $this->session->userdata("id") ? 'left' : 'right')."\">\n";
			        if($mensagem->foto_remetente != null && $mensagem->foto_remetente != ""){
			            $foto = "https://residence.acessonaweb.com.br/imagens/moradores/".$mensagem->foto_remetente.'?'.uniqid();
			        }else{
			            $foto = "https://residence.acessonaweb.com.br/images/user.svg";
			        }
			    echo "data: <img class=\"message-avatar\" src=\"$foto\" alt=\"$mensagem->nome_remetente\" >\n";
			    echo "data: <div class=\"message\">\n";
			        echo "data: <a class=\"message-author\" href=\"#\">\n";
			            echo "data: $mensagem->nome_remetente.\n";
			        echo "data: </a>\n";
			        echo "data: <span class=\"message-date\"> ".date("d/m/Y H:i:s", strtotime($mensagem->data_alteracao))." </span>\n";
			            echo "data: <span class=\"message-content\">\n";
			                echo "data: $mensagem->mensagem\n";
			            echo "data: </span>\n";
			    echo "data: </div>\n";
			echo "data: </div>\n\n";
			$this->session->set_userdata("ultimaMensagem", $mensagem->data_alteracao);
		}
		flush();
	}

	function enviarMensagemSolicitacao(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			$this->form_validation->set_rules("mensagem", "Mensagem", "required|strip_tags");
			if($this->form_validation->run()){
				$this->admin->enviarMensagemSolicitacao();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function adicionarParticipante(){
		if($this->admin->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			$this->form_validation->set_rules("usuario", "Usuário", "required", array("required"=>"Informe um usuário."));
			if($this->form_validation->run()){
				$this->admin->adicionarParticipanteComunique();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function removerParticipante(){
		if($this->admin->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			$this->form_validation->set_rules("usuario", "Usuário", "required", array("required"=>"Informe um usuário."));
			if($this->form_validation->run()){
				$this->admin->removerParticipanteComunique();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function select2_listaUsuarios(){
		if($this->admin->logado(true))
			echo json_encode($this->admin->select2_getMoradores()->result());
	}

	function ajax_criar_comunique(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("titulo", "Título", "required|addslashes");
			$this->form_validation->set_rules("destinatario", "Usuário", "required|is_numeric");
			$this->form_validation->set_rules("descricao", "Descrição", "addslashes|nl2br");
			if($this->form_validation->run()){
				$this->admin->ajax_criar_comunique();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function marcarResolvido(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$this->admin->marcarResolvido();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}