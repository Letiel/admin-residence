<?php
defined('BASEPATH') OR exit('Acesso negado');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		$this->form_validation->set_rules("login", "Login", "required|addslashes");
		$this->form_validation->set_rules("senha", "Senha", "required|addslashes");

		if($this->form_validation->run()){
			$this->admin->login();
		}else{
			$this->session->set_flashdata("mensagem_login", validation_errors());
		}
		if($this->admin->logado(true)){
			$dados = array(
				'menu_selecionado'=>"admin_mural",
				'usuario'=>$this->admin->getMorador($this->session->userdata("id"))->first_row(),
				'comunique'=>$this->admin->getComuniqueMural()->result(),
				'reservas'=>$this->admin->getReservas()->result(),
			);
			$this->load->view('admin/home/index', $dados);
		}else{
			$this->load->view("login");
		}
	}

	public function sair(){
		$this->session->unset_userdata(array("id", "nome", "admin"));
		$this->index();
	}

	function removerImagens(){
		$this->db->select("veiculos.foto");
		$this->db->where("veiculos.foto != ''");
		$veiculos = $this->db->get("veiculos")->result();
		foreach($veiculos as $veiculo){
			$array[] = $veiculo->foto;
		}


		$path = '../residence.acessonaweb.com.br/imagens/veiculos';
		$diretorio = dir($path);
		 
		$total = 0;
		while($arquivo = $diretorio -> read()){
			if(!in_array($arquivo, $array) && $arquivo != "." && $arquivo != ".."){
				echo $arquivo."<br />";
				unlink($path."/".$arquivo);
				$total++;
			}
		}
		echo "<b>".$total." arquivos</b><br />";
		$diretorio -> close();

		// ----------------

		$this->db->select("usuarios.foto");
		$this->db->where("usuarios.foto != ''");
		$usuarios = $this->db->get("usuarios")->result();
		foreach($usuarios as $usuario){
			$array[] = $usuario->foto;
		}


		$path = '../residence.acessonaweb.com.br/imagens/moradores';
		$diretorio = dir($path);
		 
		$total = 0;
		while($arquivo = $diretorio -> read()){
			if(!in_array($arquivo, $array) && $arquivo != "." && $arquivo != ".."){
				echo $arquivo."<br />";
				unlink($path."/".$arquivo);
				$total++;
			}
		}
		echo "<b>".$total." arquivos</b><br />";
		$diretorio -> close();
	}
}