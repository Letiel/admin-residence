<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moradores extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/moradores?k=".$keyword;
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getMoradores(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"admin_cadastro_moradores",
				'moradores'=>$this->admin->getMoradores($inicio, $maximo, $keyword)->result(),
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("admin/moradores/index", $dados);
		}
	}

	public function cadastrar(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			function valid_date($date){    
			   $pattern = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';

			    if(preg_match($pattern, $date) )
			        return true; 
			    else
			        return false;
			}

			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");
			$this->form_validation->set_rules("cpf", "CPF", "addslashes");
			$this->form_validation->set_rules("rg", "RG", "addslashes");
			$this->form_validation->set_rules("tipo", "Tipo", "addslashes");
			$this->form_validation->set_rules("usuario_tipo", "Tipo de usuário", "addslashes");
			$this->form_validation->set_rules("localizacao", "Localização", "required|is_numeric|addslashes");
			$this->form_validation->set_rules("naturalidade", "Naturalidade", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("sexo", "Sexo", "required|addslashes");
			$this->form_validation->set_rules("data_nascimento", "Data de nascimento", "addslashes|valid_date", array("valid_date"=>"Data inválida!"));
			$this->form_validation->set_rules("email", "Email", "addslashes|valid_email");
			$this->form_validation->set_rules("fone", "Fone", "addslashes");
			$this->form_validation->set_rules("cel1", "Cel1", "addslashes");
			$this->form_validation->set_rules("cel2", "Cel2", "addslashes");
			$this->form_validation->set_rules("endereco", "Endereço", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("complemento", "Complemento", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("bairro", "Bairro", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("cidade", "Cidade", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("uf", "UF", "addslashes|strtoupper");
			$this->form_validation->set_rules("cep", "CEP", "addslashes");
			$this->form_validation->set_rules("situacao", "Situação", "required|addslashes");
			$this->form_validation->set_rules("obs_situacao", "Obs situação", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("obs_portaria", "Obs portaria", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("login", "Login", "addslashes|is_unique[usuarios.login]", array("is_unique"=>"Usuário já existente."));
			$this->form_validation->set_rules("senha", "Senha", "sha1|addslashes");

			$this->form_validation->set_error_delimiters('', '');

			if($this->form_validation->run()){
				$this->admin->cadastrar_morador();
			}

			$dados = array(
				'menu_selecionado'=>"admin_cadastro_moradores",
				'localizacoes'=>$this->admin->getLocalizacoes()->result()
			);
			$this->load->view("admin/moradores/cadastrar", $dados);
		}
	}

	function editar(){
		if($this->admin->logado(true)){
			$dados = array(
				'menu_selecionado'=>'admin_cadastro_moradores',
				'usuario'=>$this->admin->getMorador($this->uri->segment(3))->first_row(),
				'localizacoes'=>$this->admin->getLocalizacoes()->result()
			);
			$this->load->view("admin/moradores/editar", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function getTiposPorUsuario(){ //pegando subtipos para preencher os sub-tipos de usuário no form de edição de morador
		if($this->admin->logado(true))
			$this->admin->getTiposPorUsuario();
	}

	function editable(){ //ação do x-editable para alterar informações de moradores
		if($this->admin->logado(true)){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];
			$result = array('success'=>false, 'msg'=>"Campo desconhecido");

			function valid_date($date){    
			   $pattern = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';

			    if(preg_match($pattern, $date) )
			        return true; 
			    else
			        return false;
			}

			switch($name){
				case "nome":
					$this->form_validation->set_rules('value', 'Nome', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 50 caracteres no nome.'));
					break;
				case "cpf":
					$this->form_validation->set_rules('value', 'CPF', 'addslashes|max_length[14]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 14 caracteres no cpf.'));
					break;
				case "rg":
					$this->form_validation->set_rules('value', 'RG', 'addslashes|max_length[20]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'RG muito grande.'));
					break;
				case "usuario_tipo":
					$this->form_validation->set_rules('value', 'Tipo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "tipo":
					$this->form_validation->set_rules('value', 'Sub-tipo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "naturalidade":
					$this->form_validation->set_rules('value', 'Naturalidade', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "sexo":
					$this->form_validation->set_rules('value', 'Sexo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "data_nascimento":
					$this->form_validation->set_rules('value', 'Data de nascimento', 'addslashes|valid_date', array('required'=>'Não pode ficar vazio!', 'valid_date'=>"Data inválida!"));
					break;
				case "email":
					$this->form_validation->set_rules('value', 'Email', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "fone":
					$this->form_validation->set_rules('value', 'Fone', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cel1":
					$this->form_validation->set_rules('value', 'Cel 1', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cel2":
					$this->form_validation->set_rules('value', 'Cel 2', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "endereco":
					$this->form_validation->set_rules('value', 'Endereço', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "complemento":
					$this->form_validation->set_rules('value', 'Complemento', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "bairro":
					$this->form_validation->set_rules('value', 'Bairro', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cidade":
					$this->form_validation->set_rules('value', 'Cidade', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "uf":
					$this->form_validation->set_rules('value', 'UF', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cep":
					$this->form_validation->set_rules('value', 'CEP', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "situacao":
					$this->form_validation->set_rules('value', 'Situação', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "login":
					$this->form_validation->set_rules('value', 'Login', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "senha":
					$this->form_validation->set_rules('value', 'Senha', 'required', array('required'=>'Não pode ficar vazio!'));
					$value = sha1($value);
					break;
				case "obs_situacao":
					$this->form_validation->set_rules('value', 'Obs situação', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "obs_administracao":
					$this->form_validation->set_rules('value', 'Obs administração', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "obs_portaria":
					$this->form_validation->set_rules('value', 'Obs portaria', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "localizacao":
					$this->form_validation->set_rules('value', 'Localização', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "residencia":
					$this->form_validation->set_rules('value', 'Obs portaria', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
			}

			if($this->form_validation->run() == TRUE){
				$this->admin->editable_morador($name, $value, $pk);
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
			}
		}else{
			$result = array('success'=>false, 'msg'=>'Você não está logado no sistema. <meta http-equiv="refresh" content="0; url=/" />');
		}
		echo json_encode($result);
	}

	function atualizar_foto(){
		if($this->admin->logado(true))
			$this->admin->atualizar_foto_morador();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function deletar(){
		if($this->admin->logado(true))
			$this->admin->deletar_morador();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_cadastro_tipo(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("tipo", "Tipo", "required|addslashes");
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");

			if($this->form_validation->run()){
				$this->admin->ajax_cadastro_tipo_morador();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_cadastro_localizacao(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");
			$this->form_validation->set_rules("descricao", "Descrição", "addslashes");

			if($this->form_validation->run()){
				$this->admin->ajax_cadastro_localizacao();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_cadastro_residencia(){
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");
			$this->form_validation->set_rules("localizacao", "Localização", "addslashes");

			if($this->form_validation->run()){
				$this->admin->ajax_cadastro_residencia();
			}else{
				echo validation_errors();
			}
		}else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_atualizar_subTipos(){
		if($this->admin->logado(true))
			$this->admin->ajax_atualizar_subTipos();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_atualizar_localizacoes(){
		if($this->admin->logado(true))
			$this->admin->ajax_atualizar_localizacoes();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_atualizar_residencias(){
		if($this->admin->logado(true))
			$this->admin->ajax_atualizar_residencias();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_subTipos(){ //tela de edição de cadastro de morador... x-editable de sub-tipo
		if($this->admin->logado(true))
			$this->admin->editable_atualizar_subTipos();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_localizacoes(){ //tela de edição de cadastro de morador... x-editable de localizações
		if($this->admin->logado(true))
			$this->admin->editable_atualizar_localizacoes();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_residencias(){ //tela de edição de cadastro de morador... x-editable de localizações
		if($this->admin->logado(true))
			$this->admin->editable_atualizar_residencias();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_pegar_residencia(){ //tela de edição de cadastro de morador... x-editable de residências
		if($this->admin->logado(true))
			$this->admin->editable_pegar_residencia();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}


	function ReenviarLoginSenha(){
		if($this->admin->logado(true)){
			$dados= array(
				'usuario'=>$this->admin->getMorador($this->input->post('id'))->first_row()
			);
			$this->load->view("admin/moradores/modals/reenviar_login_senha", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_reenviar_login_senha(){
		
		if($this->admin->logado(true)){
			$this->form_validation->set_rules("email", "Email", "required|addslashes");
			$this->form_validation->set_rules("id", "Usuário", "required|addslashes");

			if($this->form_validation->run()){
				$this->admin->ajax_reenviar_login_senha();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}
