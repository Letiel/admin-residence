<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("admin");
	}

	public function index(){
		if(!$this->admin->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/documentos?k=".$keyword;
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->admin->getDocumentos(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"admin_documentos",
				'documentos'=>$this->admin->getDocumentos($inicio, $maximo, $keyword)->result(),
				'paginacao'=>$this->pagination->create_links()
			);
			$this->load->view("admin/documentos/index", $dados);
		}
	}

	function cadastrar(){
		if($this->admin->logado(true)){
			$config['upload_path']          = '../residence.acessonaweb.com.br/documentos';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 100000;
            $config['encrypt_name']         = true;

            $this->load->library('upload', $config);

			$this->form_validation->set_rules('titulo', 'Título', 'required|max_length[50]');
			$this->form_validation->set_rules('descricao', 'Descrição', 'required');

			if($this->form_validation->run()){
                if($_FILES['documento']['name'] != "" && $_FILES['documento']['name'] != null){
                	if($this->upload->do_upload('documento')){
                		$this->admin->cadastrar_documento($this->upload->data());
                		$this->session->set_flashdata("cadastro_documento", "<h4 class='text-success'>Cadastrado com sucesso!<h4>");
                		redirect("/documentos");
                	}else{
                		$this->session->set_flashdata("cadastro_documento", "<h4 class='text-danger'>Erro ao enviar arquivo.</h4>");
                	}
                }else{
                	$this->admin->cadastrar_documento();
                	$this->session->set_flashdata("cadastro_documento", "<h4 class='text-success'>Cadastrado com sucesso!<h4>");
                	redirect("/documentos");
                }

			}

			$dados['menu_selecionado'] = 'admin_documentos';
			$this->load->view("admin/documentos/cadastrar", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ver(){
		$documento = $this->admin->getDocumento(trim($this->uri->segment(3)))->first_row();
		if($documento == null){
			$this->session->set_flashdata("cadastro_documento", "<h4 class='text-danger'>Documento não encontrado.</h4>");
			redirect("/documentos");
		}
		$dados = array(
			'menu_selecionado'=>'admin_documentos',
			'documento'=>$documento
		);
		$this->load->view("admin/documentos/ver", $dados);
	}

	// function editar(){
	// 	if($this->admin->logado(true)){
	// 		$dados= array(
	// 			'area'=>$this->admin->getArea()->first_row()
	// 		);
	// 		$this->load->view("admin/areas/modals/editar_area", $dados);
	// 	}else{
	// 		echo '<meta http-equiv="refresh" content="0; url=/" />';
	// 	}
	// }

	function excluir(){
		if($this->admin->logado(true))
			$this->admin->deletar_documento();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}
}
